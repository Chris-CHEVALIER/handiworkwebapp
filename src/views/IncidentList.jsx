// @flow

import React from 'react';

import { Table, Button } from 'antd';
import TableColumnFilter from '../components/TableColumnFilter.jsx';
import FilterIcon from '../components/FilterIcon.jsx';
import LoadingIcon from '../components/LoadingIcon.jsx';

import ShowIncidentModal from '../views/ShowIncidentModal.jsx';

import Locale from '../locale/LocaleFactory';
import ArrayService from '../services/utils/ArrayService';
import DateService from '../services/utils/DateService';
import StringService from '../services/utils/StringService';
import FilterService from '../services/utils/FilterService';

import IncidentActions from '../actions/IncidentActions';
import IncidentStore from '../stores/IncidentStore';

import MissionActions from '../actions/MissionActions';
import MissionStore from '../stores/MissionStore';

import UserActions from '../actions/UserActions';
import UserStore from '../stores/UserStore';

// Sorting Methods
function sortDateColumn(r1: IIncident, r2: IIncident) {
    return StringService.compareCaseInsensitive(r1.date, r2.date);
}

function sortMissionColumn(r1: IIncident, r2: IIncident) {
    const m1 = MissionStore.getById(r1.mission.id);
    const m2 = MissionStore.getById(r2.mission.id);
    return StringService.compareCaseInsensitive(m1 && m1.name, m2 && m2.name);
}

function sortAuthorColumn(r1: IIncident, r2: IIncident) {
    const m1 = UserStore.getById(r1.author.id);
    const m2 = UserStore.getById(r2.author.id);
    return StringService.compareCaseInsensitive(
        m1 && m1.firstName + m1.lastName,
        m2 && m2.firstName + m2.lastName,
    );
}

type Prop = {};
type State = {
    loading: boolean,

    incidents: IIncident[],
    filteredIncidents: IIncident[],

    // Filters
    filters: Object,

    incidentToShow: ?IIncident,
};

/**
 * The list of the incidents.
 */
export default class IncidentList extends React.Component<Prop, State> {
    constructor() {
        super();

        const incidents = IncidentStore.getAll();

        this.state = {
            loading: !incidents.length,
            incidents,

            filteredIncidents: [],

            filters: {
                date: [],
                mission: [],
                author: [],
            },

            incidentToShow: null,
        };
    }

    componentDidMount() {
        this.IncidentListener = IncidentStore.addListener(this.receiveIncidents);
        this.missionListener = MissionStore.addListener(this.receiveMissions);
        this.userListener = UserStore.addListener(this.receiveUsers);

        MissionActions.reload();
        UserActions.reload();
        this.loadIncidents();

        this.updateFilters();
        // Here we set the default sorted column
        // Temporary solution waiting for AntD to propose a native way to do it.
        const column = this.table.findColumn('date');
        this.table.toggleSortOrder('descend', column);
    }

    componentWillUnmount() {
        this.IncidentListener.remove();
        this.missionListener.remove();
        this.userListener.remove();
    }

    loadIncidents = () => {
        this.setState({
            loading: true,
        });
        IncidentActions.reload().then(() => {
            this.setState({
                loading: false,
            });
        });
    };

    receiveIncidents = () => {
        this.setState(
            {
                incidents: IncidentStore.getAll(),
            },
            this.updateFilters,
        );
    };

    receiveMissions = () => {
        this.forceUpdate();
    };

    receiveUsers = () => {
        this.forceUpdate();
    };

    // Filters
    updateFilters = () => {
        const { incidents } = this.state;
        const filteredIncidents = incidents.filter(this.incidentMatchFilters);
        this.setState({ filteredIncidents });
    };

    incidentMatchFilters = (e: IIncident) => {
        const { filters } = this.state;
        return (
            FilterService.matchFilter(filters.date, e.date) &&
            FilterService.matchFilter(filters.mission, e.mission.id) &&
            FilterService.matchFilter(filters.author, e.author.id)
        );
    };

    handleFilterChange = (name: string, values: Array<string | number>) => {
        this.state.filters[name] = values;
        this.updateFilters();
    };

    getIncidentDates = () => ArrayService.unique(this.state.incidents.map(c => c.date));

    showIncident = (incident: IIncident) => {
        this.setState({
            incidentToShow: incident,
        });
    };

    hideIncident = () => {
        this.setState({
            incidentToShow: null,
        });
    };

    render() {
        const { incidentToShow } = this.state;
        return (
            <div className="report-list">
                {this.renderIncidentTable()}
                <ShowIncidentModal
                    incident={incidentToShow}
                    onCancel={this.hideIncident}
                    visible={!!incidentToShow}
                />
            </div>
        );
    }

    renderIncidentTable() {
        const { filters, filteredIncidents } = this.state;

        const columns = [
            {
                title: Locale.trans('incident.date'),
                key: 'date',
                sorter: sortDateColumn,
                filterIcon: <FilterIcon active={filters.date.length > 0} />,
                render: this.renderIncidentDateCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="date"
                        selectedValues={filters.date}
                        values={this.getIncidentDates().map(d => ({
                            text: DateService.formatApiToDisplay(d),
                            value: d,
                        }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: Locale.trans('incident.mission'),
                key: 'mission',
                sorter: sortMissionColumn,
                filterIcon: <FilterIcon active={filters.mission.length > 0} />,
                render: this.renderIncidentMissionCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="mission"
                        selectedValues={filters.mission}
                        values={MissionStore.getAll().map(m => ({ text: m.name, value: m.id }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: Locale.trans('incident.author'),
                key: 'author',
                sorter: sortAuthorColumn,
                filterIcon: <FilterIcon active={filters.author.length > 0} />,
                render: this.renderIncidentAuthorCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="author"
                        selectedValues={filters.author}
                        values={UserStore.getAll().map(m => ({
                            text: `${m.firstName} ${m.lastName}`,
                            value: m.id,
                        }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: null,
                key: 'actions',
                width: '100px',
                render: this.rendActionsCell,
            },
        ];

        return (
            <Table
                dataSource={filteredIncidents}
                rowKey="id"
                columns={columns}
                locale={Locale.Table}
                ref={(r) => {
                    this.table = r;
                }}
                loading={this.state.loading && { indicator: <LoadingIcon /> }}
            />
        );
    }

    renderIncidentDateCell = (incident: IIncident) => DateService.formatApiToDisplay(incident.date);

    renderIncidentMissionCell = (incident: IIncident) => {
        const mission = MissionStore.getById(incident.mission.id);
        return mission && mission.name;
    };

    renderIncidentAuthorCell = (incident: IIncident) => {
        const author = UserStore.getById(incident.author.id);
        return author && `${author.firstName} ${author.lastName}`;
    };

    rendActionsCell = (incident: IIncident) => (
        <div className="actions">
            {this.rendPrintCell(incident)}
            <Button
                type="primary"
                shape="circle"
                icon="eye"
                onClick={(e) => {
                    this.showIncident(incident);
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                }}
            />
        </div>
    );

    rendPrintCell = (incident: IIncident) => (
        // TODO : Add the click event to print the incident
        <Button type="primary" shape="circle" icon="printer" />
    );
}
