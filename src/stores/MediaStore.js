// @flow

import EntityStoreBase from '../stores/EntityStoreBase';
import MediaConstants from '../constants/MediaConstants';

type State = {
    entities: IMedia[],
};

class MediaStore extends EntityStoreBase<IMedia> {
    getInitialState(): State {
        return {
            entities: [],
        };
    }

    reduce(state: State, action: { type: string, payload: Object }): State {
        const { type, payload } = action;
        let newState = Object.assign({}, state);

        switch (type) {
        case MediaConstants.RECEIVE_MEDIAS_OF_REPORT:
            newState = this._receiveByReport(state, payload.report, payload.medias);
            break;
        case MediaConstants.RECEIVE_MEDIAS_OF_INCIDENT:
            newState = this._receiveByIncident(state, payload.incident, payload.medias);
            break;
        case MediaConstants.RECEIVE_MEDIAS_OF_REVIEW:
            newState = this._receiveByReview(state, payload.review, payload.medias);
            break;
        case MediaConstants.RECEIVE_MEDIA:
            newState = this._receiveEntity(state, payload.media);
            break;
        case MediaConstants.RECEIVE_MEDIAS:
            newState = this._receiveMany(state, payload.medias);
            break;
        case MediaConstants.DELETE_MEDIA:
            newState = this._deleteOne(state, payload.id);
            break;
        case 'clearAll':
            newState = this.getInitialState();
        default:
            newState = state;
        }
        return newState;
    }

    _receiveByReport = (state: State, report: IReport, medias: IMedia[]) => {
        const newState = Object.assign({}, state);
        newState.entities = state.entities.filter(e => medias.findIndex(a => e.id === a.id) === -1);
        newState.entities.push(...medias);
        return newState;
    };

    _receiveByIncident = (state: State, incident: IIncident, medias: IMedia[]) => {
        const newState = Object.assign({}, state);
        newState.entities = state.entities.filter(e => medias.findIndex(a => e.id === a.id) === -1);
        newState.entities.push(...medias);
        return newState;
    };

    _receiveByReview = (state: State, review: IReview, medias: IMedia[]) => {
        const newState = Object.assign({}, state);
        newState.entities = state.entities.filter(e => medias.findIndex(a => e.id === a.id) === -1);
        newState.entities.push(...medias);
        return newState;
    };

    _receiveMany = (state: State, medias: IMedia[]) => {
        const newState = Object.assign({}, state);
        // We remove all the entities that have the same ID as the new medias
        newState.entities = state.entities.filter(e => medias.findIndex(a => e.id === a.id) === -1);
        newState.entities.push(...medias);
        return newState;
    };

    _deleteOne = (state: State, mediaId: number) => {
        const i = state.entities.findIndex(c => c.id === mediaId);
        if (i === -1) {
            return state;
        }
        const newState = Object.assign({}, state);
        newState.entities = state.entities.slice();
        newState.entities.splice(i, 1);
        return newState;
    };

    // /// Public Methods  /////

    getMedia(): ?IMedia {
        return this.getState().media;
    }

    /**
     * Get all the medias of a report.
     */
    getByReport = (report: IReport): IMedia[] => this.getState().entities.filter(e => e.report && e.report.id === report.id);

    /**
     * Get all the medias of an incident.
     */
    getByIncident = (incident: IIncident): IMedia[] => this.getState().entities.filter(e => e.incident && e.incident.id === incident.id);

    /**
     * Get all the medias of a review.
     */
    getByReview = (review: IReview): IMedia[] => this.getState().entities.filter(e => e.review.id === review.id);

    /**
     * Get a media by ID.
     * @param {number} id The identifier.
     * @return {?Media} The media, or NULL if no entity is found.
     */
    getById = (id: number): ?IMedia => this.getState().entities.find(e => e.id === id);

    getByUser = (user: IUser): IMedia[] => (user.medias ? user.medias.map(b => this.getById(b.id)).filter(b => !!b) : []);

    /**
     * Get all the medias.
     */
    getAll = (): Array<IMedia> => this.getState().entities;
}

export default new MediaStore();
