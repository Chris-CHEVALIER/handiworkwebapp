// @flow

let caches: { [cache_name: string]: { [key: string]: number } } = {};

const CacheManager = {
    isCached: (cacheName: string, key: string, cacheDuration: number) => {
        if (!caches[cacheName]) {
            caches[cacheName] = {};
        }

        const validityDate = caches[cacheName][key]
            ? caches[cacheName][key] + (cacheDuration || 0)
            : 0;
        const now = Date.now();
        const ic = validityDate > now;
        if (!ic) {
            caches[cacheName][key] = now;
        }
        return ic;
    },

    clear: () => {
        caches = {};
    },
};

export default CacheManager;
