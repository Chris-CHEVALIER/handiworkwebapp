// @flow

import React from 'react';

import { Input, Table, Icon, Button, Row, Col, Popover } from 'antd';
import TableColumnFilter from 'components/TableColumnFilter.jsx';
import FilterIcon from 'components/FilterIcon.jsx';
import CreateSkillModal from 'views/CreateSkillModal.jsx';
import EditSkillModal from 'views/EditSkillModal.jsx';
import LoadingIcon from 'components/LoadingIcon.jsx';

import Locale from 'locale/LocaleFactory';
import ArrayService from 'services/utils/ArrayService';
import StringService from 'services/utils/StringService';
import FilterService from 'services/utils/FilterService';

import SkillActions from 'actions/SkillActions';
import SkillStore from 'stores/SkillStore';

// Sorting Methods
function sortNameColumn(c1: ISkill, c2: ISkill) {
    return StringService.compareCaseInsensitive(c1.name, c2.name);
}

type Prop = {};
type State = {
    loading: boolean,
    createSkillVisible: boolean,
    editSkillVisible: boolean,
    skillToEdit: ?ISkill,

    skills: ISkill[],
    filteredSkills: ISkill[],

    // Filters
    filterGlobal: string,
    filters: Object,
};

/**
 * The list of the host skills.
 */
export default class SkillList extends React.Component<Prop, State> {
    skillListener: *;
    table: *;

    constructor() {
        super();

        const skills = SkillStore.getAll();

        this.state = {
            loading: !skills.length,
            skills,

            filteredSkills: [],

            filterGlobal: '',
            filters: {
                name: [],
            },

            createSkillVisible: false,
            editSkillVisible: false,
            skillToEdit: null,
        };
    }

    componentDidMount() {
        this.skillListener = SkillStore.addListener(this.receiveSkills);

        this.loadSkills();

        this.updateFilters();
        // Here we set the default sorted column
        // Temporary solution waiting for AntD to propose a native way to do it.
        const column = this.table.findColumn('reference');
        this.table.toggleSortOrder('descend', column);
    }
    componentWillUnmount() {
        this.skillListener.remove();
    }

    loadSkills = () => {
        this.setState({
            loading: true,
        });
        SkillActions.reload().then(() => {
            this.setState({
                loading: false,
            });
        });
    };

    receiveSkills = () => {
        const skills = SkillStore.getAll();
        this.setState(
            {
                skills,
            },
            this.updateFilters,
        );
    };

    // Filters
    updateFilters = () => {
        const { skills } = this.state;
        const filteredSkills = skills.filter(this.skillMatchFilters);
        this.setState({ filteredSkills });
    };

    skillMatchFilters = (s: ISkill) => {
        const { filters } = this.state;
        return (
            FilterService.matchFilter(filters.name, s.name) &&
            this.matchGlobalSearch(s)
        );
    };

    searchGlobal = (e) => {
        this.state.filterGlobal = e.target.value.toLowerCase();
        this.updateFilters();
    };

    matchGlobalSearch = (skill: ISkill) => {
        const { filterGlobal } = this.state;
        return (
            skill.name.toLowerCase().indexOf(filterGlobal) > -1
        );
    };

    handleFilterChange = (name: string, values: Array<string | number>) => {
        this.state.filters[name] = values;
        this.updateFilters();
    };

    getSkillNames = () =>
        ArrayService.unique(this.state.skills.map(s => s.name));

    showCreateSkillModal = () => {
        this.setState({
            createSkillVisible: true,
        });
    };
    hideCreateSkillModal = () => {
        this.setState({
            createSkillVisible: false,
        });
    };

    editSkill = (skill: ISkill) => {
        this.setState({
            editSkillVisible: true,
            skillToEdit: skill,
        });
    };
    hideEditSkillModal = () => {
        this.setState({
            editSkillVisible: false,
            skillToEdit: null,
        });
    };

    render() {
        const { skillToEdit, loading } = this.state;
        return (
            <div className="skill-list">
                {this.renderSkillTable()}

                {!loading && (
                    <div className="actions-row" style={{ marginTop: !filteredSkills || filteredSkills.length === 0 ? '10px' : '-50px' }}>
                        <Button type="primary" icon="plus" onClick={this.showCreateSkillModal}>
                            Ajouter une compétence
                        </Button>
                    </div>
                )}

                <CreateSkillModal
                    onCancel={this.hideCreateSkillModal}
                    visible={this.state.createSkillVisible}
                />
                <EditSkillModal
                    skill={skillToEdit}
                    onCancel={this.hideEditSkillModal}
                    visible={this.state.editSkillVisible}
                />
            </div>
        );
    }

    renderSkillTable() {
        const { filters, filteredSkills } = this.state;

        const columns = [
            {
                title: Locale.trans('skill.name'),
                key: 'name',
                sorter: sortNameColumn,
                filterIcon: <FilterIcon active={filters.name.length > 0} />,
                render: this.renderSkillNameCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="name"
                        selectedValues={filters.name}
                        values={this.getSkillNames().map(r => ({ text: r, value: r }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: null,
                key: 'actions',
                width: '50px',
                render: this.rendActionsCell,
            },
        ];

        return (
            <Table
                dataSource={filteredSkills}
                rowKey="id"
                columns={columns}
                locale={Locale.Table}
                ref={(r) => {
                    this.table = r;
                }}
                loading={this.state.loading && { indicator: <LoadingIcon /> }}
            />
        );
    }

    renderSkillNameCell = (skill: ISkill) => skill.name;

    rendActionsCell = (skill: ISkill) => (
        <React.Fragment>
            <Button
                type="primary"
                shape="circle"
                icon="edit"
                onClick={(e) => {
                    this.editSkill(skill);
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                }}
            />
        </React.Fragment>
    );
}
