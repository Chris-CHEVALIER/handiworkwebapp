// @flow

import ServiceBase from "../services/ServiceBase";
import BaseUrlConstants from "../constants/BaseUrlConstants";

const URL: string = `${BaseUrlConstants.BASE_URL}`;

class MissionService extends ServiceBase {
    constructor() {
        super(URL);
    }

    /**
     * Get a mission by unique identifier.
     * @param {number} missionId The identifier of the mission.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getById(missionId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}missions/${missionId}`,
            method: "GET"
        });
    }

    /**
     * Get missions of a user.
     * @param {number} userId The identifier of the user.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getByUser(userId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}users/${userId}/missions`,
            method: "GET"
        });
    }

    /**
     * Get all missions.
     *
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getAll(): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}missions`,
            method: 'GET',
        });
    }

    /**
     * Post a new mission.
     * @param {Object} mission The mission to create.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    post(mission: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}missions`,
            method: 'POST',
            data: mission,
        });
    }

    /**
     * Patch an existing resource. Only the properties that are set on the patch will be updated.
     * @param {number} id The identifier of the report.
     * @param {Object} patch The properties to update.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    patch(id: number, patch: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}missions/${id}`,
            method: 'PATCH',
            data: patch,
        });
    }

    /**
     * Delete an existing host mission.
     * @param {number} id The identifier of the user.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    remove(id: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}missions/${id}`,
            method: 'DELETE',
        });
    }
}

export default new MissionService();
