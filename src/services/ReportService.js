// @flow

import ServiceBase from '../services/ServiceBase';
import BaseUrlConstants from "../constants/BaseUrlConstants";

const URL: string = `${BaseUrlConstants.BASE_URL}`;

class ReportService extends ServiceBase  {
    constructor() {
        super(URL);
    }

    /**
     * Get a report by unique identifier.
     * @param {number} reportId The identifier of the report.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getById(reportId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}reports/${reportId}`,
            method: "GET"
        });
    }

    /**
     * Get reports of a user.
     * @param {number} userId The identifier of the user.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getByUser(userId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}users/${userId}/reports`,
            method: "GET"
        });
    }

    /**
     * Get all reports.
     *
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getAll(): Promise<*> {        
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}reports`,
            method: 'GET',
        });
    }

    /**
     * Post a new report to an user.
     * @param {Object} report The report to create.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    post(report: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}reports`,
            method: 'POST',
            data: report
        });
    }

    /**
     * Patch an existing resource. Only the properties that are set on the patch will be updated.
     * @param {number} reportId The identifier of the report.
     * @param {Object} patch The properties to update.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    patch(reportId: number, patch: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}reports/${reportId}`,
            method: 'PATCH',
            data: patch
        });
    }

    /**
     * Delete an existing report.
     * @param {number} reportId The identifier of the report.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    remove(reportId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}reports/${reportId}`,
            method: 'DELETE',
        });
    }
}

export default new ReportService();