// @flow

import React from 'react';
import { Modal } from 'antd';

import SubSkillForm from 'components/forms/SubSkillForm.jsx';

import ToastActions from 'actions/ToastActions';

import SubSkillActions from 'actions/SubSkillActions';

import SkillActions from 'actions/SkillActions';
import SkillStore from 'stores/SkillStore';

type Prop = {
    onCancel: () => void,
    visible: boolean,
};
type State = {
    loading: boolean,
    fields: Object,
};

/**
 * The modal to create a new subskill.
 */
export default class CreateSubSkillModal extends React.Component<Prop, State> {
    skillListener: *;

    constructor() {
        super();
        this.state = {
            loading: false,
            fields: {},
            skills: SkillStore.getAll(),
        };
    }

    componentDidMount() {
        this.skillListener = SkillStore.addListener(this.receiveSkills);
        SkillActions.reload();
    }
    componentWillUnmount() {
        this.skillListener.remove();
    }
    receiveSkills = () => {
        this.setState({
            skills: SkillStore.getAll(),
        });
    }

    handleFormChange = (changedFields: Object) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    handleSubmit = (subSkill: ISubSkill) => {
        this.setState({
            loading: true,
        });
        SubSkillActions.create(subSkill)
            .then((newSubSkill: ISubSkill) => {
                this.setState({
                    fields: {},
                    loading: false,
                });
                ToastActions.createToastSuccess(`Sous-Compétence "${newSubSkill.name}" créée`);
                this.props.onCancel();
            })
            .catch(this.handleError);
    };

    handleError = (err: Object) => {
        this.setState({
            loading: false,
        });
        try {
            const resp = JSON.parse(err.response);
            ToastActions.createToastError(resp.message);
        } catch (e) {
            ToastActions.createToastError('Une erreur est survenue');
        }
    };

    render() {
        const { visible, onCancel } = this.props;
        const { fields, loading, skills } = this.state;
        return (
            <Modal
                title="Ajouter une Sous-Compétence"
                visible={visible}
                onCancel={onCancel}
                footer={null}
            >
                <SubSkillForm
                    skills={skills}
                    onChange={this.handleFormChange}
                    fields={fields}
                    onSubmit={this.handleSubmit}
                    loading={loading}
                />
            </Modal>
        );
    }
}
