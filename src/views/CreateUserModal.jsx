// @flow

import React from 'react';
import { Modal } from 'antd';

import UserForm from 'components/forms/UserForm.jsx';

import ToastActions from 'actions/ToastActions';

import UserActions from 'actions/UserActions';

import RoleActions from 'actions/RoleActions';
import RoleStore from 'stores/RoleStore';

import UserTypeActions from 'actions/UserTypeActions';
import UserTypeStore from 'stores/UserTypeStore';

type Prop = {
    onCancel: () => void,
    visible: boolean,
};
type State = {
    loading: boolean,
    fields: Object,
    roles: IRole[],
    userTypes: IUserType[],
};

/**
 * The modal to create a new user.
 */
export default class CreateUserModal extends React.Component<Prop, State> {
    constructor() {
        super();
        this.state = {
            loading: false,
            fields: {},
            roles: RoleStore.getAll(),
            userTypes: UserTypeStore.getAll()
        };
    }

    componentDidMount() {
        this.roleListener = RoleStore.addListener(this.receiveRoles);
        this.userTypeListener = UserTypeStore.addListener(this.receiveUserTypes);
        RoleActions.reload();
        UserTypeActions.reload();
    }

    componentWillUnmount() {
        this.roleListener.remove();
        this.userTypeListener.remove();
    }

    receiveRoles = () => {
        this.setState({
            roles: RoleStore.getAll(),
        });
    };

    receiveUserTypes = () => {
        this.setState({
            userTypes: UserTypeStore.getAll()
        });
    };

    handleFormChange = (changedFields: Object) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    handleSubmit = (user: IUser) => {
        this.setState({
            loading: true,
        });
        UserActions.create(user)
            .then((newUser: IUser) => {
                this.setState({
                    fields: {},
                    loading: false,
                });
                ToastActions.createToastSuccess(`Utilisateur "${newUser.firstName} ${newUser.lastName}" créé`);
                this.props.onCancel();
            })
            .catch(this.handleError);
    };

    handleError = (err: Object) => {
        this.setState({
            loading: false,
        });
        try {
            const resp = JSON.parse(err.response);
            ToastActions.createToastError(resp.message);
        } catch (e) {
            ToastActions.createToastError('Une erreur est survenue');
        }
    };

    render() {
        const { visible, onCancel } = this.props;
        const { fields, roles, userTypes, loading } = this.state;
        return (
            <Modal
                title="Ajouter un Utilisateur"
                visible={visible}
                onCancel={onCancel}
                footer={null}
            >
                <UserForm
                    onChange={this.handleFormChange}
                    fields={fields}
                    roles={roles}
                    userTypes={userTypes}
                    onSubmit={this.handleSubmit}
                    loading={loading}
                />
            </Modal>
        );
    }
}
