// @flow

import EntityStoreBase from "../stores/EntityStoreBase";
import SkillConstants from "../constants/SkillConstants";

type State = {
    entities: { [user_id: number]: ISkill[] },
    currentSkillId: ?number,
};

class SkillStore extends EntityStoreBase<ISkill> {
    getInitialState(): State {
        return {
            currentSkillId: null,
            entities: []
        };
    }

    reduce(state: State, action: { type: string, payload: Object }): State {
        const { type, payload } = action;
        let newState = Object.assign({}, state);

        switch (type) {
            case SkillConstants.RECEIVE_SKILLS_OF_USER:
                newState = this._receiveByUser(
                    state,
                    payload.user,
                    payload.skills
                );
                break;
            case SkillConstants.RECEIVE_SKILL:
                newState = this._receiveEntity(state, payload.skill);
                break;
            case SkillConstants.RECEIVE_SKILLS:
                newState = this._receiveMany(state, payload.skills);
                break;
            case SkillConstants.CHANGE_CURRENT_SKILL:
                newState.currentSkillId = payload.skillId;
                break;
            case "clearAll":
                newState = this.getInitialState();
            default:
                newState = state;
        }
        return newState;
    }

    _receiveByUser = (state: State, user: IUser, skills: ISkill[]) => {
        let newState = Object.assign({}, state);
        newState.entities = state.entities.filter(
            e => skills.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...skills);
        return newState;
    };

    _receiveMany = (state: State, skills: ISkill[]) => {
        let newState = Object.assign({}, state);
        // We remove all the entities that have the same ID as the new skills
        newState.entities = state.entities.filter(
            e => skills.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...skills);
        return newState;
    };

    ///// Public Methods  /////

    getSkill(): ?ISkill {
        return this.getState().skill;
    }

    /**
     * Get a skill by ID.
     * @param {number} id The identifier.
     * @return {?Skill}    The skill, or NULL if no entity is found.
     */
    getById = (id: number): ?ISkill => {
        return this.getState().entities.find(e => e.id === id);
    };

    /**
     * Get all skills of a user.
     */
    getByUser = (user: IUser): ISkill[] => {
        return this.getState().entities.filter(e => !!e.user.find(a => a.id === user.id));
    };

    /**
     * Get all the skills.
     */
    getAll = (): Array<ISkill> => this.getState().entities;

    /**
     * Get the identifier of the current skill.
     * @return {[type]} [description]
     */
    getCurrentSkillId = (): ?number => {
        return (
            this.getState().currentSkillId ||
            (this.getState().entities.length
                ? this.getState().entities[0].id
                : null)
        );
    };

    /**
     * Get the identifier of the current skill.
     * @return {[type]} [description]
     */
    getCurrentSkill = (): ISkill =>
        this.getById(this.getCurrentSkillId());
}

export default new SkillStore();