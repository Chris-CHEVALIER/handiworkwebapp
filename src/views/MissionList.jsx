// @flow

import React from 'react';

import { Input, Table, Icon, Button, Row, Col, Popover, Tooltip, Menu, Dropdown } from 'antd';
import TableColumnFilter from 'components/TableColumnFilter.jsx';
import FilterIcon from 'components/FilterIcon.jsx';
import CreateMissionModal from 'views/CreateMissionModal.jsx';
import EditMissionModal from 'views/EditMissionModal.jsx';
import MissionEmargementsModal from 'views/MissionEmargementsModal.jsx';
import LoadingIcon from 'components/LoadingIcon.jsx';

import Locale from 'locale/LocaleFactory';
import ArrayService from 'services/utils/ArrayService';
import StringService from 'services/utils/StringService';
import FilterService from 'services/utils/FilterService';
import SecurityService from 'services/SecurityService';
import Access from 'constants/AccessLevel';
import Resource from 'constants/Resource';

import MissionActions from 'actions/MissionActions';
import MissionStore from 'stores/MissionStore';
import EmargementService from '../services/EmargementService';

// Sorting Methods
function sortNameColumn(c1: IMission, c2: IMission) {
    return StringService.compareCaseInsensitive(c1.name, c2.name);
}

type Prop = {};
type State = {
    loading: boolean,
    createMissionVisible: boolean,
    editMissionVisible: boolean,
    missionToEdit: ?IMission,
    displayEmargementsOf: ?IMission,

    missions: IMission[],
    filteredMissions: IMission[],

    // Filters
    filterGlobal: string,
    filters: Object,
};

/**
 * The list of the missions.
 */
export default class MissionList extends React.Component<Prop, State> {
    constructor(props) {
        super(props);

        const missions = MissionStore.getAll();

        this.state = {
            loading: !missions.length,
            missions,

            filteredMissions: [],

            filterGlobal: '',
            filters: {
                name: [],
            },

            createMissionVisible: false,
            editMissionVisible: false,
            missionToEdit: null,

            displayEmargementsOf: null,
        };
    }

    componentDidMount() {
        this.hostMissionListener = MissionStore.addListener(this.receiveMissions);

        this.loadHostMissions();

        this.updateFilters();
        // Here we set the default sorted column
        // Temporary solution waiting for AntD to propose a native way to do it.
        const column = this.table.findColumn('reference');
        this.table.toggleSortOrder('descend', column);
    }

    componentWillUnmount() {
        this.hostMissionListener.remove();
    }

    loadHostMissions = () => {
        this.setState({
            loading: true,
        });
        MissionActions.reload().then(() => {
            this.setState({
                loading: false,
            });
        });
    };

    receiveMissions = () => {
        const missions = MissionStore.getAll();
        this.setState(
            {
                missions,
            },
            this.updateFilters,
        );
    };

    // Filters
    updateFilters = () => {
        const { missions } = this.state;
        const filteredMissions = missions.filter(this.missionMatchFilters);
        this.setState({ filteredMissions });
    };

    missionMatchFilters = (e: IMission) => {
        const { filters } = this.state;
        return FilterService.matchFilter(filters.name, e.name) && this.matchGlobalSearch(e);
    };

    searchGlobal = (e) => {
        this.state.filterGlobal = e.target.value.toLowerCase();
        this.updateFilters();
    };

    matchGlobalSearch = (mission: IMission) => {
        const { filterGlobal } = this.state;
        return mission.name.toLowerCase().indexOf(filterGlobal) > -1;
    };

    handleFilterChange = (name: string, values: Array<string | number>) => {
        this.state.filters[name] = values;
        this.updateFilters();
    };

    getMissionNames = () => ArrayService.unique(this.state.missions.map(c => c.name));

    showCreateMissionModal = () => {
        this.setState({
            createMissionVisible: true,
        });
    };
    hideCreateMissionModal = () => {
        this.setState({
            createMissionVisible: false,
        });
    };

    editMission = (mission: IMission) => {
        this.setState({
            editMissionVisible: true,
            missionToEdit: mission,
        });
    };
    hideEditMissionModal = () => {
        this.setState({
            editMissionVisible: false,
            missionToEdit: null,
        });
    };

    showEmargementsOfMission = (mission: IMission) => {
        this.setState({
            displayEmargementsOf: mission,
        });
    };

    hideEmargementModal = () => {
        this.setState({
            displayEmargementsOf: null,
        });
    };

    render() {
        const { missionToEdit, loading, displayEmargementsOf, filteredMissions } = this.state;
        return (
            <div className="mission-list">
                {this.renderMissionTable()}

                {!loading && SecurityService.isGranted(Resource.MISSION, Access.CREATE) && (
                    <div className="actions-row" style={{ marginTop: !filteredMissions || filteredMissions.length === 0 ? '10px' : '-50px' }}>
                        <Button type="primary" icon="plus" onClick={this.showCreateMissionModal}>
                            Ajouter un dispositif
                        </Button>
                    </div>
                )}

                <CreateMissionModal
                    onCancel={this.hideCreateMissionModal}
                    visible={this.state.createMissionVisible}
                />
                <EditMissionModal
                    mission={missionToEdit}
                    onCancel={this.hideEditMissionModal}
                    visible={this.state.editMissionVisible}
                />
                {displayEmargementsOf && (
                    <MissionEmargementsModal
                        onCancel={this.hideEmargementModal}
                        mission={displayEmargementsOf}
                    />
                )}
            </div>
        );
    }

    renderMissionTable() {
        const { filters, filteredMissions } = this.state;

        const columns = [
            {
                title: Locale.trans('mission.name'),
                key: 'name',
                sorter: sortNameColumn,
                filterIcon: <FilterIcon active={filters.name.length > 0} />,
                render: this.renderMissionNameCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="name"
                        selectedValues={filters.name}
                        values={this.getMissionNames().map(r => ({ text: r, value: r }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: null,
                key: 'actions',
                width: '200px',
                render: this.rendActionsCell,
            },
        ];

        return (
            <Table
                dataSource={filteredMissions}
                rowKey="id"
                columns={columns}
                locale={Locale.Table}
                ref={(r) => {
                    this.table = r;
                }}
                loading={this.state.loading && { indicator: <LoadingIcon /> }}
            />
        );
    }

    renderMissionNameCell = (mission: IMission) => mission.name;

    rendActionsCell = (mission: IMission) => {
        const showEmargementsBtn =
            SecurityService.isGranted(Resource.EMARGEMENT, Access.READ) &&
            this.renderShowEmargementsCell(mission);
        const printEmargementsBtn =
            SecurityService.isGranted(Resource.PRINT_EMARGEMENT, Access.READ) &&
            this.rendPrintCell(mission);
        return (
            <div className="actions">
                {showEmargementsBtn}
                {printEmargementsBtn}
                <Button
                    type="primary"
                    shape="circle"
                    icon="edit"
                    onClick={(e) => {
                        this.editMission(mission);
                        e.stopPropagation();
                        e.preventDefault();
                        return false;
                    }}
                />
            </div>
        );
    };

    renderShowEmargementsCell = (mission: IMission) => (
        <Tooltip title="Voir les émargements">
            <Button
                type="primary"
                shape="circle"
                icon="check"
                onClick={() => {
                    this.showEmargementsOfMission(mission);
                }}
            />
        </Tooltip>
    );

    rendPrintCell = (mission: IMission) => {
        const menu = (
            <Menu onClick={({ key }) => EmargementService.download(mission.id, key)}>
                <Menu.Item key="user">Imprimer par stagiaire</Menu.Item>
                <Menu.Item key="date">Imprimer par date</Menu.Item>
            </Menu>
        );
        return (
            <Dropdown overlay={menu}>
                <Button type="primary" shape="circle" icon="printer" />
            </Dropdown>
        );
    };
}
