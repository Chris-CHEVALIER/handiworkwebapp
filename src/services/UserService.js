// @flow

import ServiceBase from '../services/ServiceBase';
import BaseUrlConstants from '../constants/BaseUrlConstants';

const URL: string = `${BaseUrlConstants.BASE_URL}`;

class UserService extends ServiceBase {
    constructor() {
        super(URL);
    }

    /**
     * Get a user by unique identifier.
     * @param {number} userId The identifier of the user.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getById(userId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}users/${userId}`,
            method: 'GET',
        });
    }

    /**
     * Get users of a mission.
     * @param {number} missionId The identifier of the mission.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getByMission(missionId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}missions/${missionId}/users`,
            method: 'GET',
        });
    }

    /**
     * Get all users.
     *
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getAll(): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}users`,
            method: 'GET',
        });
    }

    /**
     * Post a new user.
     * @param {Object} user The user to create.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    post(user: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}users`,
            method: 'POST',
            data: user,
        });
    }

    /**
     * Patch an existing resource. Only the properties that are set on the patch will be updated.
     * @param {number} reportId The identifier of the report.
     * @param {Object} patch The properties to update.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    patch(userId: number, patch: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}users/${userId}`,
            method: 'PATCH',
            data: patch,
        });
    }

    /**
     * Delete an existing user.
     * @param {number} userId The identifier of the user.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    remove(userId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}users/${userId}`,
            method: 'DELETE',
        });
    }
}

export default new UserService();
