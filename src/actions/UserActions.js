// @flow

import dispatcher from '../dispatchers/AppDispatcher';
import ActionsBase from '../actions/ActionsBase';

import UserConstants from '../constants/UserConstants';
import UserService from '../services/UserService';

import LoginStore from '../stores/LoginStore';

import CacheManager from '../services/CacheManager';

const CACHE_DURATION = 30 * 1000;

class UserActions extends ActionsBase {
    create = (user: Object): Promise<*> => {
        const $this = this;
        return new Promise((resolve: (user: IUser) => void, reject: (response: Object) => void): void => {
            function handleSuccess(newUser: IUser): void {
                dispatcher.dispatch({
                    type: UserConstants.RECEIVE_USER,
                    payload: {
                        user: newUser,
                    },
                });
                resolve(newUser);
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            UserService.post(user)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    edit = (userId: number, user: Object): Promise<*> => {
        const $this = this;
        return new Promise((resolve: (user: IUser) => void, reject: (response: Object) => void): void => {
            function handleSuccess(newUser: IUser): void {
                dispatcher.dispatch({
                    type: UserConstants.RECEIVE_USER,
                    payload: {
                        user: newUser,
                    },
                });
                resolve(newUser);
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            UserService.patch(userId, user)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    delete = (id: number): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(): void {
                dispatcher.dispatch({
                    type: UserConstants.DELETE_USER,
                    payload: {
                        id,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            UserService.remove(id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    changeCurrentUserId = (userId: number): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            dispatcher.dispatch({
                type: UserConstants.CHANGE_CURRENT_USER,
                payload: {
                    userId,
                },
            });
            resolve();
        });
    };

    reload = (): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(users: Array<User>): void {
                dispatcher.dispatch({
                    type: UserConstants.RECEIVE_USERS,
                    payload: {
                        users,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            UserService.getAll()
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reloadByMission = (mission: Entity): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            if (
                CacheManager.isCached(
                    'User:reloadByMission',
                    mission.id.toString(),
                    CACHE_DURATION,
                )
            ) {
                resolve();
                return;
            }

            function handleSuccess(users: Array<IUser>): void {
                dispatcher.dispatch({
                    type: UserConstants.RECEIVE_USERS_OF_MISSION,
                    payload: {
                        mission,
                        users,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            UserService.getByMission(mission.id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reloadById = (id: number): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(user: IUser): void {
                dispatcher.dispatch({
                    type: UserConstants.RECEIVE_USER,
                    payload: {
                        user,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            UserService.getById(id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };
}

export default new UserActions();
