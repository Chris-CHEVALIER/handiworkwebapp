// @flow

import dispatcher from '../dispatchers/AppDispatcher';
import ActionsBase from '../actions/ActionsBase';

import SkillConstants from '../constants/SkillConstants';
import SkillService from '../services/SkillService';

import CacheManager from '../services/CacheManager';

const CACHE_DURATION = 30 * 1000;

class SkillActions extends ActionsBase {
    create = (skill: Object): Promise<*> => {
        const $this = this;
        return new Promise((
            resolve: (skill: ISkill) => void,
            reject: (response: Object) => void,
        ): void => {
            function handleSuccess(newSkill: ISkill): void {
                dispatcher.dispatch({
                    type: SkillConstants.RECEIVE_SKILL,
                    payload: {
                        skill: newSkill,
                    },
                });
                resolve(newSkill);
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            SkillService.post(skill)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    edit = (skillId: number, skill: Object): Promise<*> => {
        const $this = this;
        return new Promise((
            resolve: (skill: ISkill) => void,
            reject: (response: Object) => void,
        ): void => {
            function handleSuccess(newSkill: ISkill): void {
                dispatcher.dispatch({
                    type: SkillConstants.RECEIVE_SKILL,
                    payload: {
                        skill: newSkill,
                    },
                });
                resolve(newSkill);
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            SkillService.patch(skillId, skill)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    delete = (id: number): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(): void {
                dispatcher.dispatch({
                    type: SkillConstants.DELETE_SKILL,
                    payload: {
                        id,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            SkillService.remove(id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reload = (): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(skills: Array<Skill>): void {
                dispatcher.dispatch({
                    type: SkillConstants.RECEIVE_SKILLS,
                    payload: {
                        skills,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            SkillService.getAll()
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    changeCurrentSkillId = (skillId: number): Promise<*> => new Promise((resolve: () => void, reject: (response: Object) => void): void => {
        dispatcher.dispatch({
            type: SkillConstants.CHANGE_CURRENT_SKILL,
            payload: {
                skillId,
            },
        });
        resolve();
    });

    reloadById = (id: number): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(skill: ISkill): void {
                dispatcher.dispatch({
                    type: SkillConstants.RECEIVE_SKILL,
                    payload: {
                        skill,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            SkillService.getById(id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reloadByUser = (user: Entity): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            if (
                CacheManager.isCached('Skill:reloadByUser', user.id.toString(), CACHE_DURATION)
            ) {
                resolve();
                return;
            }

            function handleSuccess(skills: Array<ISkill>): void {
                dispatcher.dispatch({
                    type: SkillConstants.RECEIVE_SKILLS_OF_USER,
                    payload: {
                        user,
                        skills,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            SkillService.getByUser(user.id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };
}

export default new SkillActions();
