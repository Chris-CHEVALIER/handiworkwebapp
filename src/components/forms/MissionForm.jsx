import React from 'react';
import { Form, Button } from 'antd';
import Locale from 'locale/LocaleFactory';
import FormBase from 'components/forms/FormBase.jsx';
import FormItem from 'components/forms/FormItems';
import StringService from 'services/utils/StringService';

function cmpUsersByName(u1, u2) {
    const uN1 = u1.firstName + u1.lastName;
    const uN2 = u2.firstName + u2.lastName;
    return StringService.compareCaseInsensitive(uN1, uN2);
}
function cmpSkillsByName(s1, s2) {
    return StringService.compareCaseInsensitive(s1.name, s2.name);
}
function cmpEstablishmentsByName(s1, s2) {
    return StringService.compareCaseInsensitive(s1.name, s2.name);
}

class MissionForm extends FormBase {
    componentDidMount() {
        // To disabled submit button at the beginning.
        this.props.form.validateFields();
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.isFormSubmited(true);
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err && this.props.onSubmit) {
                const mission = this.getEntityFromValues(values);

                mission.skill = values.skill_ids;

                mission.establishment = values.establishment_ids;
                mission.hostCompany = values.hostCompany_ids;

                this.props.onSubmit(mission);
            }
        });
    };

    getEntityFromValues = (values) => {
        const entity = {};
        const keys = Object.keys(values);
        const ignoredKeys = [];

        for (let i = 0; i < keys.length; i++) {
            const k = keys[i];
            if (!k.endsWith('_ids') && k !== 'user') {
                if (k.endsWith('_id')) {
                    const tK = k.replace('_id', '');
                    entity[tK] = values[k];
                    ignoredKeys.push(tK);
                } else if (ignoredKeys.indexOf(k) === -1) {
                    entity[k] = values[k];
                }
            } else if (k.startsWith('user_')) {
                if (!entity.user) {
                    entity.user = [];
                }
                entity.user = entity.user.concat(values[k]);
            }
        }

        return entity;
    };

    checkPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && value !== form.getFieldValue('plainPassword')) {
            callback('Les mots de passe ne sont pas identiques!');
        } else {
            callback();
        }
    };

    render() {
        const { readOnly } = this.props;
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem.Input
                    id="name"
                    required={!readOnly}
                    label={Locale.trans('mission.name')}
                    form={this.props.form}
                    validateStatus={this.getValidateStatus('name')}
                    help={this.getHelp('name')}
                    hasFeedback
                    readOnly={readOnly}
                />
                <FormItem.Input
                    id="schedules"
                    label={Locale.trans('user.schedules')}
                    form={this.props.form}
                    readOnly={readOnly}
                />

                {this.renderUserFields()}

                {this.renderEstablishmentsField()}
                {this.renderHostCompaniesField()}

                {this.renderSkillsField()}

                {!readOnly && (
                    <Button
                        type="primary"
                        htmlType="submit"
                        className="login-form-button"
                        loading={this.props.loading}
                    >
                        {Locale.trans('save')}
                    </Button>
                )}
            </Form>
        );
    }

    renderUserFields() {
        const { userTypes } = this.props;

        if (!userTypes) {
            return null;
        }

        return userTypes.filter(u => u.isVisibleOnMission).map(this.renderUserField);
    }

    renderUserField = (userType) => {
        const { users, readOnly } = this.props;

        if (!users || users.length === 0) {
            return null;
        }

        const { getFieldValue } = this.props.form;
        const initialValue = (getFieldValue('user') || [])
            .map(u => users.find(u1 => u1.id === u.id))
            .filter(u => u && u.type && u.type.id === userType.id)
            .map(s => s.id.toString());
        const options = users
            .filter(u => u.type && u.type.id === userType.id)
            .sort(cmpUsersByName)
            .map(u => ({
                value: u.id,
                label: `${u.lastName} ${u.firstName}`,
            }));
        return (
            <FormItem.Select
                key={userType.id}
                id={`user_${userType.id}_ids`}
                multiple
                label={userType.title}
                initialValue={initialValue}
                options={options}
                form={this.props.form}
                readOnly={readOnly}
            />
        );
    };

    renderSkillsField() {
        const { skills, readOnly } = this.props;

        if (!skills || skills.length === 0) {
            return null;
        }

        const { getFieldValue } = this.props.form;
        const initialValue = (getFieldValue('skill') || []).map(s => s.id.toString());
        const options = skills.sort(cmpSkillsByName).map(s => ({
            value: s.id,
            label: s.name,
        }));
        return (
            <FormItem.Select
                id="skill_ids"
                multiple
                label={Locale.trans('mission.skills')}
                initialValue={initialValue}
                options={options}
                form={this.props.form}
                readOnly={readOnly}
            />
        );
    }

    renderEstablishmentsField() {
        const { establishments, readOnly } = this.props;

        if (!establishments || establishments.length === 0) {
            return null;
        }

        const { getFieldValue } = this.props.form;
        const initialValue = (getFieldValue('establishment') || []).map(s => s.id.toString());
        const options = establishments.sort(cmpEstablishmentsByName).map(s => ({
            value: s.id,
            label: s.name,
        }));
        return (
            <FormItem.Select
                id="establishment_ids"
                multiple
                label={Locale.trans('mission.establishments')}
                initialValue={initialValue}
                options={options}
                form={this.props.form}
                readOnly={readOnly}
            />
        );
    }

    renderHostCompaniesField() {
        const { hostCompanies, readOnly } = this.props;

        if (!hostCompanies || hostCompanies.length === 0) {
            return null;
        }

        const { getFieldValue } = this.props.form;
        const initialValue = (getFieldValue('hostCompany') || []).map(s => s.id.toString());
        const options = hostCompanies.sort(cmpEstablishmentsByName).map(s => ({
            value: s.id,
            label: s.name,
        }));
        return (
            <FormItem.Select
                id="hostCompany_ids"
                multiple
                label={Locale.trans('mission.hostCompanies')}
                initialValue={initialValue}
                options={options}
                form={this.props.form}
                readOnly={readOnly}
            />
        );
    }
}

export default Form.create({
    onFieldsChange(props, changedFields) {
        if (props.onChange) {
            props.onChange(changedFields);
        }
    },
    mapPropsToFields(props) {
        const { fields } = props;
        if (!fields) {
            return {};
        }
        const kFields = Object.keys(fields);
        const map = {};
        for (let i = 0; i < kFields.length; i++) {
            const k = kFields[i];
            map[k] = Form.createFormField({
                ...fields[k],
                value: fields[k].value,
            });
        }
        return map;
    },
})(MissionForm);
