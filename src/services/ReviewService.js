// @flow

import ServiceBase from '../services/ServiceBase';
import BaseUrlConstants from "../constants/BaseUrlConstants";

const URL: string = `${BaseUrlConstants.BASE_URL}`;

class ReviewService extends ServiceBase  {
    constructor() {
        super(URL);
    }

    /**
     * Get a review by unique identifier.
     * @param {number} reviewId The identifier of the review.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getById(reviewId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}reviews/${reviewId}`,
            method: "GET"
        });
    }

    /**
     * Get reviews of a user.
     * @param {number} userId The identifier of the user.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getByUser(userId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}users/${userId}/reviews`,
            method: "GET"
        });
    }

    /**
     * Get all reviews.
     *
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getAll(): Promise<*> {        
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}reviews`,
            method: 'GET',
        });
    }

    /**
     * Post a new review to an user.
     * @param {Object} review The review to create.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    post(review: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}reviews`,
            method: 'POST',
            data: review
        });
    }

    /**
     * Patch an existing resource. Only the properties that are set on the patch will be updated.
     * @param {number} reviewId The identifier of the review.
     * @param {Object} patch The properties to update.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    patch(reviewId: number, patch: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}reviews/${reviewId}`,
            method: 'PATCH',
            data: patch
        });
    }

    /**
     * Delete an existing review.
     * @param {number} reviewId The identifier of the review.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    remove(reviewId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}reviews/${reviewId}`,
            method: 'DELETE',
        });
    }
}

export default new ReviewService();