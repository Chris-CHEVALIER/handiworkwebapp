import React from 'react';
import { Form, Button } from 'antd';
import Locale from 'locale/LocaleFactory';
import FormBase from 'components/forms/FormBase.jsx';
import FormItem from 'components/forms/FormItems';

import StringService from 'services/utils/StringService';

function cmpUsersByName(u1, u2) {
    const uN1 = u1.firstName + u1.lastName;
    const uN2 = u2.firstName + u2.lastName;
    return StringService.compareCaseInsensitive(uN1, uN2);
}
class EstablishmentForm extends FormBase {
    componentDidMount() {
        // To disabled submit button at the beginning.
        this.props.form.validateFields();
    }

    handleSubmit = (e) => {
        const { form } = this.props;
        e.preventDefault();
        this.isFormSubmited(true);
        form.validateFieldsAndScroll((err, values) => {
            if (!err && this.props.onSubmit) {
                const establishment = this.getEntityFromValues(values);
                this.props.onSubmit(establishment);
            }
        });
    };

    getEntityFromValues = (values) => {
        const entity = {};
        const keys = Object.keys(values);
        const ignoredKeys = [];

        for (let i = 0; i < keys.length; i++) {
            const k = keys[i];
            if (!k.endsWith('_ids') && k !== 'user') {
                if (k.endsWith('_id')) {
                    const tK = k.replace('_id', '');
                    entity[tK] = values[k];
                    ignoredKeys.push(tK);
                } else if (ignoredKeys.indexOf(k) === -1) {
                    entity[k] = values[k];
                }
            } else if (k.startsWith('user_')) {
                if (!entity.user) {
                    entity.user = [];
                }
                entity.user = entity.user.concat(values[k]);
            }
        }

        return entity;
    };

    checkPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && value !== form.getFieldValue('plainPassword')) {
            callback('Les mots de passe ne sont pas identiques!');
        } else {
            callback();
        }
    };

    render() {
        const isEditing = !!this.props.establishment;
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem.Input
                    id="name"
                    required
                    label={Locale.trans('establishment.name')}
                    form={this.props.form}
                    validateStatus={this.getValidateStatus('name')}
                    help={this.getHelp('name')}
                    hasFeedback
                />
                <FormItem.Input
                    id="email"
                    label={Locale.trans('establishment.email')}
                    form={this.props.form}
                    validateStatus={this.getValidateStatus('email')}
                    help={this.getHelp('email')}
                    hasFeedback
                />
                <FormItem.Input
                    id="phoneNumber"
                    label={Locale.trans('establishment.phoneNumber')}
                    form={this.props.form}
                    validateStatus={this.getValidateStatus('phoneNumber')}
                    help={this.getHelp('phoneNumber')}
                    hasFeedback
                />

                <FormItem.TextArea
                    id="address"
                    autosize={{ minRows: 1, maxRows: 10 }}
                    label={Locale.trans('establishment.address')}
                    form={this.props.form}
                />
                <FormItem.Input
                    id="cp"
                    label={Locale.trans('establishment.postalCode')}
                    form={this.props.form}
                />
                <FormItem.Input
                    id="town"
                    label={Locale.trans('establishment.city')}
                    form={this.props.form}
                />

                {this.renderUserFields()}

                <Button
                    type="primary"
                    htmlType="submit"
                    className="login-form-button"
                    loading={this.props.loading}
                >
                    {Locale.trans('save')}
                </Button>
            </Form>
        );
    }

    renderUserFields() {
        const { userTypes } = this.props;

        if (!userTypes) {
            return null;
        }

        return userTypes.filter(u => u.isVisibleOnEstablishment).map(this.renderUserField);
    }

    renderUserField = (userType) => {
        const { users } = this.props;

        if (!users || users.length === 0) {
            return null;
        }

        const { getFieldValue } = this.props.form;
        const initialValue = (getFieldValue('user') || [])
            .map(u => users.find(u1 => u1.id === u.id))
            .filter(u => u && u.type && u.type.id === userType.id)
            .map(s => s.id.toString());
        const options = users
            .filter(u => u.type && u.type.id === userType.id)
            .sort(cmpUsersByName)
            .map(u => ({
                value: u.id,
                label: `${u.lastName} ${u.firstName}`,
            }));
        return (
            <FormItem.Select
                key={userType.id}
                id={`user_${userType.id}_ids`}
                multiple
                label={userType.title}
                initialValue={initialValue}
                options={options}
                form={this.props.form}
            />
        );
    };
}

export default Form.create({
    onFieldsChange(props, changedFields) {
        if (props.onChange) {
            props.onChange(changedFields);
        }
    },
    mapPropsToFields(props) {
        const { fields } = props;
        if (!fields) {
            return {};
        }
        const kFields = Object.keys(fields);
        const map = {};
        for (let i = 0; i < kFields.length; i++) {
            const k = kFields[i];
            map[k] = Form.createFormField({
                ...fields[k],
                value: fields[k].value,
            });
        }
        return map;
    },
})(EstablishmentForm);
