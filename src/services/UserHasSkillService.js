// @flow

import ServiceBase from '../services/ServiceBase';
import BaseUrlConstants from "../constants/BaseUrlConstants";

const URL: string = `${BaseUrlConstants.BASE_URL}`;

class UserHasSkillService extends ServiceBase  {
    constructor() {
        super(URL);
    }

    /**
     * Get a user has skill by unique identifier.
     * @param {number} userHasSkillId The identifier of the user has skill.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getById(userHasSkillId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}user-has-skills/${userHasSkillId}`,
            method: "GET"
        });
    }

    /**
     * Get all user has skills.
     *
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getAll(): Promise<*> {        
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}user-has-skills`,
            method: 'GET',
        });
    }

    /**
     * Post a new user has skill to an user.
     * @param {Object} userHasSkill The user has skill to create.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    post(userHasSkill: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}user-has-skills`,
            method: 'POST',
            data: userHasSkill
        });
    }

    /**
     * Patch an existing resource. Only the properties that are set on the patch will be updated.
     * @param {number} userHasSkillId The identifier of the user has skill.
     * @param {Object} patch The properties to update.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    patch(userHasSkillId: number, patch: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}user-has-skills/${userHasSkillId}`,
            method: 'PATCH',
            data: patch
        });
    }

    /**
     * Delete an existing user has skill.
     * @param {number} userHasSkillId The identifier of the user has skill.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    remove(userHasSkillId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}user-has-skills/${userHasSkillId}`,
            method: 'DELETE',
        });
    }
}

export default new UserHasSkillService();