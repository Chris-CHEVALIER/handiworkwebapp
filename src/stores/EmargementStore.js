// @flow

import EntityStoreBase from '../stores/EntityStoreBase';
import EmargementConstants from '../constants/EmargementConstants';

type State = {
    entities: IEmargement[],
};

class EmargementStore extends EntityStoreBase<IEmargement> {
    getInitialState(): State {
        return {
            entities: [],
        };
    }

    reduce(state: State, action: { type: string, payload: Object }): State {
        const { type, payload } = action;
        let newState = Object.assign({}, state);

        switch (type) {
        case EmargementConstants.RECEIVE_EMARGEMENTS_OF_MISSION:
            newState = this._receiveByMission(state, payload.mission, payload.emargements);
            break;
        case EmargementConstants.RECEIVE_EMARGEMENT:
            newState = this._receiveEntity(state, payload.emargement);
            break;
        case EmargementConstants.RECEIVE_EMARGEMENTS:
            newState = this._receiveMany(state, payload.emargements);
            break;
        case EmargementConstants.DELETE_EMARGEMENT:
            newState = this._deleteOne(state, payload.id);
            break;
        case 'clearAll':
            newState = this.getInitialState();
        default:
            newState = state;
        }
        return newState;
    }

    _receiveByMission = (state: State, mission: IMission, emargements: IEmargement[]) => {
        const newState = Object.assign({}, state);
        newState.entities = state.entities.filter(e => e.mission.id !== mission.id);
        newState.entities.push(...emargements);
        return newState;
    };

    _receiveMany = (state: State, emargements: IEmargement[]) => {
        const newState = Object.assign({}, state);
        // We remove all the entities that have the same ID as the new emargements
        newState.entities = state.entities.filter(e => emargements.findIndex(a => e.id === a.id) === -1);
        newState.entities.push(...emargements);
        return newState;
    };

    _deleteOne = (state: State, emargementId: number) => {
        const i = state.entities.findIndex(c => c.id === emargementId);
        if (i === -1) {
            return state;
        }
        const newState = Object.assign({}, state);
        newState.entities = state.entities.slice();
        newState.entities.splice(i, 1);
        return newState;
    };

    // /// Public Methods  /////

    getEmargement(): ?IEmargement {
        return this.getState().emargement;
    }

    /**
     * Get a emargement by ID.
     * @param {number} id The identifier.
     * @return {?Emargement} The emargement, or NULL if no entity is found.
     */
    getById = (id: number): ?IEmargement => this.getState().entities.find(e => e.id === id);

    /**
     * Get all the emargements.
     */
    getAll = (): Array<IEmargement> => this.getState().entities;

    /**
     * Get all the emargements of a mission.
     */
    getByMission = (mission: IMission): IEmargement[] => this.getState().entities.filter(e => e.mission.id === mission.id);
}

export default new EmargementStore();
