// @flow

import EntityStoreBase from "../stores/EntityStoreBase";
import ReportConstants from "../constants/ReportConstants";

type State = {
    entities: { [mission_id: number]: IReport[] },
    currentReportId: ?number
};

class ReportStore extends EntityStoreBase<IReport> {
    getInitialState(): State {
        return {
            entities: []
        };
    }

    reduce(state: State, action: { type: string, payload: Object }): State {
        const { type, payload } = action;
        let newState = Object.assign({}, state);

        switch (type) {
            case ReportConstants.RECEIVE_REPORTS_OF_USER:
                newState = this._receiveByUser(
                    state,
                    payload.user,
                    payload.reports
                );
                break;
            case ReportConstants.RECEIVE_REPORT:
                newState = this._receiveEntity(state, payload.report);
                break;
            case ReportConstants.RECEIVE_REPORTS:
                newState = this._receiveMany(state, payload.reports);
                break;
            case ReportConstants.DELETE_REPORT:
                newState = this._deleteOne(state, payload.id);
                break;
            case "clearAll":
                newState = this.getInitialState();
            default:
                newState = state;
        }
        return newState;
    }

    _receiveByUser = (state: State, user: IUser, reports: IReport[]) => {
        let newState = Object.assign({}, state);
        newState.entities = state.entities.filter(
            e => reports.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...reports);
        return newState;
    };

    _receiveMany = (state: State, reports: IReport[]) => {
        let newState = Object.assign({}, state);
        // We remove all the entities that have the same ID as the new reports
        newState.entities = state.entities.filter(
            e => reports.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...reports);
        return newState;
    };

    _deleteOne = (state: State, reportId: number) => {
        const i = state.entities.findIndex(c => c.id === reportId);
        if (i === -1) {
            return state;
        }
        let newState = Object.assign({}, state);
        newState.entities = state.entities.slice();
        newState.entities.splice(i, 1);
        return newState;
    };

    ///// Public Methods  /////

    getReport(): ?IReport {
        return this.getState().report;
    }

    /**
     * Get all the reports of an user.
     */
    getByUser = (user: IUser): IReport[] => {
        return this.getState().entities.filter(e => !!e.user.find(a => a.id === user.id));
    };

    /**
     * Get a report by ID.
     * @param {number} id The identifier.
     * @return {?Report} The report, or NULL if no entity is found.
     */
    getById = (id: number): ?IReport => {
        return this.getState().entities.find(e => e.id === id);
    };

    /**
     * Get all the reports.
     */
    getAll = (): Array<IReport> => this.getState().entities;

    /**
     * Get the identifier of the current report.
     * @return {[type]} [description]
     */
    getCurrentReportId = (): ?number => {
        return (
            this.getState().currentReportId ||
            (this.getState().entities.length
                ? this.getState().entities[0].id
                : null)
        );
    };

    /**
     * Get the identifier of the current report.
     * @return {[type]} [description]
     */
    getCurrentReport = (): IReport =>
        this.getById(this.getCurrentReportId());
}

export default new ReportStore();