// @flow

import React from 'react';
import { Modal } from 'antd';

import SubSkillForm from 'components/forms/SubSkillForm.jsx';

import ToastActions from 'actions/ToastActions';

import SubSkillActions from 'actions/SubSkillActions';

import SkillActions from 'actions/SkillActions';
import SkillStore from 'stores/SkillStore';

type Prop = {
    onCancel: () => void,
    visible: boolean,
    subSkill: ?ISubSkill,
};
type State = {
    loading: boolean,
    fields: Object,
};

/**
 * The modal to edit a subskill.
 */
export default class EditSubSkillModal extends React.Component<Prop, State> {
    skillListener: *;

    constructor(props: Prop) {
        super();
        this.state = {
            loading: false,
            fields: this.getFieldsFromEntity(props.subSkill),
            skills: SkillStore.getAll(),
        };
    }

    componentDidMount() {
        this.skillListener = SkillStore.addListener(this.receiveSkills);
        SkillActions.reload();
    }

    componentWillUnmount() {
        this.skillListener.remove();
    }

    componentWillReceiveProps(nextProps: Prop) {
        if (nextProps.visible && !this.props.visible) {
            this.setState({
                fields: this.getFieldsFromEntity(nextProps.subSkill),
            });
        }
    }

    receiveSkills = () => {
        this.setState({
            skills: SkillStore.getAll(),
        });
    }

    getFieldsFromEntity = (entity: Entity) => {
        if (!entity) {
            return {};
        }
        const fields = {};
        const keys = Object.keys(entity);
        for (let i = 0; i < keys.length; i++) {
            const k = keys[i];
            fields[k] = { value: entity[k] };
        }
        return fields;
    };

    handleFormChange = (changedFields: Object) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    handleSubmit = (subSkill: ISubSkill) => {
        if (!this.props.subSkill) {
            return;
        }
        const subSkillId = this.props.subSkill.id;

        this.setState({
            loading: true,
        });
        SubSkillActions.edit(subSkillId, subSkill)
            .then((newSubSkill: ISubSkill) => {
                this.setState({
                    fields: {},
                    loading: false,
                });
                ToastActions.createToastSuccess(`Sous-Compétence "${newSubSkill.name}" modifiée`);
                this.props.onCancel();
            })
            .catch(this.handleError);
    };

    handleError = (err) => {
        this.setState({
            loading: false,
        });
        try {
            const resp = JSON.parse(err.response);
            ToastActions.createToastError(resp.message);
        } catch (e) {
            ToastActions.createToastError('Une erreur est survenue');
        }
    };

    render() {
        const { subSkill, visible, onCancel } = this.props;
        const { fields, loading, skills } = this.state;
        return (
            <Modal
                title="Modifier une Sous-Compétence"
                visible={subSkill && visible}
                onCancel={onCancel}
                footer={null}
            >
                <SubSkillForm
                    skills={skills}
                    subSkill={subSkill}
                    onChange={this.handleFormChange}
                    fields={fields}
                    onSubmit={this.handleSubmit}
                    loading={loading}
                />
            </Modal>
        );
    }
}
