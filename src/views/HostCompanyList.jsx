// @flow

import React from 'react';

import { Input, Table, Icon, Button, Row, Col, Popover } from 'antd';
import TableColumnFilter from 'components/TableColumnFilter.jsx';
import FilterIcon from 'components/FilterIcon.jsx';
import CreateHostCompanyModal from 'views/CreateHostCompanyModal.jsx';
import EditHostCompanyModal from 'views/EditHostCompanyModal.jsx';
import LoadingIcon from 'components/LoadingIcon.jsx';

import Locale from 'locale/LocaleFactory';
import ArrayService from 'services/utils/ArrayService';
import StringService from 'services/utils/StringService';
import FilterService from 'services/utils/FilterService';

import HostCompanyActions from 'actions/HostCompanyActions';
import HostCompanyStore from 'stores/HostCompanyStore';

// Sorting Methods
function sortNameColumn(c1: IHostCompany, c2: IHostCompany) {
    return StringService.compareCaseInsensitive(c1.name, c2.name);
}

type Prop = {};
type State = {
    loading: boolean,
    createCompanyVisible: boolean,
    editCompanyVisible: boolean,
    companyToEdit: ?IHostCompany,

    companies: IHostCompany[],
    filteredCompanies: IHostCompany[],

    // Filters
    filterGlobal: string,
    filters: Object,
};

/**
 * The list of the host companies.
 */
export default class HostCompanyList extends React.Component<Prop, State> {
    constructor() {
        super();

        const companies = HostCompanyStore.getAll();

        this.state = {
            loading: !companies.length,
            companies,

            filteredCompanies: [],

            filterGlobal: '',
            filters: {
                name: [],
            },

            createCompanyVisible: false,
            editCompanyVisible: false,
            companyToEdit: null,
        };
    }

    componentDidMount() {
        this.hostCompanyListener = HostCompanyStore.addListener(this.receiveCompanies);

        this.loadHostCompanies();

        this.updateFilters();
        // Here we set the default sorted column
        // Temporary solution waiting for AntD to propose a native way to do it.
        const column = this.table.findColumn('reference');
        this.table.toggleSortOrder('descend', column);
    }
    componentWillUnmount() {
        this.hostCompanyListener.remove();
    }

    loadHostCompanies = () => {
        this.setState({
            loading: true,
        });
        HostCompanyActions.reload().then(() => {
            this.setState({
                loading: false,
            });
        });
    };

    receiveCompanies = () => {
        const companies = HostCompanyStore.getAll();
        this.setState(
            {
                companies,
            },
            this.updateFilters,
        );
    };

    // Filters
    updateFilters = () => {
        const { companies } = this.state;
        const filteredCompanies = companies.filter(this.companyMatchFilters);
        this.setState({ filteredCompanies });
    };

    companyMatchFilters = (u: IHostCompany) => {
        const { filters } = this.state;
        return (
            FilterService.matchFilter(filters.name, `${u.firstName} ${u.lastName}`) &&
            this.matchGlobalSearch(u)
        );
    };

    searchGlobal = (e) => {
        this.state.filterGlobal = e.target.value.toLowerCase();
        this.updateFilters();
    };

    matchGlobalSearch = (company: IHostCompany) => {
        const { filterGlobal } = this.state;
        return (
            company.name.toLowerCase().indexOf(filterGlobal) > -1
        );
    };

    handleFilterChange = (name: string, values: Array<string | number>) => {
        this.state.filters[name] = values;
        this.updateFilters();
    };

    getCompanyNames = () =>
        ArrayService.unique(this.state.companies.map(c => c.name));

    showCreateCompanyModal = () => {
        this.setState({
            createCompanyVisible: true,
        });
    };
    hideCreateCompanyModal = () => {
        this.setState({
            createCompanyVisible: false,
        });
    };

    editCompany = (company: IHostCompany) => {
        this.setState({
            editCompanyVisible: true,
            companyToEdit: company,
        });
    };
    hideEditCompanyModal = () => {
        this.setState({
            editCompanyVisible: false,
            companyToEdit: null,
        });
    };

    render() {
        const { companyToEdit, loading } = this.state;
        return (
            <div className="host-company-list">
                {this.renderCompanyTable()}

                {!loading && (
                    <div className="actions-row" style={{ marginTop: !filteredCompanies || filteredCompanies.length === 0 ? '10px' : '-50px' }}>
                        <Button type="primary" icon="plus" onClick={this.showCreateCompanyModal}>
                            Ajouter une entreprise
                        </Button>
                    </div>
                )}

                <CreateHostCompanyModal
                    onCancel={this.hideCreateCompanyModal}
                    visible={this.state.createCompanyVisible}
                />
                <EditHostCompanyModal
                    hostCompany={companyToEdit}
                    onCancel={this.hideEditCompanyModal}
                    visible={this.state.editCompanyVisible}
                />
            </div>
        );
    }

    renderCompanyTable() {
        const { filters, filteredCompanies } = this.state;

        const columns = [
            {
                title: Locale.trans('hostCompany.name'),
                key: 'name',
                sorter: sortNameColumn,
                filterIcon: <FilterIcon active={filters.name.length > 0} />,
                render: this.renderCompanyNameCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="name"
                        selectedValues={filters.name}
                        values={this.getCompanyNames().map(r => ({ text: r, value: r }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: null,
                key: 'actions',
                width: '50px',
                render: this.rendActionsCell,
            },
        ];

        return (
            <Table
                dataSource={filteredCompanies}
                rowKey="id"
                columns={columns}
                locale={Locale.Table}
                ref={(r) => {
                    this.table = r;
                }}
                loading={this.state.loading && { indicator: <LoadingIcon /> }}
            />
        );
    }

    renderCompanyNameCell = (company: IHostCompany) => company.name;

    rendActionsCell = (company: IHostCompany) => (
        <React.Fragment>
            <Button
                type="primary"
                shape="circle"
                icon="edit"
                onClick={(e) => {
                    this.editCompany(company);
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                }}
            />
        </React.Fragment>
    );
}
