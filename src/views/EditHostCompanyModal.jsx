// @flow

import React from 'react';
import { Modal } from 'antd';

import HostCompanyForm from 'components/forms/HostCompanyForm.jsx';

import ToastActions from 'actions/ToastActions';

import HostCompanyActions from 'actions/HostCompanyActions';

import UserActions from 'actions/UserActions';
import UserStore from 'stores/UserStore';

import UserTypeActions from 'actions/UserTypeActions';
import UserTypeStore from 'stores/UserTypeStore';

type Prop = {
    onCancel: () => void,
    visible: boolean,
    hostCompany: ?IHostCompany,
};
type State = {
    loading: boolean,
    fields: Object,
    users: IUser[],
    userTypes: IUserType[]
};

/**
 * The modal to edit a host company.
 */
export default class EditHostCompanyModal extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super();
        this.state = {
            loading: false,
            fields: this.getFieldsFromEntity(props.hostCompany),
            users: UserStore.getAll(),
            userTypes: UserTypeStore.getAll(),
        };
    }

    componentDidMount() {
        this.userListener = UserStore.addListener(this.receiveUsers);
        this.userTypeListener = UserTypeStore.addListener(this.receiveUserTypes);
        UserActions.reload();
        UserTypeActions.reload();
    }

    componentWillUnmount() {
        this.userListener.remove();
        this.userTypeListener.remove();
    }

    receiveUsers = () => {
        this.setState({
            users: UserStore.getAll(),
        });
    };

    receiveUserTypes = () => {
        this.setState({
            userTypes: UserTypeStore.getAll(),
        });
    };

    componentWillReceiveProps(nextProps: Prop) {
        if (nextProps.visible && !this.props.visible) {
            this.setState({
                fields: this.getFieldsFromEntity(nextProps.hostCompany),
            });
        }
    }

    getFieldsFromEntity = (entity: Entity) => {
        if (!entity) {
            return {};
        }
        const fields = {};
        const keys = Object.keys(entity);
        for (let i = 0; i < keys.length; i++) {
            const k = keys[i];
            fields[k] = { value: entity[k] };
        }
        return fields;
    };

    handleFormChange = (changedFields: Object) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    handleSubmit = (company: IHostCompany) => {
        if (!this.props.hostCompany) {
            return;
        }
        const companyId = this.props.hostCompany.id;

        this.setState({
            loading: true,
        });
        HostCompanyActions.edit(companyId, company)
            .then((newCompany: IHostCompany) => {
                this.setState({
                    fields: {},
                    loading: false,
                });
                ToastActions.createToastSuccess(`Entreprise "${newCompany.name}" modifiée`);
                this.props.onCancel();
            })
            .catch(this.handleError);
    };

    handleError = (err) => {
        this.setState({
            loading: false,
        });
        try {
            const resp = JSON.parse(err.response);
            ToastActions.createToastError(resp.message);
        } catch (e) {
            ToastActions.createToastError('Une erreur est survenue');
        }
    };

    render() {
        const { hostCompany, visible, onCancel } = this.props;
        const { fields, loading, users, userTypes } = this.state;
        return (
            <Modal
                title="Modifier une Entreprise d'Accueil"
                visible={hostCompany && visible}
                onCancel={onCancel}
                footer={null}
            >
                <HostCompanyForm
                    hostCompany={hostCompany}
                    users={users}
                    userTypes={userTypes}
                    onChange={this.handleFormChange}
                    fields={fields}
                    onSubmit={this.handleSubmit}
                    loading={loading}
                />
            </Modal>
        );
    }
}
