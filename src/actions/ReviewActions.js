// @flow

import dispatcher from "../dispatchers/AppDispatcher";
import ActionsBase from "../actions/ActionsBase";

import ReviewConstants from "../constants/ReviewConstants";
import ReviewService from "../services/ReviewService";

import CacheManager from "../services/CacheManager";

const CACHE_DURATION = 30 * 1000;

class ReviewActions extends ActionsBase {
    create = (review: Object): Promise<*> => {
        const $this = this;
        return new Promise(
            (
                resolve: (review: IReview) => void,
                reject: (response: Object) => void
            ): void => {
                function handleSuccess(newReview: IReview): void {
                    dispatcher.dispatch({
                        type: ReviewConstants.RECEIVE_REVIEW,
                        payload: {
                            review: newReview
                        }
                    });
                    resolve(newReview);
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                ReviewService.post(review)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    edit = (reviewId: number, review: Object): Promise<*> => {
        const $this = this;
        return new Promise(
            (
                resolve: (review: IReview) => void,
                reject: (response: Object) => void
            ): void => {
                function handleSuccess(newReview: IReview): void {
                    dispatcher.dispatch({
                        type: ReviewConstants.RECEIVE_REVIEW,
                        payload: {
                            review: newReview
                        }
                    });
                    resolve(newReview);
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                ReviewService.patch(reviewId, review)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    delete = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(): void {
                    dispatcher.dispatch({
                        type: ReviewConstants.DELETE_REVIEW,
                        payload: {
                            id
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                ReviewService.remove(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reload = (): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                if (
                    CacheManager.isCached("Review:reload", "", CACHE_DURATION)
                ) {
                    resolve();
                    return;
                }

                function handleSuccess(reviews: Array<IReview>): void {
                    dispatcher.dispatch({
                        type: ReviewConstants.RECEIVE_REVIEWS,
                        payload: {
                            reviews
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                ReviewService.getAll()
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    changeCurrentReviewId = (reviewId: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                dispatcher.dispatch({
                    type: ReviewConstants.CHANGE_CURRENT_REVIEW,
                    payload: {
                        reviewId
                    }
                });
                resolve();
            }
        );
    };

    reloadByUser = (user: Entity): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                if (
                    CacheManager.isCached(
                        "Review:reloadByUser",
                        user.id.toString(),
                        CACHE_DURATION
                    )
                ) {
                    resolve();
                    return;
                }

                function handleSuccess(reviews: Array<IReview>): void {
                    dispatcher.dispatch({
                        type: ReviewConstants.RECEIVE_REVIEWS_OF_USER,
                        payload: {
                            user,
                            reviews
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                ReviewService.getByUser(user.id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reloadById = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(review: IReview): void {
                    dispatcher.dispatch({
                        type: ReviewConstants.RECEIVE_REVIEW,
                        payload: {
                            review
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                ReviewService.getById(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };
}

export default new ReviewActions();