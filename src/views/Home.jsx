// @flow

import React from 'react';

import Panel from 'components/Panel.jsx';

type Prop = {};
type State = {};

export default class Home extends React.Component<Prop, State> {
    constructor() {
        super();
        this.state = {};
    }

    render() {
        return (
            <Panel title="Accueil">
                <div>Bienvenue sur Handiwork.</div>
            </Panel>
        );
    }
}
