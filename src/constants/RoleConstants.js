export default {
    RECEIVE_ROLE: 'RECEIVE_ROLE',
    RECEIVE_ROLES: 'RECEIVE_ROLES',
    RECEIVE_ROLES_OF_USER: 'RECEIVE_ROLES_OF_USER',
    CHANGE_CURRENT_ROLE: 'CHANGE_CURRENT_ROLE'
}