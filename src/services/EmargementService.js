// @flow

import ServiceBase from '../services/ServiceBase';
import BaseUrlConstants from '../constants/BaseUrlConstants';
import TokenContainer from '../services/TokenContainer';

const URL: string = `${BaseUrlConstants.BASE_URL}`;

class EmargementService extends ServiceBase {
    constructor() {
        super(URL);
    }

    /**
     * Get a emargement by unique identifier.
     * @param {number} emargementId The identifier of the emargement.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getById(emargementId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}emargements/${emargementId}`,
            method: 'GET',
        });
    }

    /**
     * Get emargement of a mission.
     * @param {number} missionId The identifier of the mission.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getByMission(missionId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}missions/${missionId}/emargements`,
            method: "GET"
        });
    }

    /**
     * Get all emargements.
     *
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getAll(): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}emargements`,
            method: 'GET',
        });
    }

    /**
     * Post a new emargement to an user.
     * @param {Object} emargement The emargement to create.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    post(emargement: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}emargements`,
            method: 'POST',
            data: emargement,
        });
    }

    /**
     * Patch an existing resource. Only the properties that are set on the patch will be updated.
     * @param {number} emargementId The identifier of the emargement.
     * @param {Object} patch The properties to update.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    patch(emargementId: number, patch: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}emargements/${emargementId}`,
            method: 'PATCH',
            data: patch,
        });
    }

    /**
     * Delete an existing emargement.
     * @param {number} emargementId The identifier of the emargement.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    remove(emargementId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}emargements/${emargementId}`,
            method: 'DELETE',
        });
    }

    /**
     * Download Mission's emargements in PDF format.
     * @param  {number} missionId The ID of the mission.
     * @return {[type]}            [description]
     */
    download(missionId: number, groupBy: ?string) {
        const token = TokenContainer.get().replace(/[+]/g, '%2B');
        const url = `${BaseUrlConstants.BASE_URL}missions/${missionId}/emargements.pdf`;
        const groupByParam = groupBy? '&group-by='+groupBy : '';
        /* global window */
        window.location = `${url}?X-Auth-Token=${token}${groupByParam}`;
    }
}

export default new EmargementService();
