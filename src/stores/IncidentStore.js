// @flow

import EntityStoreBase from "../stores/EntityStoreBase";
import IncidentConstants from "../constants/IncidentConstants";

type State = {
    entities: IIncident[]
};

class IncidentStore extends EntityStoreBase<IIncident> {
    getInitialState(): State {
        return {
            entities: []
        };
    }

    reduce(state: State, action: { type: string, payload: Object }): State {
        const { type, payload } = action;
        let newState = Object.assign({}, state);

        switch (type) {
            case IncidentConstants.RECEIVE_INCIDENT:
                newState = this._receiveEntity(state, payload.incident);
                break;
            case IncidentConstants.RECEIVE_INCIDENTS:
                newState = this._receiveMany(state, payload.incidents);
                break;
            case IncidentConstants.DELETE_INCIDENT:
                newState = this._deleteOne(state, payload.id);
                break;
            case "clearAll":
                newState = this.getInitialState();
            default:
                newState = state;
        }
        return newState;
    }

    _receiveMany = (state: State, incidents: IIncident[]) => {
        let newState = Object.assign({}, state);
        // We remove all the entities that have the same ID as the new incidents
        newState.entities = state.entities.filter(
            e => incidents.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...incidents);
        return newState;
    };

    _deleteOne = (state: State, incidentId: number) => {
        const i = state.entities.findIndex(c => c.id === incidentId);
        if (i === -1) {
            return state;
        }
        let newState = Object.assign({}, state);
        newState.entities = state.entities.slice();
        newState.entities.splice(i, 1);
        return newState;
    };

    ///// Public Methods  /////

    getIncident(): ?IIncident {
        return this.getState().incident;
    }

    /**
     * Get a incident by ID.
     * @param {number} id The identifier.
     * @return {?Incident} The incident, or NULL if no entity is found.
     */
    getById = (id: number): ?IIncident => {
        return this.getState().entities.find(e => e.id === id);
    };

    getByUser = (user: IUser): IIncident[] => {
        return user.incidents
            ? user.incidents.map(b => this.getById(b.id)).filter(b => !!b)
            : [];
    };

    /**
     * Get all the incidents.
     */
    getAll = (): Array<IIncident> => this.getState().entities;
}

export default new IncidentStore();