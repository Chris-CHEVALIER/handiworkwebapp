// @flow

import EntityStoreBase from "../stores/EntityStoreBase";
import EstablishmentConstants from "../constants/EstablishmentConstants";

type State = {
    entities: { [mission_id: number]: IEstablishment[] },
    currentEstablishmentId: ?number
};

class EstablishmentStore extends EntityStoreBase<IEstablishment> {
    getInitialState(): State {
        return {
            currentEstablishmentId: null,
            entities: []
        };
    }

    reduce(state: State, action: { type: string, payload: Object }): State {
        const { type, payload } = action;
        let newState = Object.assign({}, state);

        switch (type) {
            case EstablishmentConstants.RECEIVE_ESTABLISHMENTS_OF_MISSION:
                newState = this._receiveByMission(
                    state,
                    payload.mission,
                    payload.establishments
                );
                break;
            case EstablishmentConstants.RECEIVE_ESTABLISHMENT:
                newState = this._receiveEntity(state, payload.establishment);
                break;
            case EstablishmentConstants.RECEIVE_ESTABLISHMENTS:
                newState = this._receiveMany(state, payload.establishments);
                break;
            case EstablishmentConstants.CHANGE_CURRENT_ESTABLISHMENT:
                newState.currentEstablishmentId = payload.establishmentId;
                break;
            case "clearAll":
                newState = this.getInitialState();
            default:
                newState = state;
        }
        return newState;
    }

    _receiveByMission = (state: State, mission: Entity, establishments: IEstablishment[]) => {
        let newState = Object.assign({}, state);
        newState.entities = establishments;
        return newState;
    };

    /*_receiveByMission = (
        state: State,
        mission: IMission,
        establishments: IEstablishment[]
    ) => {
        let newState = Object.assign({}, state);
        newState.entities = state.entities.filter(
            e => establishments.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...establishments);
        return newState;
    };*/

    _receiveMany = (state: State, establishments: IEstablishment[]) => {
        let newState = Object.assign({}, state);
        // We remove all the entities that have the same ID as the new establishments
        newState.entities = state.entities.filter(
            e => establishments.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...establishments);
        return newState;
    };

    ///// Public Methods  /////

    getEstablishment(): ?IEstablishment {
        return this.getState().establishment;
    };

    /**
     * Get all the establishment of a mission.
     */
    getByMission = (mission: IMission): IMission[] => {
        return this.getState().entities.filter(e => !!e.mission.find(a => a.id === mission.id));
    }

    /**
     * Get an establishment by ID.
     * @param {number} id The identifier.
     * @return {?Establishment} The establishment, or NULL if no entity is found.
     */
    getById = (id: number): ?IEstablishment => {
        return this.getState().entities.find(e => e.id === id);
    };

    /**
     * Get all the establishments.
     */
    getAll = (): Array<IEstablishment> => this.getState().entities;

    /**
     * Get the identifier of the current establishment.
     * @return {[type]} [description]
     */
    getCurrentEstablishmentId = (): ?number => {
        return (
            this.getState().currentEstablishmentId ||
            (this.getState().entities.length
                ? this.getState().entities[0].id
                : null)
        );
    };

    /**
     * Get the identifier of the current establishment.
     * @return {[type]} [description]
     */
    getCurrentEstablishment = (): IEstablishment => {
        return this.getById(this.getCurrentEstablishmentId());
    };
}

export default new EstablishmentStore();
