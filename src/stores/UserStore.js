// @flow

import EntityStoreBase from "../stores/EntityStoreBase";
import UserConstants from "../constants/UserConstants";

type State = {
    entities: { [mission_id: number]: IUser[] },
    currentUserId: ?number
};

class UserStore extends EntityStoreBase<IUser> {
    getInitialState(): State {
        return {
            missionUsers: {},
            currentUserId: null,
            entities: []
        };
    }

    reduce(state: State, action: { type: string, payload: Object }): State {
        const { type, payload } = action;
        let newState = Object.assign({}, state);

        switch (type) {
            case UserConstants.RECEIVE_USERS_OF_MISSION:
                newState = this._receiveByMission(
                    state,
                    payload.mission,
                    payload.users
                );
                break;
            case UserConstants.RECEIVE_USER:
                newState = this._receiveEntity(state, payload.user);
                break;
            case UserConstants.RECEIVE_USERS:
                newState = this._receiveMany(state, payload.users);
                break;
            case UserConstants.CHANGE_CURRENT_USER:
                newState.currentUserId = payload.userId;
                break;
            case "clearAll":
                newState = this.getInitialState();
            default:
                newState = state;
        }
        return newState;
    }

    _receiveByMission = (state: State, mission: IMission, users: IUser[]) => {
        let newState = Object.assign({}, state);
        newState.entities = state.entities.filter(
            e => users.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...users);
        return newState;
    };

    _receiveMany = (state: State, users: IUser[]) => {
        let newState = Object.assign({}, state);
        // We remove all the entities that have the same ID as the new users
        newState.entities = state.entities.filter(
            e => users.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...users);
        return newState;
    };

    // /// Public Methods  /////

    getUser(): ?IUser {
        return this.getState().user;
    }

    /**
     * Get all the users of a mission.
     */
    getByMission = (mission: IMission): IUser[] => {
        return this.getState().entities.filter(e => !!e.mission.find(a => a.id === mission.id));
    };

    /**
     * Get a user by ID.
     * @param {number} id The identifier of the user.
     * @return {?User}    The user, or NULL if no entity is found.
     */
    getById = (id: number): ?IUser => {
        return this.getState().entities.find(e => e.id === id);
    };

    /**
     * Get all the users.
     */
    getAll = (): Array<IUser> => this.getState().entities;

    /**
     * Get the identifier of the current user.
     * @return {[type]} [description]
     */
    getCurrentUserId = (): ?number => {
        return (
            this.getState().currentUserId ||
            (this.getState().entities.length
                ? this.getState().entities[0].id
                : null)
        );
    };

    /**
     * Get the identifier of the current user.
     * @return {[type]} [description]
     */
    getCurrentUser = (): IUser => this.getById(this.getCurrentUserId());
}

export default new UserStore();
