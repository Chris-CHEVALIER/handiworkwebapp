// @flow

import React from 'react';
import { Modal } from 'antd';

import EstablishmentForm from 'components/forms/EstablishmentForm.jsx';

import ToastActions from 'actions/ToastActions';

import EstablishmentActions from 'actions/EstablishmentActions';

import UserActions from 'actions/UserActions';
import UserStore from 'stores/UserStore';

import UserTypeActions from 'actions/UserTypeActions';
import UserTypeStore from 'stores/UserTypeStore';

type Prop = {
    onCancel: () => void,
    visible: boolean,
};
type State = {
    loading: boolean,
    fields: Object,
    users: IUser[],
    userTypes: IUserType[]
};

/**
 * The modal to create a new establishment.
 */
export default class CreateEstablishmentModal extends React.Component<Prop, State> {
    constructor() {
        super();
        this.state = {
            loading: false,
            fields: {},
            users: UserStore.getAll(),
            userTypes: UserTypeStore.getAll(),
        };
    }

    componentDidMount() {
        this.userListener = UserStore.addListener(this.receiveUsers);
        this.userTypeListener = UserTypeStore.addListener(this.receiveUserTypes);
        UserActions.reload();
        UserTypeActions.reload();
    }

    componentWillUnmount() {
        this.userListener.remove();
        this.userTypeListener.remove();
    }

    receiveUsers = () => {
        this.setState({
            users: UserStore.getAll(),
        });
    };

    receiveUserTypes = () => {
        this.setState({
            userTypes: UserTypeStore.getAll(),
        });
    };

    handleFormChange = (changedFields: Object) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    handleSubmit = (establishment: IEstablishment) => {
        this.setState({
            loading: true,
        });
        EstablishmentActions.create(establishment)
            .then((newEstablishment: IEstablishment) => {
                this.setState({
                    fields: {},
                    loading: false,
                });
                ToastActions.createToastSuccess(`Etablissement "${newEstablishment.name}" créé`);
                this.props.onCancel();
            })
            .catch(this.handleError);
    };

    handleError = (err: Object) => {
        this.setState({
            loading: false,
        });
        try {
            const resp = JSON.parse(err.response);
            ToastActions.createToastError(resp.message);
        } catch (e) {
            ToastActions.createToastError('Une erreur est survenue');
        }
    };

    render() {
        const { visible, onCancel } = this.props;
        const { fields, loading, users, userTypes } = this.state;
        return (
            <Modal
                title="Ajouter un Etablissement"
                visible={visible}
                onCancel={onCancel}
                footer={null}
            >
                <EstablishmentForm
                    users={users}
                    userTypes={userTypes}
                    onChange={this.handleFormChange}
                    fields={fields}
                    onSubmit={this.handleSubmit}
                    loading={loading}
                />
            </Modal>
        );
    }
}
