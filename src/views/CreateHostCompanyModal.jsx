// @flow

import React from 'react';
import { Modal } from 'antd';

import HostCompanyForm from 'components/forms/HostCompanyForm.jsx';

import ToastActions from 'actions/ToastActions';

import HostCompanyActions from 'actions/HostCompanyActions';

import UserActions from 'actions/UserActions';
import UserStore from 'stores/UserStore';

import UserTypeActions from 'actions/UserTypeActions';
import UserTypeStore from 'stores/UserTypeStore';

type Prop = {
    onCancel: () => void,
    visible: boolean,
};
type State = {
    loading: boolean,
    fields: Object,
    users: IUser[],
    userTypes: IUserType[]
};

/**
 * The modal to create a new host company.
 */
export default class CreateHostCompanyModal extends React.Component<Prop, State> {
    constructor() {
        super();
        this.state = {
            loading: false,
            fields: {},
            users: UserStore.getAll(),
            userTypes: UserTypeStore.getAll(),
        };
    }

    componentDidMount() {
        this.userListener = UserStore.addListener(this.receiveUsers);
        this.userTypeListener = UserTypeStore.addListener(this.receiveUserTypes);
        UserActions.reload();
        UserTypeActions.reload();
    }

    componentWillUnmount() {
        this.userListener.remove();
        this.userTypeListener.remove();
    }

    receiveUsers = () => {
        this.setState({
            users: UserStore.getAll(),
        });
    };

    receiveUserTypes = () => {
        this.setState({
            userTypes: UserTypeStore.getAll(),
        });
    };

    handleFormChange = (changedFields: Object) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    handleSubmit = (company: IHostCompany) => {
        this.setState({
            loading: true,
        });
        HostCompanyActions.create(company)
            .then((newCompany: IHostCompany) => {
                this.setState({
                    fields: {},
                    loading: false,
                });
                ToastActions.createToastSuccess(`Entreprise "${newCompany.name}" créée`);
                this.props.onCancel();
            })
            .catch(this.handleError);
    };

    handleError = (err: Object) => {
        this.setState({
            loading: false,
        });
        try {
            const resp = JSON.parse(err.response);
            ToastActions.createToastError(resp.message);
        } catch (e) {
            ToastActions.createToastError('Une erreur est survenue');
        }
    };

    render() {
        const { visible, onCancel } = this.props;
        const { fields, loading, users, userTypes } = this.state;
        return (
            <Modal
                title="Ajouter une Entreprise d'Accueil"
                visible={visible}
                onCancel={onCancel}
                footer={null}
            >
                <HostCompanyForm
                    users={users}
                    userTypes={userTypes}
                    onChange={this.handleFormChange}
                    fields={fields}
                    onSubmit={this.handleSubmit}
                    loading={loading}
                />
            </Modal>
        );
    }
}
