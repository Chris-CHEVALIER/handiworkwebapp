// @flow

import dispatcher from '../dispatchers/AppDispatcher';
import ActionsBase from '../actions/ActionsBase';

import RoleConstants from '../constants/RoleConstants';
import RoleService from '../services/RoleService';

import CacheManager from '../services/CacheManager';

const CACHE_DURATION = 30 * 1000;

class RoleActions extends ActionsBase {
    changeCurrentRoleId = (roleId: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                dispatcher.dispatch({
                    type: RoleConstants.CHANGE_CURRENT_ROLE,
                    payload: {
                        roleId
                    }
                });
                resolve();
            }
        );
    };

    reload = (): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(roles: Array<Role>): void {
                dispatcher.dispatch({
                    type: RoleConstants.RECEIVE_ROLES,
                    payload: {
                        roles,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            RoleService.getAll()
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reloadById = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(role: IRole): void {
                    dispatcher.dispatch({
                        type: RoleConstants.RECEIVE_ROLE,
                        payload: {
                            role
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                RoleService.getById(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reloadByUser = (user: Entity): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                if (
                    CacheManager.isCached(
                        "Role:reloadByUser",
                        user.id.toString(),
                        CACHE_DURATION
                    )
                ) {
                    resolve();
                    return;
                }

                function handleSuccess(roles : Array<IRole>): void {
                    dispatcher.dispatch({
                        type: RoleConstants.RECEIVE_ROLES_OF_USER,
                        payload: {
                            user,
                            roles
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                RoleService.getByUser(user.id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };
}

export default new RoleActions();