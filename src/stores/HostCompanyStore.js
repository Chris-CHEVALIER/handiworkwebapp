// @flow

import EntityStoreBase from "../stores/EntityStoreBase";
import HostCompanyConstants from "../constants/HostCompanyConstants";

type State = {
    entities: { [mission_id: number]: IHostCompany[] },
    currentHostCompanyId: ?number,
};

class HostCompanyStore extends EntityStoreBase<IHostCompany> {
    getInitialState(): State {
        return {
            currentHostCompanyId: null,
            entities: []
        };
    }

    reduce(state: State, action: { type: string, payload: Object }): State {
        const { type, payload } = action;
        let newState = Object.assign({}, state);

        switch (type) {
            case HostCompanyConstants.RECEIVE_HOST_COMPANIES_OF_MISSION:
                newState = this._receiveByMission(
                    state,
                    payload.mission,
                    payload.hostCompanies
                );
                break;
            case HostCompanyConstants.RECEIVE_HOST_COMPANY:
                newState = this._receiveEntity(state, payload.hostCompany);
                break;
            case HostCompanyConstants.RECEIVE_HOST_COMPANIES:
                newState = this._receiveMany(state, payload.hostCompanies);
                break;
                
            case HostCompanyConstants.CHANGE_CURRENT_HOST_COMPANY:
                newState.currentHostCompanyId = payload.hostCompanyId;
                break;
            case "clearAll":
                newState = this.getInitialState();
            default:
                newState = state;
        }
        return newState;
    }

    _receiveByMission = (state: State, mission: Entity, hostCompanies: IHostCompany[]) => {
        let newState = Object.assign({}, state);
        newState.entities = hostCompanies;
        return newState;
    };

    _receiveMany = (state: State, hostCompanies: IHostCompany[]) => {
        let newState = Object.assign({}, state);
        // We remove all the entities that have the same ID as the new host companies
        newState.entities = state.entities.filter(
            e => hostCompanies.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...hostCompanies);
        return newState;
    };

    ///// Public Methods  /////

    getHostCompany(): ?IHostCompany {
        return this.getState().hostCompany;
    }

    /**
     * Get all the host companies of a mission.
     */
    getByMission = (mission: IMission): IMission[] => {
        return this.getState().entities.filter(e => !!e.mission.find(a => a.id === mission.id));
    }

    /**
     * Get a host company by ID.
     * @param {number} id The identifier.
     * @return {?HostCompany}    The host company, or NULL if no entity is found.
     */
    getById = (id: number): ?IHostCompany => {
        return this.getState().entities.find(e => e.id === id);
    }

    /**
     * Get all the host companies.
     */
    getAll = (): Array<IHostCompany> => this.getState().entities;

    /**
     * Get the identifier of the current host company.
     * @return {[type]} [description]
     */
    getCurrentHostCompanyId = (): ?number => {
        return this.getState().currentHostCompanyId ||
        (this.getState().entities.length
            ? this.getState().entities[0].id
            : null);
    }

    /**
     * Get the identifier of the current host company.
     * @return {[type]} [description]
     */
    getCurrentHostCompany = (): IHostCompany => {
        return this.getById(this.getCurrentHostCompanyId());
    }
}

export default new HostCompanyStore();