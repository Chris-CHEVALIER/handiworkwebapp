// @flow

import EntityStoreBase from "../stores/EntityStoreBase";
import ReviewConstants from "../constants/ReviewConstants";

type State = {
    entities: { [user_id: number]: IReview[] },
    currentReviewId: ?number
};

class ReviewStore extends EntityStoreBase<IReview> {
    getInitialState(): State {
        return {
            entities: []
        };
    }

    reduce(state: State, action: { type: string, payload: Object }): State {
        const { type, payload } = action;
        let newState = Object.assign({}, state);

        switch (type) {
            case ReviewConstants.RECEIVE_REVIEWS_OF_USER:
                newState = this._receiveByUser(
                    state,
                    payload.user,
                    payload.reviews
                );
                break;
            case ReviewConstants.RECEIVE_REVIEW:
                newState = this._receiveEntity(state, payload.review);
                break;
            case ReviewConstants.RECEIVE_REVIEWS:
                newState = this._receiveMany(state, payload.reviews);
                break;
            case ReviewConstants.DELETE_REVIEW:
                newState = this._deleteOne(state, payload.id);
                break;
            case "clearAll":
                newState = this.getInitialState();
            default:
                newState = state;
        }
        return newState;
    }

    _receiveByUser = (state: State, user: IUser, reviews: IReview[]) => {
        let newState = Object.assign({}, state);
        newState.entities = state.entities.filter(
            e => reviews.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...reviews);
        return newState;
    };

    _receiveMany = (state: State, reviews: IReview[]) => {
        let newState = Object.assign({}, state);
        // We remove all the entities that have the same ID as the new reviews
        newState.entities = state.entities.filter(
            e => reviews.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...reviews);
        return newState;
    };

    _deleteOne = (state: State, reviewId: number) => {
        const i = state.entities.findIndex(c => c.id === reviewId);
        if (i === -1) {
            return state;
        }
        let newState = Object.assign({}, state);
        newState.entities = state.entities.slice();
        newState.entities.splice(i, 1);
        return newState;
    };

    ///// Public Methods  /////

    getReview(): ?IReview {
        return this.getState().review;
    }

    /**
     * Get all the reviews of an user.
     */
    getByUser = (user: IUser): IReview[] => {
        return this.getState().entities.filter(e => !!e.user.find(a => a.id === user.id));
    };

    /**
     * Get a review by ID.
     * @param {number} id The identifier.
     * @return {?Review} The review, or NULL if no entity is found.
     */
    getById = (id: number): ?IReview => {
        return this.getState().entities.find(e => e.id === id);
    };

    /**
     * Get all the reviews.
     */
    getAll = (): Array<IReview> => this.getState().entities;

    /**
     * Get the identifier of the current review.
     * @return {[type]} [description]
     */
    getCurrentReviewId = (): ?number => {
        return (
            this.getState().currentReviewId ||
            (this.getState().entities.length
                ? this.getState().entities[0].id
                : null)
        );
    };

    /**
     * Get the identifier of the current review.
     * @return {[type]} [description]
     */
    getCurrentReview = (): IReview =>
        this.getById(this.getCurrentReviewId());
}

export default new ReviewStore();