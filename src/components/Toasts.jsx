// @flow

import React from 'react';
import { notification } from 'antd';

import ToastStore from 'stores/ToastStore';

type State = {
    toasts: Array<Toast>,
};

export default class Toasts extends React.Component<void, void, State> {
    state: State;

    constructor() {
        super();
        this.state = {
            toasts: ToastStore.getAll(),
        };
    }

    componentWillMount(): void {
        this.toastListener = ToastStore.addListener(this.getToasts);
    }

    componentWillUnmount(): void {
        this.toastListener.remove();
    }

    componentDidUpdate(): void {
        this.state.toasts.map((toast: Toast): void => {
            notification[toast.type]({
                message: toast.message,
                description: toast.description,
            });
        });
        this.state.toasts = [];
    }

    getToasts = (): void => {
        this.setState({
            toasts: ToastStore.getAll(),
        });
    };

    render() {
        return null;
    }
}
