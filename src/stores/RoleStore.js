// @flow

import EntityStoreBase from "../stores/EntityStoreBase";
import RoleConstants from "../constants/RoleConstants";

type State = {
    entities: { [user_id: number]: IRole[] },
    currentRoleId: ?number,
};

class RoleStore extends EntityStoreBase<IRole> {
    getInitialState(): State {
        return {
            currentRoleId: null,
            entities: []
        };
    }

    reduce(state: State, action: { type: string, payload: Object }): State {
        const { type, payload } = action;
        let newState = Object.assign({}, state);

        switch (type) {
            case RoleConstants.RECEIVE_ROLES_OF_USER:
                newState = this._receiveByUser(
                    state,
                    payload.user,
                    payload.roles
                );
                break;
            case RoleConstants.RECEIVE_ROLE:
                newState = this._receiveEntity(state, payload.role);
                break;
            case RoleConstants.RECEIVE_ROLES:
                newState = this._receiveMany(state, payload.roles);
                break;
            case RoleConstants.CHANGE_CURRENT_ROLE:
                newState.currentRoleId = payload.roleId;
                break;
            case "clearAll":
                newState = this.getInitialState();
            default:
                newState = state;
        }
        return newState;
    }

    _receiveByUser = (state: State, user: IUser, roles: IRole[]) => {
        let newState = Object.assign({}, state);
        newState.entities = state.entities.filter(
            e => e.user.id !== user.id
        );
        newState.entities.push(roles);
        return newState;
    };

    _receiveMany = (state: State, roles: IRole[]) => {
        let newState = Object.assign({}, state);
        // We remove all the entities that have the same ID as the new roles
        newState.entities = state.entities.filter(
            e => roles.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...roles);
        return newState;
    };

    ///// Public Methods  /////

    getRole(): ?IRole {
        return this.getState().role;
    }

    /**
     * Get a role by ID.
     * @param {number} id The identifier.
     * @return {?Role}    The role, or NULL if no entity is found.
     */
    getById = (id: number): ?IRole => {
        return this.getState().entities.find(e => e[0].id === id);
    };

    /**
     * Get all roles of a user.
     */
    getByUser = (user: IUser): IRole[] => {        
        return this.getState().entities;
    };

    /**
     * Get all the roles.
     */
    getAll = (): Array<IRole> => this.getState().entities;

    /**
     * Get the identifier of the current role.
     * @return {[type]} [description]
     */
    getCurrentRoleId = (): ?number => {
        return (
            this.getState().currentRoleId ||
            (this.getState().entities.length
                ? this.getState().entities[0].id
                : null)
        );
    };

    /**
     * Get the identifier of the current role.
     * @return {[type]} [description]
     */
    getCurrentRole = (): IRole =>
        this.getById(this.getCurrentRoleId());
}

export default new RoleStore();