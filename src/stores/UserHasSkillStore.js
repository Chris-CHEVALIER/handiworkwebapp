// @flow

import EntityStoreBase from "../stores/EntityStoreBase";
import UserHasSkillConstants from "../constants/UserHasSkillConstants";

type State = {
    entities: IUserHasSkill[]
};

class UserHasSkillStore extends EntityStoreBase<IUserHasSkill> {
    getInitialState(): State {
        return {
            entities: []
        };
    }

    reduce(state: State, action: { type: string, payload: Object }): State {
        const { type, payload } = action;
        let newState = Object.assign({}, state);

        switch (type) {
            case UserHasSkillConstants.RECEIVE_USER_HAS_SKILL:
                newState = this._receiveEntity(state, payload.userHasSkill);
                break;
            case UserHasSkillConstants.RECEIVE_USER_HAS_SKILLS:
                newState = this._receiveMany(state, payload.userHasSkills);
                break;
            case UserHasSkillConstants.DELETE_USER_HAS_SKILL:
                newState = this._deleteOne(state, payload.id);
                break;
            case "clearAll":
                newState = this.getInitialState();
            default:
                newState = state;
        }
        return newState;
    }

    _receiveMany = (state: State, userHasSkills: IUserHasSkill[]) => {
        let newState = Object.assign({}, state);
        // We remove all the entities that have the same ID as the new userHasSkills
        newState.entities = state.entities.filter(
            e => userHasSkills.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...userHasSkills);
        return newState;
    };

    _deleteOne = (state: State, userHasSkillId: number) => {
        const i = state.entities.findIndex(c => c.id === userHasSkillId);
        if (i === -1) {
            return state;
        }
        let newState = Object.assign({}, state);
        newState.entities = state.entities.slice();
        newState.entities.splice(i, 1);
        return newState;
    };

    ///// Public Methods  /////

    getUserHasSkill(): ?IUserHasSkill {
        return this.getState().userHasSkill;
    };

    /**
     * Get a userHasSkill by ID.
     * @param {number} id The identifier.
     * @return {?UserHasSkill} The userHasSkill, or NULL if no entity is found.
     */
    getById = (id: number): ?IUserHasSkill => {
        return this.getState().entities.find(e => e.id === id);
    };

    /**
     * Get all the userHasSkills.
     */
    getAll = (): Array<IUserHasSkill> => {
        return this.getState().entities;
    }
}

export default new UserHasSkillStore();