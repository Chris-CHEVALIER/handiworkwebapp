// @flow

import React from 'react';

import { Input, Table, Icon, Button, Row, Col, Popover } from 'antd';
import TableColumnFilter from 'components/TableColumnFilter.jsx';
import FilterIcon from 'components/FilterIcon.jsx';
import CreateSubSkillModal from 'views/CreateSubSkillModal.jsx';
import EditSubSkillModal from 'views/EditSubSkillModal.jsx';
import LoadingIcon from 'components/LoadingIcon.jsx';

import Locale from 'locale/LocaleFactory';
import ArrayService from 'services/utils/ArrayService';
import StringService from 'services/utils/StringService';
import FilterService from 'services/utils/FilterService';

import SkillActions from 'actions/SkillActions';
import SkillStore from 'stores/SkillStore';

import SubSkillActions from 'actions/SubSkillActions';
import SubSkillStore from 'stores/SubSkillStore';

// Sorting Methods
function sortNameColumn(c1: ISubSkill, c2: ISubSkill) {
    return StringService.compareCaseInsensitive(c1.name, c2.name);
}
function sortSkillColumn(c1: ISubSkill, c2: ISubSkill) {
    const skill1 = c1.skill && SkillStore.getById(c1.skill.id);
    const skill2 = c2.skill && SkillStore.getById(c2.skill.id);
    const s1 = skill1 && skill1.name;
    const s2 = skill2 && skill2.name;
    return StringService.compareCaseInsensitive(s1, s2);
}

type Prop = {};
type State = {
    loading: boolean,
    createSubSkillVisible: boolean,
    editSubSkillVisible: boolean,
    subSkillToEdit: ?ISubSkill,

    subSkills: ISubSkill[],
    filteredSubSkills: ISubSkill[],

    // Filters
    filterGlobal: string,
    filters: Object,
};

/**
 * The list of the host subskills.
 */
export default class SubSkillList extends React.Component<Prop, State> {
    subSkillListener: *;
    table: *;

    constructor() {
        super();

        const subSkills = SubSkillStore.getAll();

        this.state = {
            loading: !subSkills.length,
            subSkills,

            filteredSubSkills: [],

            filterGlobal: '',
            filters: {
                name: [],
                skill: [],
            },

            createSubSkillVisible: false,
            editSubSkillVisible: false,
            subSkillToEdit: null,
        };
    }

    componentDidMount() {
        this.skillListener = SkillStore.addListener(this.receiveSkills);
        this.subSkillListener = SubSkillStore.addListener(this.receiveSubSkills);

        this.loadSubSkills();
        SkillActions.reload();

        this.updateFilters();
        // Here we set the default sorted column
        // Temporary solution waiting for AntD to propose a native way to do it.
        const column = this.table.findColumn('reference');
        this.table.toggleSortOrder('descend', column);
    }
    componentWillUnmount() {
        this.subSkillListener.remove();
    }

    loadSubSkills = () => {
        this.setState({
            loading: true,
        });
        SubSkillActions.reload().then(() => {
            this.setState({
                loading: false,
            });
        });
    };

    receiveSkills = () => {
        this.forceUpdate();
    };

    receiveSubSkills = () => {
        const subSkills = SubSkillStore.getAll();
        this.setState(
            {
                subSkills,
            },
            this.updateFilters,
        );
    };

    // Filters
    updateFilters = () => {
        const { subSkills } = this.state;
        const filteredSubSkills = subSkills.filter(this.subSkillMatchFilters);
        this.setState({ filteredSubSkills });
    };

    subSkillMatchFilters = (s: ISubSkill) => {
        const { filters } = this.state;
        return (
            FilterService.matchFilter(filters.name, s.name) &&
            FilterService.matchFilter(filters.skill, s.skill && s.skill.id) &&
            this.matchGlobalSearch(s)
        );
    };

    searchGlobal = (e) => {
        this.state.filterGlobal = e.target.value.toLowerCase();
        this.updateFilters();
    };

    matchGlobalSearch = (subSkill: ISubSkill) => {
        const { filterGlobal } = this.state;
        return subSkill.name.toLowerCase().indexOf(filterGlobal) > -1;
    };

    handleFilterChange = (name: string, values: Array<string | number>) => {
        this.state.filters[name] = values;
        this.updateFilters();
    };

    getSubSkillNames = () => ArrayService.unique(this.state.subSkills.map(s => s.name));

    getSkills = () => SkillStore.getAll();

    showCreateSubSkillModal = () => {
        this.setState({
            createSubSkillVisible: true,
        });
    };
    hideCreateSubSkillModal = () => {
        this.setState({
            createSubSkillVisible: false,
        });
    };

    editSubSkill = (subSkill: ISubSkill) => {
        this.setState({
            editSubSkillVisible: true,
            subSkillToEdit: subSkill,
        });
    };
    hideEditSubSkillModal = () => {
        this.setState({
            editSubSkillVisible: false,
            subSkillToEdit: null,
        });
    };

    render() {
        const { subSkillToEdit, loading } = this.state;
        return (
            <div className="subskill-list">
                {this.renderSubSkillTable()}

                {!loading && (
                    <div className="actions-row" style={{ marginTop: !filteredSubSkills || filteredSubSkills.length === 0 ? '10px' : '-50px' }}>
                        <Button type="primary" icon="plus" onClick={this.showCreateSubSkillModal}>
                            Ajouter une compétence
                        </Button>
                    </div>
                )}

                <CreateSubSkillModal
                    onCancel={this.hideCreateSubSkillModal}
                    visible={this.state.createSubSkillVisible}
                />
                <EditSubSkillModal
                    subSkill={subSkillToEdit}
                    onCancel={this.hideEditSubSkillModal}
                    visible={this.state.editSubSkillVisible}
                />
            </div>
        );
    }

    renderSubSkillTable() {
        const { filters, filteredSubSkills } = this.state;

        const columns = [
            {
                title: Locale.trans('subSkill.name'),
                key: 'name',
                sorter: sortNameColumn,
                filterIcon: <FilterIcon active={filters.name.length > 0} />,
                render: this.renderSubSkillNameCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="name"
                        selectedValues={filters.name}
                        values={this.getSubSkillNames().map(r => ({ text: r, value: r }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: Locale.trans('subSkill.skill'),
                key: 'skill',
                sorter: sortSkillColumn,
                filterIcon: <FilterIcon active={filters.skill.length > 0} />,
                render: this.renderSkillCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="skill"
                        selectedValues={filters.skill}
                        values={this.getSkills().map(s => ({ text: s.name, value: s.id }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: null,
                key: 'actions',
                width: '50px',
                render: this.rendActionsCell,
            },
        ];

        return (
            <Table
                dataSource={filteredSubSkills}
                rowKey="id"
                columns={columns}
                locale={Locale.Table}
                ref={(r) => {
                    this.table = r;
                }}
                loading={this.state.loading && { indicator: <LoadingIcon /> }}
            />
        );
    }

    renderSkillCell = (subSkill: ISubSkill) => {
        if (!subSkill.skill) {
            return null;
        }
        const skill = SkillStore.getById(subSkill.skill.id);
        if (!skill) {
            return null;
        }
        return skill.name;
    };

    renderSubSkillNameCell = (subSkill: ISubSkill) => subSkill.name;

    rendActionsCell = (subSkill: ISubSkill) => (
        <React.Fragment>
            <Button
                type="primary"
                shape="circle"
                icon="edit"
                onClick={(e) => {
                    this.editSubSkill(subSkill);
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                }}
            />
        </React.Fragment>
    );
}
