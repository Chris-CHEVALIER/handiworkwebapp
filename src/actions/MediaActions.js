// @flow

import dispatcher from '../dispatchers/AppDispatcher';
import ActionsBase from '../actions/ActionsBase';

import MediaConstants from '../constants/MediaConstants';
import MediaService from '../services/MediaService';

import CacheManager from '../services/CacheManager';

const CACHE_DURATION = 30 * 1000;

class MediaActions extends ActionsBase {
    create = (media: Object): Promise<*> => {
        const $this = this;
        return new Promise((resolve: (media: IMedia) => void, reject: (response: Object) => void): void => {
            function handleSuccess(newMedia: IMedia): void {
                dispatcher.dispatch({
                    type: MediaConstants.RECEIVE_MEDIA,
                    payload: {
                        media: newMedia,
                    },
                });
                resolve(newMedia);
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            MediaService.post(media)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    edit = (mediaId: number, media: Object): Promise<*> => {
        const $this = this;
        return new Promise((resolve: (media: IMedia) => void, reject: (response: Object) => void): void => {
            function handleSuccess(newMedia: IMedia): void {
                dispatcher.dispatch({
                    type: MediaConstants.RECEIVE_MEDIA,
                    payload: {
                        media: newMedia,
                    },
                });
                resolve(newMedia);
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            MediaService.patch(mediaId, media)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    delete = (id: number): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(): void {
                dispatcher.dispatch({
                    type: MediaConstants.DELETE_MEDIA,
                    payload: {
                        id,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            MediaService.remove(id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reload = (): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            if (CacheManager.isCached('Media:reload', '', CACHE_DURATION)) {
                resolve();
                return;
            }

            function handleSuccess(medias: Array<IMedia>): void {
                dispatcher.dispatch({
                    type: MediaConstants.RECEIVE_MEDIAS,
                    payload: {
                        medias,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            MediaService.getAll()
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reloadByReport = (report: Entity): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            if (
                CacheManager.isCached(
                    'Media:reloadByReport',
                    report.id.toString(),
                    CACHE_DURATION,
                )
            ) {
                resolve();
            }

            function handleSuccess(medias: Array<IMedia>): void {
                dispatcher.dispatch({
                    type: MediaConstants.RECEIVE_MEDIAS_OF_REPORT,
                    payload: {
                        report,
                        medias,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            MediaService.getByReport(report.id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reloadByIncident = (incident: Entity): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            if (
                CacheManager.isCached(
                    'Media:reloadByIncident',
                    incident.id.toString(),
                    CACHE_DURATION,
                )
            ) {
                resolve();
            }

            function handleSuccess(medias: Array<IMedia>): void {
                dispatcher.dispatch({
                    type: MediaConstants.RECEIVE_MEDIAS_OF_INCIDENT,
                    payload: {
                        incident,
                        medias,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            MediaService.getByIncident(incident.id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reloadByReview = (review: Entity): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            if (
                CacheManager.isCached(
                    'Media:reloadByReview',
                    review.id.toString(),
                    CACHE_DURATION,
                )
            ) {
                resolve();
            }

            function handleSuccess(medias: Array<IMedia>): void {
                dispatcher.dispatch({
                    type: MediaConstants.RECEIVE_MEDIAS_OF_REVIEW,
                    payload: {
                        review,
                        medias,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            MediaService.getByReview(review.id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reloadById = (id: number): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(media: IMedia): void {
                dispatcher.dispatch({
                    type: MediaConstants.RECEIVE_MEDIA,
                    payload: {
                        media,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            MediaService.getById(id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };
}

export default new MediaActions();
