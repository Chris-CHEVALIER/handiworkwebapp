// @flow

import React from 'react';

import { Input, Table, Icon, Button, Row, Col, Popover } from 'antd';
import TableColumnFilter from 'components/TableColumnFilter.jsx';
import FilterIcon from 'components/FilterIcon.jsx';
import CreateEstablishmentModal from 'views/CreateEstablishmentModal.jsx';
import EditEstablishmentModal from 'views/EditEstablishmentModal.jsx';
import LoadingIcon from 'components/LoadingIcon.jsx';

import Locale from 'locale/LocaleFactory';
import ArrayService from 'services/utils/ArrayService';
import StringService from 'services/utils/StringService';
import FilterService from 'services/utils/FilterService';

import EstablishmentActions from 'actions/EstablishmentActions';
import EstablishmentStore from 'stores/EstablishmentStore';

// Sorting Methods
function sortNameColumn(c1: IEstablishment, c2: IEstablishment) {
    return StringService.compareCaseInsensitive(c1.name, c2.name);
}

type Prop = {};
type State = {
    loading: boolean,
    createEstablishmentVisible: boolean,
    editEstablishmentVisible: boolean,
    establishmentToEdit: ?IEstablishment,

    establishments: IEstablishment[],
    filteredEstablishments: IEstablishment[],

    // Filters
    filterGlobal: string,
    filters: Object,
};

/**
 * The list of the host establishments.
 */
export default class EstablishmentList extends React.Component<Prop, State> {
    constructor() {
        super();

        const establishments = EstablishmentStore.getAll();

        this.state = {
            loading: !establishments.length,
            establishments,

            filteredEstablishments: [],

            filterGlobal: '',
            filters: {
                name: [],
            },

            createEstablishmentVisible: false,
            editEstablishmentVisible: false,
            establishmentToEdit: null,
        };
    }

    componentDidMount() {
        this.hostEstablishmentListener = EstablishmentStore.addListener(this.receiveEstablishments);

        this.loadHostEstablishments();

        this.updateFilters();
        // Here we set the default sorted column
        // Temporary solution waiting for AntD to propose a native way to do it.
        const column = this.table.findColumn('reference');
        this.table.toggleSortOrder('descend', column);
    }
    componentWillUnmount() {
        this.hostEstablishmentListener.remove();
    }

    loadHostEstablishments = () => {
        this.setState({
            loading: true,
        });
        EstablishmentActions.reload().then(() => {
            this.setState({
                loading: false,
            });
        });
    };

    receiveEstablishments = () => {
        const establishments = EstablishmentStore.getAll();
        this.setState(
            {
                establishments,
            },
            this.updateFilters,
        );
    };

    // Filters
    updateFilters = () => {
        const { establishments } = this.state;
        const filteredEstablishments = establishments.filter(this.establishmentMatchFilters);
        this.setState({ filteredEstablishments });
    };

    establishmentMatchFilters = (e: IEstablishment) => {
        const { filters } = this.state;
        return (
            FilterService.matchFilter(filters.name, e.name) &&
            this.matchGlobalSearch(e)
        );
    };

    searchGlobal = (e) => {
        this.state.filterGlobal = e.target.value.toLowerCase();
        this.updateFilters();
    };

    matchGlobalSearch = (establishment: IEstablishment) => {
        const { filterGlobal } = this.state;
        return (
            establishment.name.toLowerCase().indexOf(filterGlobal) > -1
        );
    };

    handleFilterChange = (name: string, values: Array<string | number>) => {
        this.state.filters[name] = values;
        this.updateFilters();
    };

    getEstablishmentNames = () =>
        ArrayService.unique(this.state.establishments.map(c => c.name));

    showCreateEstablishmentModal = () => {
        this.setState({
            createEstablishmentVisible: true,
        });
    };
    hideCreateEstablishmentModal = () => {
        this.setState({
            createEstablishmentVisible: false,
        });
    };

    editEstablishment = (establishment: IEstablishment) => {
        this.setState({
            editEstablishmentVisible: true,
            establishmentToEdit: establishment,
        });
    };
    hideEditEstablishmentModal = () => {
        this.setState({
            editEstablishmentVisible: false,
            establishmentToEdit: null,
        });
    };

    render() {
        const { establishmentToEdit, loading } = this.state;
        return (
            <div className="establishment-list">
                {this.renderEstablishmentTable()}

                {!loading && (
                    <div className="actions-row" style={{ marginTop: !filteredEstablishments || filteredEstablishments.length === 0 ? '10px' : '-50px' }}>
                        <Button type="primary" icon="plus" onClick={this.showCreateEstablishmentModal}>
                            Ajouter un établissement
                        </Button>
                    </div>
                )}

                <CreateEstablishmentModal
                    onCancel={this.hideCreateEstablishmentModal}
                    visible={this.state.createEstablishmentVisible}
                />
                <EditEstablishmentModal
                    establishment={establishmentToEdit}
                    onCancel={this.hideEditEstablishmentModal}
                    visible={this.state.editEstablishmentVisible}
                />
            </div>
        );
    }

    renderEstablishmentTable() {
        const { filters, filteredEstablishments } = this.state;

        const columns = [
            {
                title: Locale.trans('establishment.name'),
                key: 'name',
                sorter: sortNameColumn,
                filterIcon: <FilterIcon active={filters.name.length > 0} />,
                render: this.renderEstablishmentNameCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="name"
                        selectedValues={filters.name}
                        values={this.getEstablishmentNames().map(r => ({ text: r, value: r }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: null,
                key: 'actions',
                width: '50px',
                render: this.rendActionsCell,
            },
        ];

        return (
            <Table
                dataSource={filteredEstablishments}
                rowKey="id"
                columns={columns}
                locale={Locale.Table}
                ref={(r) => {
                    this.table = r;
                }}
                loading={this.state.loading && { indicator: <LoadingIcon /> }}
            />
        );
    }

    renderEstablishmentNameCell = (establishment: IEstablishment) => establishment.name;

    rendActionsCell = (establishment: IEstablishment) => (
        <React.Fragment>
            <Button
                type="primary"
                shape="circle"
                icon="edit"
                onClick={(e) => {
                    this.editEstablishment(establishment);
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                }}
            />
        </React.Fragment>
    );
}
