// @flow

import dispatcher from "../dispatchers/AppDispatcher";
import ActionsBase from "../actions/ActionsBase";

import IncidentConstants from "../constants/IncidentConstants";
import IncidentService from "../services/IncidentService";

import CacheManager from "../services/CacheManager";

const CACHE_DURATION = 30 * 1000;

class IncidentActions extends ActionsBase {
    create = (incident: Object): Promise<*> => {
        const $this = this;
        return new Promise(
            (
                resolve: (incident: IIncident) => void,
                reject: (response: Object) => void
            ): void => {
                function handleSuccess(newIncident: IIncident): void {

                    dispatcher.dispatch({
                        type: IncidentConstants.RECEIVE_INCIDENT,
                        payload: {
                            incident: newIncident
                        }
                    });
                    resolve(newIncident);
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                IncidentService.post(incident)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    edit = (incidentId: number, incident: Object): Promise<*> => {
        const $this = this;
        return new Promise(
            (
                resolve: (incident: IIncident) => void,
                reject: (response: Object) => void
            ): void => {
                function handleSuccess(newIncident: IIncident): void {
                    dispatcher.dispatch({
                        type: IncidentConstants.RECEIVE_INCIDENT,
                        payload: {
                            incident: newIncident
                        }
                    });
                    resolve(newIncident);
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                IncidentService.patch(incidentId, incident)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    delete = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(): void {
                    dispatcher.dispatch({
                        type: IncidentConstants.DELETE_INCIDENT,
                        payload: {
                            id
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                IncidentService.remove(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reload = (): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                if (
                    CacheManager.isCached("Incident:reload", "", CACHE_DURATION)
                ) {
                    resolve();
                    return;
                }

                function handleSuccess(incidents: Array<IIncident>): void {
                    dispatcher.dispatch({
                        type: IncidentConstants.RECEIVE_INCIDENTS,
                        payload: {
                            incidents
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                IncidentService.getAll()
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reloadByUser = (user: Entity): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                if (
                    CacheManager.isCached(
                        "Incident:reloadByUser",
                        user.id.toString(),
                        CACHE_DURATION
                    )
                ) {
                    resolve();
                    return;
                }

                function handleSuccess(incidents: Array<IIncident>): void {
                    dispatcher.dispatch({
                        type: IncidentConstants.RECEIVE_INCIDENTS_OF_USER,
                        payload: {
                            user,
                            incidents
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                IncidentService.getByUser(user.id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reloadById = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(incident: IIncident): void {
                    dispatcher.dispatch({
                        type: IncidentConstants.RECEIVE_INCIDENT,
                        payload: {
                            incident
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                IncidentService.getById(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };
}

export default new IncidentActions();