// @flow

import React from 'react';

import { Table, Button } from 'antd';
import TableColumnFilter from '../components/TableColumnFilter.jsx';
import FilterIcon from '../components/FilterIcon.jsx';
import LoadingIcon from '../components/LoadingIcon.jsx';

import ShowReportModal from '../views/ShowReportModal.jsx';

import Locale from '../locale/LocaleFactory';
import ArrayService from '../services/utils/ArrayService';
import DateService from '../services/utils/DateService';
import StringService from '../services/utils/StringService';
import FilterService from '../services/utils/FilterService';

import ReportActions from '../actions/ReportActions';
import ReportStore from '../stores/ReportStore';

import MissionActions from '../actions/MissionActions';
import MissionStore from '../stores/MissionStore';

import UserActions from '../actions/UserActions';
import UserStore from '../stores/UserStore';

// Sorting Methods
function sortDateColumn(r1: IReport, r2: IReport) {
    return StringService.compareCaseInsensitive(r1.date, r2.date);
}

function sortMissionColumn(r1: IReport, r2: IReport) {
    const m1 = MissionStore.getById(r1.mission.id);
    const m2 = MissionStore.getById(r2.mission.id);
    return StringService.compareCaseInsensitive(m1 && m1.name, m2 && m2.name);
}

function sortAuthorColumn(r1: IReport, r2: IReport) {
    const m1 = UserStore.getById(r1.author.id);
    const m2 = UserStore.getById(r2.author.id);
    return StringService.compareCaseInsensitive(
        m1 && m1.firstName + m1.lastName,
        m2 && m2.firstName + m2.lastName,
    );
}

type Prop = {};
type State = {
    loading: boolean,

    reports: IReport[],
    filteredReports: IReport[],

    // Filters
    filters: Object,

    reportToShow: ?IReport,
};

/**
 * The list of the reports.
 */
export default class ReportList extends React.Component<Prop, State> {
    constructor() {
        super();

        const reports = ReportStore.getAll();

        this.state = {
            loading: !reports.length,
            reports,

            filteredReports: [],

            filters: {
                date: [],
                mission: [],
                author: [],
            },

            reportToShow: null,
        };
    }

    componentDidMount() {
        this.reportListener = ReportStore.addListener(this.receiveReports);
        this.missionListener = MissionStore.addListener(this.receiveMissions);
        this.userListener = UserStore.addListener(this.receiveUsers);

        MissionActions.reload();
        UserActions.reload();
        this.loadReports();

        this.updateFilters();
        // Here we set the default sorted column
        // Temporary solution waiting for AntD to propose a native way to do it.
        const column = this.table.findColumn('date');
        this.table.toggleSortOrder('descend', column);
    }

    componentWillUnmount() {
        this.reportListener.remove();
        this.missionListener.remove();
        this.userListener.remove();
    }

    loadReports = () => {
        this.setState({
            loading: true,
        });
        ReportActions.reload().then(() => {
            this.setState({
                loading: false,
            });
        });
    };

    receiveReports = () => {
        this.setState(
            {
                reports: ReportStore.getAll(),
            },
            this.updateFilters,
        );
    };

    receiveMissions = () => {
        this.forceUpdate();
    };

    receiveUsers = () => {
        this.forceUpdate();
    };

    // Filters
    updateFilters = () => {
        const { reports } = this.state;
        const filteredReports = reports.filter(this.reportMatchFilters);
        this.setState({ filteredReports });
    };

    reportMatchFilters = (e: IReport) => {
        const { filters } = this.state;
        return (
            FilterService.matchFilter(filters.date, e.date) &&
            FilterService.matchFilter(filters.mission, e.mission.id) &&
            FilterService.matchFilter(filters.author, e.author.id)
        );
    };

    handleFilterChange = (name: string, values: Array<string | number>) => {
        this.state.filters[name] = values;
        this.updateFilters();
    };

    getReportDates = () => ArrayService.unique(this.state.reports.map(c => c.date));

    showReport = (report: IReport) => {
        this.setState({
            reportToShow: report,
        });
    };

    hideReport = () => {
        this.setState({
            reportToShow: null,
        });
    };

    render() {
        const { reportToShow } = this.state;
        return (
            <div className="report-list">
                {this.renderReportTable()}
                <ShowReportModal
                    report={reportToShow}
                    onCancel={this.hideReport}
                    visible={!!reportToShow}
                />
            </div>
        );
    }

    renderReportTable() {
        const { filters, filteredReports } = this.state;

        const columns = [
            {
                title: Locale.trans('report.date'),
                key: 'date',
                sorter: sortDateColumn,
                filterIcon: <FilterIcon active={filters.date.length > 0} />,
                render: this.renderReportDateCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="date"
                        selectedValues={filters.date}
                        values={this.getReportDates().map(d => ({
                            text: DateService.formatApiToDisplay(d),
                            value: d,
                        }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: Locale.trans('report.mission'),
                key: 'mission',
                sorter: sortMissionColumn,
                filterIcon: <FilterIcon active={filters.mission.length > 0} />,
                render: this.renderReportMissionCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="mission"
                        selectedValues={filters.mission}
                        values={MissionStore.getAll().map(m => ({ text: m.name, value: m.id }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: Locale.trans('report.author'),
                key: 'author',
                sorter: sortAuthorColumn,
                filterIcon: <FilterIcon active={filters.author.length > 0} />,
                render: this.renderReportAuthorCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="author"
                        selectedValues={filters.author}
                        values={UserStore.getAll().map(m => ({
                            text: `${m.firstName} ${m.lastName}`,
                            value: m.id,
                        }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: null,
                key: 'actions',
                width: '100px',
                render: this.rendActionsCell,
            },
        ];

        return (
            <Table
                dataSource={filteredReports}
                rowKey="id"
                columns={columns}
                locale={Locale.Table}
                ref={(r) => {
                    this.table = r;
                }}
                loading={this.state.loading && { indicator: <LoadingIcon /> }}
            />
        );
    }

    renderReportDateCell = (report: IReport) => DateService.formatApiToDisplay(report.date);

    renderReportMissionCell = (report: IReport) => {
        const mission = MissionStore.getById(report.mission.id);
        return mission && mission.name;
    };

    renderReportAuthorCell = (report: IReport) => {
        const author = UserStore.getById(report.author.id);
        return author && `${author.firstName} ${author.lastName}`;
    };

    rendActionsCell = (report: IReport) => (
        <div className="actions">
            {this.rendPrintCell(report)}
            <Button
                type="primary"
                shape="circle"
                icon="eye"
                onClick={(e) => {
                    this.showReport(report);
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                }}
            />
        </div>
    );

    rendPrintCell = (report: IReport) => (
        // TODO : Add the click event to print the report
        <Button type="primary" shape="circle" icon="printer" />
    );
}
