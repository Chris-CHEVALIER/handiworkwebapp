declare interface IEstablishment extends Entity {
    name: string;
    email: string;
    phoneNumber: string;
}
