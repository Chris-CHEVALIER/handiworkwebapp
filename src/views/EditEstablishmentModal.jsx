// @flow

import React from 'react';
import { Modal } from 'antd';

import EstablishmentForm from 'components/forms/EstablishmentForm.jsx';

import ToastActions from 'actions/ToastActions';

import EstablishmentActions from 'actions/EstablishmentActions';

import UserActions from 'actions/UserActions';
import UserStore from 'stores/UserStore';

import UserTypeActions from 'actions/UserTypeActions';
import UserTypeStore from 'stores/UserTypeStore';

type Prop = {
    onCancel: () => void,
    visible: boolean,
    establishment: ?IEstablishment,
};
type State = {
    loading: boolean,
    fields: Object,
    users: IUser[],
    userTypes: IUserType[]
};

/**
 * The modal to edit a establishment.
 */
export default class EditEstablishmentModal extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super();
        this.state = {
            loading: false,
            fields: this.getFieldsFromEntity(props.establishment),
            users: UserStore.getAll(),
            userTypes: UserTypeStore.getAll(),
        };
    }

    componentWillReceiveProps(nextProps: Prop) {
        if (nextProps.visible && !this.props.visible) {
            this.setState({
                fields: this.getFieldsFromEntity(nextProps.establishment),
            });
        }
    }

    componentDidMount() {
        this.userListener = UserStore.addListener(this.receiveUsers);
        this.userTypeListener = UserTypeStore.addListener(this.receiveUserTypes);
        UserActions.reload();
        UserTypeActions.reload();
    }

    componentWillUnmount() {
        this.userListener.remove();
        this.userTypeListener.remove();
    }

    receiveUsers = () => {
        this.setState({
            users: UserStore.getAll(),
        });
    };

    receiveUserTypes = () => {
        this.setState({
            userTypes: UserTypeStore.getAll(),
        });
    };

    getFieldsFromEntity = (entity: Entity) => {
        if (!entity) {
            return {};
        }
        const fields = {};
        const keys = Object.keys(entity);
        for (let i = 0; i < keys.length; i++) {
            const k = keys[i];
            fields[k] = { value: entity[k] };
        }
        return fields;
    };

    handleFormChange = (changedFields: Object) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    handleSubmit = (establishment: IEstablishment) => {
        if (!this.props.establishment) {
            return;
        }
        const establishmentId = this.props.establishment.id;

        this.setState({
            loading: true,
        });
        EstablishmentActions.edit(establishmentId, establishment)
            .then((newEstablishment: IEstablishment) => {
                this.setState({
                    fields: {},
                    loading: false,
                });
                ToastActions.createToastSuccess(`Etablissement "${newEstablishment.name}" modifié`);
                this.props.onCancel();
            })
            .catch(this.handleError);
    };

    handleError = (err) => {
        this.setState({
            loading: false,
        });
        try {
            const resp = JSON.parse(err.response);
            ToastActions.createToastError(resp.message);
        } catch (e) {
            ToastActions.createToastError('Une erreur est survenue');
        }
    };

    render() {
        const { establishment, visible, onCancel } = this.props;
        const { fields, loading, users, userTypes } = this.state;
        return (
            <Modal
                title="Modifier un Etablissement"
                visible={establishment && visible}
                onCancel={onCancel}
                footer={null}
            >
                <EstablishmentForm
                    establishment={establishment}
                    users={users}
                    userTypes={userTypes}
                    onChange={this.handleFormChange}
                    fields={fields}
                    onSubmit={this.handleSubmit}
                    loading={loading}
                />
            </Modal>
        );
    }
}
