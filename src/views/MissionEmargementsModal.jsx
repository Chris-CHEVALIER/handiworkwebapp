// @flow

import React from 'react';
import { Modal, Table } from 'antd';

import DeleteButton from 'components/forms/DeleteButton.jsx';

import TableColumnFilter from 'components/TableColumnFilter.jsx';
import FilterIcon from 'components/FilterIcon.jsx';
import LoadingIcon from 'components/LoadingIcon.jsx';
import EmargementPicture from 'components/EmargementPicture.jsx';

import Locale from 'locale/LocaleFactory';
import ArrayService from 'services/utils/ArrayService';
import StringService from 'services/utils/StringService';
import DateService from 'services/utils/DateService';
import FilterService from 'services/utils/FilterService';
import SecurityService from 'services/SecurityService';
import Access from 'constants/AccessLevel';
import Resource from 'constants/Resource';

import ToastActions from 'actions/ToastActions';

import EmargementActions from 'actions/EmargementActions';
import EmargementStore from 'stores/EmargementStore';

import UserActions from 'actions/UserActions';
import UserStore from 'stores/UserStore';

type Prop = {
    onCancel: () => void,
    mission: IMission,
};
type State = {
    loading: boolean,

    fields: Object,
    emargements: IEmargement[],
    userTypes: IUserType[],

    filters: Object,
    filteredEmargements: IEmargement[],
};

// Sorting Methods
function sortDateColumn(c1: IEmargement, c2: IEmargement) {
    return StringService.compareCaseInsensitive(c1.emargementDate, c2.emargementDate);
}
function sortNameColumn(u1: ?IUser, u2: ?IUser) {
    const s1 = u1 ? u1.firstName + u1.lastName : '';
    const s2 = u2 ? u2.firstName + u2.lastName : '';
    return StringService.compareCaseInsensitive(s1, s2);
}
function sortUserColumn(c1: IEmargement, c2: IEmargement) {
    const u1 = UserStore.getById(c1.user.id);
    const u2 = UserStore.getById(c2.user.id);
    return sortNameColumn(u1, u2);
}

/**
 * The modal to list the emargements of a mission.
 */
export default class MissionEmargementsModal extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super();
        this.state = {
            emargements: EmargementStore.getByMission(props.mission),

            filters: {
                date: [],
                user: [],
            },
            filteredEmargements: [],
        };
    }
    componentDidMount() {
        this.emargementListener = EmargementStore.addListener(this.receiveEmargements);
        this.userListener = UserStore.addListener(this.receiveUsers);

        EmargementActions.reload();
        UserActions.reload();

        this.updateFilters();
    }

    componentWillUnmount() {
        this.emargementListener.remove();
        this.userListener.remove();
    }

    receiveUsers = () => {
        this.forceUpdate();
    };

    receiveEmargements = () => {
        const { mission } = this.props;
        this.setState(
            {
                emargements: EmargementStore.getByMission(mission),
            },
            this.updateFilters,
        );
    };

    // Filters
    updateFilters = () => {
        const { emargements } = this.state;
        const filteredEmargements = emargements
            .filter(this.emargementMatchFilters)
            .sort((e1, e2) => sortDateColumn(e2, e1));
        this.setState({ filteredEmargements });
    };

    handleFilterChange = (name: string, values: Array<string | number>) => {
        this.state.filters[name] = values;
        this.updateFilters();
    };

    emargementMatchFilters = (e: IEmargement) => {
        const { filters } = this.state;
        return (
            FilterService.matchFilter(filters.date, e.emargementDate) &&
            FilterService.matchFilter(filters.user, e.user && e.user.id)
        );
    };

    getDates = () => ArrayService.unique(this.state.emargements.map(c => c.emargementDate));

    getUsers = () =>
        ArrayService.uniqueEntity(this.state.emargements
            .map(e => UserStore.getById(e.user.id))
            .filter(u => !!u)
            .sort(sortNameColumn));

    deleteEmargement = (emargement: IEmargement) => {
        EmargementActions.delete(emargement.id)
            .then(() => {
                ToastActions.createToastSuccess('Emargement supprimé');
            })
            .catch(() => {
                ToastActions.createToastError('Une erreur est survenue');
            });
    };

    render() {
        const { onCancel, mission } = this.props;

        return (
            <Modal
                title={`Emargements de ${mission.name}`}
                visible={!!mission}
                onCancel={onCancel}
                footer={null}
            >
                {this.renderEmargementTable()}
            </Modal>
        );
    }

    renderEmargementTable() {
        const { filters, filteredEmargements, loading } = this.state;
        const columns = [
            {
                title: 'Date',
                key: 'date',
                sorter: sortDateColumn,
                filterIcon: <FilterIcon active={filters.date.length > 0} />,
                render: this.renderDateCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="date"
                        selectedValues={filters.name}
                        values={this.getDates().map(r => ({
                            text: DateService.formatApiToDisplay(r),
                            value: r,
                        }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: 'Utilisateur',
                key: 'user',
                sorter: sortUserColumn,
                filterIcon: <FilterIcon active={filters.user.length > 0} />,
                render: this.renderUserCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="user"
                        selectedValues={filters.user}
                        values={this.getUsers().map(u => ({
                            text: `${u.firstName} ${u.lastName}`,
                            value: u.id,
                        }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: 'Signature',
                key: 'signature',
                width: '100px',
                render: this.renderSignatureCell,
            },
        ];

        if (SecurityService.isGranted(Resource.EMARGEMENT, Access.DELETE)) {
            columns.push({
                title: null,
                key: 'actions',
                width: '50px',
                render: this.rendActionsCell,
            });
        }

        return (
            <Table
                dataSource={filteredEmargements}
                rowKey="id"
                columns={columns}
                locale={Locale.Table}
                loading={loading && { indicator: <LoadingIcon /> }}
            />
        );
    }

    renderDateCell = (emargement: IEmargement) =>
        DateService.formatApiToDisplay(emargement.emargementDate);

    renderUserCell = (emargement: IEmargement) => {
        const user = UserStore.getById(emargement.user.id);
        return user && `${user.firstName} ${user.lastName}`;
    };

    renderSignatureCell = (emargement: IEmargement) => (
        <EmargementPicture emargement={emargement} />
    );

    rendActionsCell = (emargement: IEmargement) => (
        <DeleteButton
            shape="circle"
            onDelete={() => {
                this.deleteEmargement(emargement);
            }}
        />
    );
}
