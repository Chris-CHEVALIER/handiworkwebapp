// @flow

import EntityStoreBase from "../stores/EntityStoreBase";
import MissionConstants from "../constants/MissionConstants";

type State = {
    entities: { [user_id: number]: IMission[] },
    currentMissionId: ?number,
};

class MissionStore extends EntityStoreBase<IMission> {
    getInitialState(): State {
        return {
            currentMissionId: null,
            entities: []
        };
    }

    reduce(state: State, action: { type: string, payload: Object }): State {
        const { type, payload } = action;
        let newState = Object.assign({}, state);

        switch (type) {
            case MissionConstants.RECEIVE_MISSIONS_OF_USER:
                newState = this._receiveByUser(
                    state,
                    payload.user,
                    payload.missions
                );
                break;
            case MissionConstants.RECEIVE_MISSION:
                newState = this._receiveEntity(state, payload.mission);
                break;
            case MissionConstants.RECEIVE_MISSIONS:
                newState = this._receiveMany(state, payload.missions);
                break;
            case MissionConstants.CHANGE_CURRENT_MISSION:
                newState.currentMissionId = payload.missionId;
                break;
            case "clearAll":
                newState = this.getInitialState();
            default:
                newState = state;
        }
        return newState;
    }

    _receiveByUser = (state: State, user: IUser, missions: IMission[]) => {
        let newState = Object.assign({}, state);
        newState.entities = state.entities.filter(
            e => missions.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...missions);
        return newState;
    };

    _receiveMany = (state: State, missions: IMission[]) => {
        let newState = Object.assign({}, state);
        // We remove all the entities that have the same ID as the new missions
        newState.entities = state.entities.filter(
            e => missions.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...missions);
        return newState;
    };

    ///// Public Methods  /////

    getMission(): ?IMission {
        return this.getState().mission;
    }

    /**
     * Get a mission by ID.
     * @param {number} id The identifier.
     * @return {?Mission}    The mission, or NULL if no entity is found.
     */
    getById = (id: number): ?IMission => {
        return this.getState().entities.find(e => e.id === id);
    };

    /**
     * Get all missions of a user.
     */
    getByUser = (user: IUser): IMission[] => {
        return this.getState().entities.filter(e => !!e.user.find(a => a.id === user.id));
    };

    /**
     * Get all the missions.
     */
    getAll = (): Array<IMission> => this.getState().entities;

    /**
     * Get the identifier of the current mission.
     * @return {[type]} [description]
     */
    getCurrentMissionId = (): ?number => {
        return (
            this.getState().currentMissionId ||
            (this.getState().entities.length
                ? this.getState().entities[0].id
                : null)
        );
    };

    /**
     * Get the identifier of the current mission.
     * @return {[type]} [description]
     */
    getCurrentMission = (): IMission =>
        this.getById(this.getCurrentMissionId());
}

export default new MissionStore();
