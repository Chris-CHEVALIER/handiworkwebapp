/* global expect */
import Locale from 'locale/LocaleFactory';
import DateService from 'services/utils/DateService';

test('formatAPI', () => {
    const date = new Date(2017, 4, 11);
    expect( DateService.formatApi(date) ).toBe('2017-05-11');
});

test('isWorkingDay 01 May to equal TRUE', () => {
    Locale.setLocale('fr');
    const date = new Date(2017, 4, 1);
    expect( DateService.isWorkingDay(date) ).toBe(false);
});

test('isWorkingDay 02 May to equal FALSE', () => {
    Locale.setLocale('en');
    const date = new Date(2017, 4, 2);
    expect( DateService.isWorkingDay(date) ).toBe(true);
});

test('getDaysInMonth May 2017', () => {
    const months = DateService.getDaysInMonth(2017, 5);
    expect( months.length ).toBe(31);
});