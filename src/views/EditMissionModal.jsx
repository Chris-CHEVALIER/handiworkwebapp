// @flow

import React from 'react';
import { Modal } from 'antd';

import MissionForm from 'components/forms/MissionForm.jsx';

import SecurityService from 'services/SecurityService';
import Access from 'constants/AccessLevel';
import Resource from 'constants/Resource';

import ToastActions from 'actions/ToastActions';

import MissionActions from 'actions/MissionActions';

import UserActions from 'actions/UserActions';
import UserStore from 'stores/UserStore';

import UserTypeActions from 'actions/UserTypeActions';
import UserTypeStore from 'stores/UserTypeStore';

import SkillActions from 'actions/SkillActions';
import SkillStore from 'stores/SkillStore';

import EstablishmentActions from 'actions/EstablishmentActions';
import EstablishmentStore from 'stores/EstablishmentStore';

import HostCompanyActions from 'actions/HostCompanyActions';
import HostCompanyStore from 'stores/HostCompanyStore';

type Prop = {
    onCancel: () => void,
    visible: boolean,
    mission: ?IMission,
};
type State = {
    loading: boolean,
    fields: Object,
    users: IUser[],
    userTypes: IUserType[],
    skills: ISkill[],
    establishments: IEstablishment[],
    hostCompanies: IHostCompany[],
};

/**
 * The modal to edit a mission.
 */
export default class EditMissionModal extends React.Component<Prop, State> {
    userListener: *;
    skillListener: *;
    establishmentListener: *;
    hostCompaniListener: *;

    constructor(props: Prop) {
        super();
        this.state = {
            loading: false,
            fields: this.getFieldsFromEntity(props.mission),
            users: UserStore.getAll(),
            userTypes: UserTypeStore.getAll(),
            skills: SkillStore.getAll(),
            establishments: EstablishmentStore.getAll(),
            hostCompanies: HostCompanyStore.getAll(),
        };
    }

    componentWillReceiveProps(nextProps: Prop) {
        if (nextProps.visible && !this.props.visible) {
            this.setState({
                fields: this.getFieldsFromEntity(nextProps.mission),
            });
        }
    }

    componentDidMount() {
        this.userListener = UserStore.addListener(this.receiveUsers);
        this.userTypeListener = UserTypeStore.addListener(this.receiveUserTypes);
        this.skillListener = SkillStore.addListener(this.receiveSkills);
        this.establishmentListener = EstablishmentStore.addListener(this.receiveEstablishments);
        this.hostCompaniListener = HostCompanyStore.addListener(this.receiveHostCompanies);
        UserActions.reload();
        UserTypeActions.reload();
        SkillActions.reload();
        EstablishmentActions.reload();
        HostCompanyActions.reload();
    }

    componentWillUnmount() {
        this.userListener.remove();
        this.userTypeListener.remove();
        this.skillListener.remove();
        this.establishmentListener.remove();
        this.hostCompaniListener.remove();
    }

    receiveUsers = () => {
        this.setState({
            users: UserStore.getAll(),
        });
    };

    receiveUserTypes = () => {
        this.setState({
            userTypes: UserTypeStore.getAll(),
        });
    };

    receiveSkills = () => {
        this.setState({
            skills: SkillStore.getAll(),
        });
    };

    receiveEstablishments = () => {
        this.setState({
            establishments: EstablishmentStore.getAll(),
        });
    };

    receiveHostCompanies = () => {
        this.setState({
            hostCompanies: HostCompanyStore.getAll(),
        });
    };

    getFieldsFromEntity = (entity: ?Entity) => {
        if (!entity) {
            return {};
        }
        const fields = {};
        const keys = Object.keys(entity);
        for (let i = 0; i < keys.length; i++) {
            const k = keys[i];
            fields[k] = { value: entity[k] };
        }
        return fields;
    };

    handleFormChange = (changedFields: Object) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    handleSubmit = (mission: IMission) => {
        if (!this.props.mission) {
            return;
        }
        const missionId = this.props.mission.id;

        this.setState({
            loading: true,
        });
        MissionActions.edit(missionId, mission)
            .then((newMission: IMission) => {
                this.setState({
                    fields: {},
                    loading: false,
                });
                ToastActions.createToastSuccess(`Dispositif "${newMission.name}" modifié`);
                this.props.onCancel();
            })
            .catch(this.handleError);
    };

    handleError = (err) => {
        this.setState({
            loading: false,
        });
        try {
            const resp = JSON.parse(err.response);
            ToastActions.createToastError(resp.message);
        } catch (e) {
            ToastActions.createToastError('Une erreur est survenue');
        }
    };

    render() {
        const { mission, visible, onCancel } = this.props;
        const {
            fields, loading, users, userTypes, skills, establishments, hostCompanies,
        } = this.state;

        return (
            <Modal
                title="Modifier un Dispositif"
                visible={mission && visible}
                onCancel={onCancel}
                footer={null}
                bodyStyle={{ marginBottom: 200 }}
            >
                <MissionForm
                    mission={mission}
                    users={users}
                    userTypes={userTypes}
                    skills={skills}
                    establishments={establishments}
                    hostCompanies={hostCompanies}
                    onChange={this.handleFormChange}
                    fields={fields}
                    onSubmit={this.handleSubmit}
                    loading={loading}
                    readOnly={!SecurityService.isGranted(Resource.MISSION, Access.EDIT)}
                />
            </Modal>
        );
    }
}
