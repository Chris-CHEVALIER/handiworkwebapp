// @flow

import dispatcher from '../dispatchers/AppDispatcher';
import ActionsBase from '../actions/ActionsBase';

import EstablishmentConstants from '../constants/EstablishmentConstants';
import EstablishmentService from '../services/EstablishmentService';

import CacheManager from '../services/CacheManager';

const CACHE_DURATION = 30 * 1000;

class EstablishmentActions extends ActionsBase {
    create = (establishment: Object): Promise<*> => {
        const $this = this;
        return new Promise((
            resolve: (establishment: IEstablishment) => void,
            reject: (response: Object) => void,
        ): void => {
            function handleSuccess(newEstablishment: IEstablishment): void {
                dispatcher.dispatch({
                    type: EstablishmentConstants.RECEIVE_ESTABLISHMENT,
                    payload: {
                        establishment: newEstablishment,
                    },
                });
                resolve(newEstablishment);
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            EstablishmentService.post(establishment)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    edit = (establishmentId: number, establishment: Object): Promise<*> => {
        const $this = this;
        return new Promise((
            resolve: (establishment: IEstablishment) => void,
            reject: (response: Object) => void,
        ): void => {
            function handleSuccess(newEstablishment: IEstablishment): void {
                dispatcher.dispatch({
                    type: EstablishmentConstants.RECEIVE_ESTABLISHMENT,
                    payload: {
                        establishment: newEstablishment,
                    },
                });
                resolve(newEstablishment);
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            EstablishmentService.patch(establishmentId, establishment)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    delete = (id: number): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(): void {
                dispatcher.dispatch({
                    type: EstablishmentConstants.DELETE_ESTABLISHMENT,
                    payload: {
                        id,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            EstablishmentService.remove(id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    changeCurrentEstablishmentId = (establishmentId: number): Promise<*> => new Promise((resolve: () => void, reject: (response: Object) => void): void => {
        dispatcher.dispatch({
            type: EstablishmentConstants.CHANGE_CURRENT_ESTABLISHMENT,
            payload: {
                establishmentId,
            },
        });
        resolve();
    });

    reload = (): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(establishments: Array<Establishment>): void {
                dispatcher.dispatch({
                    type: EstablishmentConstants.RECEIVE_ESTABLISHMENTS,
                    payload: {
                        establishments,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            EstablishmentService.getAll()
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reloadById = (id: number): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(establishment: IEstablishment): void {
                dispatcher.dispatch({
                    type: EstablishmentConstants.RECEIVE_ESTABLISHMENT,
                    payload: {
                        establishment,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            EstablishmentService.getById(id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reloadByMission = (mission: Entity): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            if (
                CacheManager.isCached(
                    'Establishment:reloadByMission',
                    mission.id.toString(),
                    CACHE_DURATION,
                )
            ) {
                resolve();
                return;
            }

            function handleSuccess(establishments: Array<IEstablishment>): void {
                dispatcher.dispatch({
                    type: EstablishmentConstants.RECEIVE_ESTABLISHMENTS_OF_MISSION,
                    payload: {
                        establishments,
                        mission,
                    },
                });

                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            EstablishmentService.getByMission(mission.id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };
}

export default new EstablishmentActions();
