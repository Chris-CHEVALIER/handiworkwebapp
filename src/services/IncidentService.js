// @flow

import ServiceBase from '../services/ServiceBase';
import BaseUrlConstants from "../constants/BaseUrlConstants";

const URL: string = `${BaseUrlConstants.BASE_URL}`;

class IncidentService extends ServiceBase  {
    constructor() {
        super(URL);
    }

    /**
     * Get a incident by unique identifier.
     * @param {number} incidentId The identifier of the incident.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getById(incidentId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}incidents/${incidentId}`,
            method: "GET"
        });
    }

    /**
     * Get incidents of a user.
     * @param {number} userId The identifier of the user.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getByUser(userId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}users/${userId}/incidents`,
            method: "GET"
        });
    }

    /**
     * Get all incidents.
     *
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getAll(): Promise<*> {        
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}incidents`,
            method: 'GET',
        });
    }

    /**
     * Post a new incident to an user.
     * @param {Object} incident The incident to create.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    post(incident: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}incidents`,
            method: 'POST',
            data: incident
        });
    }

    /**
     * Patch an existing incident. Only the properties that are set on the patch will be updated.
     * @param {number} incidentId The identifier of the incident.
     * @param {Object} patch The properties to update.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    patch(incidentId: number, patch: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}incidents/${incidentId}`,
            method: 'PATCH',
            data: patch
        });
    }

    /**
     * Delete an existing incident.
     * @param {number} incidentId The identifier of the incident.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    remove(incidentId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}incidents/${incidentId}`,
            method: 'DELETE',
        });
    }
}

export default new IncidentService();