// @flow

const LocalStorage = {
    removeItem(key: string): Promise<*> {
        return new Promise((
            resolve: () => void,
        ): void => {
            localStorage.removeItem(key);
            resolve();
        });
    },

    setItem(key: string, value: string): Promise<*> {
        return new Promise((
            resolve: () => void,
        ): void => {
            localStorage.setItem(key, value);
            resolve();
        });
    },

    getItem(key: string): Promise<*> {
        return new Promise((
            resolve: (value: string) => void,
        ): void => {
            const value = localStorage.getItem(key);
            resolve(value);
        });
    }
};

export default LocalStorage;
