// @flow

import React from 'react';
import { Modal } from 'antd';

import SkillForm from 'components/forms/SkillForm.jsx';

import ToastActions from 'actions/ToastActions';

import SkillActions from 'actions/SkillActions';

type Prop = {
    onCancel: () => void,
    visible: boolean,
    skill: ?ISkill,
};
type State = {
    loading: boolean,
    fields: Object,
};

/**
 * The modal to edit a skill.
 */
export default class EditSkillModal extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super();
        this.state = {
            loading: false,
            fields: this.getFieldsFromEntity(props.skill),
        };
    }

    componentWillReceiveProps(nextProps: Prop) {
        if (nextProps.visible && !this.props.visible) {
            this.setState({
                fields: this.getFieldsFromEntity(nextProps.skill),
            });
        }
    }

    getFieldsFromEntity = (entity: Entity) => {
        if (!entity) {
            return {};
        }
        const fields = {};
        const keys = Object.keys(entity);
        for (let i = 0; i < keys.length; i++) {
            const k = keys[i];
            fields[k] = { value: entity[k] };
        }
        return fields;
    };

    handleFormChange = (changedFields: Object) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    handleSubmit = (skill: ISkill) => {
        if (!this.props.skill) {
            return;
        }
        const skillId = this.props.skill.id;

        this.setState({
            loading: true,
        });
        SkillActions.edit(skillId, skill)
            .then((newSkill: ISkill) => {
                this.setState({
                    fields: {},
                    loading: false,
                });
                ToastActions.createToastSuccess(`Compétence "${newSkill.name}" modifiée`);
                this.props.onCancel();
            })
            .catch(this.handleError);
    };

    handleError = (err) => {
        this.setState({
            loading: false,
        });
        try {
            const resp = JSON.parse(err.response);
            ToastActions.createToastError(resp.message);
        } catch (e) {
            ToastActions.createToastError('Une erreur est survenue');
        }
    };

    render() {
        const { skill, visible, onCancel } = this.props;
        const { fields, loading } = this.state;
        return (
            <Modal
                title="Modifier une Compétence"
                visible={skill && visible}
                onCancel={onCancel}
                footer={null}
            >
                <SkillForm
                    skill={skill}
                    onChange={this.handleFormChange}
                    fields={fields}
                    onSubmit={this.handleSubmit}
                    loading={loading}
                />
            </Modal>
        );
    }
}
