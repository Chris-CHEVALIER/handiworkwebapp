/* global expect */
import Locale from 'locale/LocaleFactory';

test('trans EN', () => {
    Locale.setLocale('en');
    expect( Locale.trans('save') ).toBe('Save');
});

test('trans FR', () => {
    Locale.setLocale('fr');
    expect( Locale.trans('welcome', {'USER': 'Test'}) ).toBe('Bonjour Test');
});
