// @flow

import dispatcher from '../dispatchers/AppDispatcher';
import ActionsBase from './ActionsBase';

import UserTypeConstants from '../constants/UserTypeConstants';
import UserTypeService from '../services/UserTypeService';

import CacheManager from '../services/CacheManager';

const CACHE_DURATION = 30 * 1000;

class UserTypeActions extends ActionsBase {
    reload = (): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            if (
                CacheManager.isCached(
                    "UserType:reload",
                    null,
                    CACHE_DURATION
                )
            ) {
                resolve();
                return;
            }

            function handleSuccess(userTypes: Array<IUserType>): void {
                dispatcher.dispatch({
                    type: UserTypeConstants.RECEIVE_USER_TYPES,
                    payload: {
                        userTypes,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            UserTypeService.getAll()
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reloadById = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(userType: IUserType): void {
                    dispatcher.dispatch({
                        type: UserTypeConstants.RECEIVE_USER_TYPE,
                        payload: {
                            userType
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                UserTypeService.getById(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };
}

export default new UserTypeActions();