/* global document */

import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import { LocaleProvider } from 'antd';
import frFR from 'antd/lib/locale-provider/fr_FR';

import LoginActions from 'actions/LoginActions';

import Layout from 'views/Layout.jsx';
import Login from 'views/Login.jsx';
import ResetPassword from 'views/ResetPassword.jsx';
import ResetPasswordRequest from 'views/ResetPasswordRequest.jsx';

import ProtectedRoute from 'components/ProtectedRoute.jsx';

const app = document.getElementById('app');
const routes = (
    <LocaleProvider locale={frFR}>
        <Router>
            <Switch>
                <Route exact path="/login" component={Login} />
                <Route exact path="/reset-password" component={ResetPassword} />
                <Route exact path="/reset-password-request" component={ResetPasswordRequest} />
                <ProtectedRoute path="/" component={Layout} />
            </Switch>
        </Router>
    </LocaleProvider>
);

LoginActions.loginUserIfRemembered();

ReactDOM.render(routes, app);
