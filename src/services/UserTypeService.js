// @flow

import ServiceBase from './ServiceBase';
import BaseUrlConstants from '../constants/BaseUrlConstants';

const URL: string = `${BaseUrlConstants.BASE_URL}`;

class UserTypeService extends ServiceBase {
    constructor() {
        super(URL);
    }

    /**
     * Get a user type by unique identifier.
     * @param {number} userTypeId The identifier of the user type.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getById(userTypeId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}user-types/${userTypeId}`,
            method: 'GET',
        });
    }

    /**
     * Get all user types.
     *
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getAll(): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}user-types`,
            method: 'GET',
        });
    }
}

export default new UserTypeService();