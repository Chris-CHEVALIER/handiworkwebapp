// @flow

import dispatcher from "../dispatchers/AppDispatcher";
import ActionsBase from "../actions/ActionsBase";

import MissionConstants from "../constants/MissionConstants";
import MissionService from "../services/MissionService";

import CacheManager from "../services/CacheManager";

const CACHE_DURATION = 30 * 1000;

class MissionActions extends ActionsBase {
    create = (mission: Object): Promise<*> => {
        const $this = this;
        return new Promise((
            resolve: (mission: IMission) => void,
            reject: (response: Object) => void,
        ): void => {
            function handleSuccess(newMission: IMission): void {
                dispatcher.dispatch({
                    type: MissionConstants.RECEIVE_MISSION,
                    payload: {
                        mission: newMission,
                    },
                });
                resolve(newMission);
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            MissionService.post(mission)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    edit = (missionId: number, mission: Object): Promise<*> => {
        const $this = this;
        return new Promise((
            resolve: (mission: IMission) => void,
            reject: (response: Object) => void,
        ): void => {
            function handleSuccess(newMission: IMission): void {
                dispatcher.dispatch({
                    type: MissionConstants.RECEIVE_MISSION,
                    payload: {
                        mission: newMission,
                    },
                });
                resolve(newMission);
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            MissionService.patch(missionId, mission)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    delete = (id: number): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(): void {
                dispatcher.dispatch({
                    type: MissionConstants.DELETE_MISSION,
                    payload: {
                        id,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            MissionService.remove(id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reload = (): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(missions: Array<IMission>): void {
                dispatcher.dispatch({
                    type: MissionConstants.RECEIVE_MISSIONS,
                    payload: {
                        missions,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            MissionService.getAll()
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    changeCurrentMissionId = (missionId: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                dispatcher.dispatch({
                    type: MissionConstants.CHANGE_CURRENT_MISSION,
                    payload: {
                        missionId
                    }
                });
                resolve();
            }
        );
    };

    reloadById = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(mission: IMission): void {
                    dispatcher.dispatch({
                        type: MissionConstants.RECEIVE_MISSION,
                        payload: {
                            mission
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                MissionService.getById(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reloadByUser = (user: Entity): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                if (
                    CacheManager.isCached(
                        "Mission:reloadByUser",
                        user.id.toString(),
                        CACHE_DURATION
                    )
                ) {
                    resolve();
                    return;
                }

                function handleSuccess(missions: Array<IMission>): void {
                    dispatcher.dispatch({
                        type: MissionConstants.RECEIVE_MISSIONS_OF_USER,
                        payload: {
                            user,
                            missions
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                MissionService.getByUser(user.id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };
}

export default new MissionActions();