declare interface IHostCompany extends Entity {
    name: string;
    email: string;
    phoneNumber: string;
}
