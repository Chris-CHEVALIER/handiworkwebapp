declare interface User extends Entity {
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    nationality: string;
}
