/* global expect */
import Logger from 'services/Logger';

test('log message and object', () => {
    
    Logger.log('Message', {title: 'object'});
    
});

test('log error', () => {
    
    Logger.error('Error Message');
    
});
