// @flow

import ServiceBase from '../services/ServiceBase';
import BaseUrlConstants from '../constants/BaseUrlConstants';

const URL: string = `${BaseUrlConstants.BASE_URL}`;

class SkillService extends ServiceBase {
    constructor() {
        super(URL);
    }

    /**
     * Get a skill by unique identifier.
     * @param {number} skillId The identifier of the skill.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getById(skillId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}skills/${skillId}`,
            method: 'GET',
        });
    }

    /**
     * Get skills of a user.
     * @param {number} userId The identifier of the user.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getByUser(userId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}users/${userId}/skills`,
            method: 'GET',
        });
    }

    /**
     * Get all skills.
     *
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getAll(): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}skills`,
            method: 'GET',
        });
    }

    /**
     * Post a new skill.
     * @param {Object} skill The skill to create.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    post(skill: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}skills`,
            method: 'POST',
            data: skill,
        });
    }

    /**
     * Patch an existing resource. Only the properties that are set on the patch will be updated.
     * @param {number} id The identifier of the report.
     * @param {Object} patch The properties to update.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    patch(id: number, patch: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}skills/${id}`,
            method: 'PATCH',
            data: patch,
        });
    }

    /**
     * Delete an existing host skill.
     * @param {number} id The identifier of the user.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    remove(id: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}skills/${id}`,
            method: 'DELETE',
        });
    }
}

export default new SkillService();
