// @flow

import dispatcher from "../dispatchers/AppDispatcher";
import ActionsBase from "../actions/ActionsBase";

import EmargementConstants from "../constants/EmargementConstants";
import EmargementService from "../services/EmargementService";

import CacheManager from "../services/CacheManager";

const CACHE_DURATION = 30 * 1000;

class EmargementActions extends ActionsBase {
    create = (emargement: Object): Promise<*> => {
        const $this = this;
        return new Promise(
            (
                resolve: (emargement: IEmargement) => void,
                reject: (response: Object) => void
            ): void => {
                function handleSuccess(newEmargement: IEmargement): void {

                    dispatcher.dispatch({
                        type: EmargementConstants.RECEIVE_EMARGEMENT,
                        payload: {
                            emargement: newEmargement
                        }
                    });
                    resolve(newEmargement);
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                EmargementService.post(emargement)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    edit = (
        emargementId: number,
        emargement: Object
    ): Promise<*> => {
        const $this = this;
        return new Promise(
            (
                resolve: (emargement: IEmargement) => void,
                reject: (response: Object) => void
            ): void => {
                function handleSuccess(newEmargement: IEmargement): void {

                    dispatcher.dispatch({
                        type: EmargementConstants.RECEIVE_EMARGEMENT,
                        payload: {
                            emargement: newEmargement
                        }
                    });
                    resolve(newEmargement);
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                EmargementService.patch(emargementId, emargement)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    delete = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(): void {
                    dispatcher.dispatch({
                        type: EmargementConstants.DELETE_EMARGEMENT,
                        payload: {
                            id
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                EmargementService.remove(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reload = (): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                if (
                    CacheManager.isCached("Emargement:reload", "", CACHE_DURATION)
                ) {
                    resolve();
                    return;
                }

                function handleSuccess(emargements: Array<IEmargement>): void {
                    dispatcher.dispatch({
                        type: EmargementConstants.RECEIVE_EMARGEMENTS,
                        payload: {
                            emargements
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                EmargementService.getAll()
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reloadById = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(emargement: IEmargement): void {
                    dispatcher.dispatch({
                        type: EmargementConstants.RECEIVE_EMARGEMENT,
                        payload: {
                            emargement
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                EmargementService.getById(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reloadByMission = (mission: Entity): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                if (
                    CacheManager.isCached(
                        "Emargement:reloadByMission",
                        mission.id.toString(),
                        CACHE_DURATION
                    )
                ) {
                    resolve();
                }

                function handleSuccess(emargements: Array<IEmargement>): void {
                    dispatcher.dispatch({
                        type:
                            EmargementConstants.RECEIVE_EMARGEMENTS_OF_MISSION,
                        payload: {
                            mission,
                            emargements
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                EmargementService.getByMission(mission.id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };
}

export default new EmargementActions();