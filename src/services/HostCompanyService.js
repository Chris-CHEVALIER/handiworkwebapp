// @flow

import ServiceBase from '../services/ServiceBase';
import BaseUrlConstants from '../constants/BaseUrlConstants';

const URL: string = `${BaseUrlConstants.BASE_URL}`;

class HostCompanyService extends ServiceBase {
    constructor() {
        super(URL);
    }

    /**
     * Get a host company by unique identifier.
     * @param {number} hostCompanyId The identifier of the host company.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getById(hostCompanyId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}host-companies/${hostCompanyId}`,
            method: 'GET',
        });
    }

    /**
     * Get host company of a mission.
     * @param {number} missionId The identifier of the mission.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getByMission(missionId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}missions/${missionId}/host-company`,
            method: "GET"
        });
    }

    /**
     * Get all host companies.
     *
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getAll(): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}host-companies`,
            method: 'GET',
        });
    }

    /**
     * Post a new host company.
     * @param {Object} hostCompany The company to create.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    post(hostCompany: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}host-companies`,
            method: 'POST',
            data: hostCompany,
        });
    }

    /**
     * Patch an existing resource. Only the properties that are set on the patch will be updated.
     * @param {number} hostCompanyId The identifier of the report.
     * @param {Object} patch The properties to update.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    patch(hostCompanyId: number, patch: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}host-companies/${hostCompanyId}`,
            method: 'PATCH',
            data: patch,
        });
    }

    /**
     * Delete an existing host company.
     * @param {number} hostCompanyId The identifier of the user.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    remove(hostCompanyId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}host-companies/${hostCompanyId}`,
            method: 'DELETE',
        });
    }
}

export default new HostCompanyService();