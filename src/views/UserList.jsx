// @flow

import React from 'react';

import { Input, Table, Icon, Button, Row, Col, Popover } from 'antd';
import TableColumnFilter from 'components/TableColumnFilter.jsx';
import FilterIcon from 'components/FilterIcon.jsx';
import CreateUserModal from 'views/CreateUserModal.jsx';
import EditUserModal from 'views/EditUserModal.jsx';
import LoadingIcon from 'components/LoadingIcon.jsx';

import Locale from 'locale/LocaleFactory';
import ArrayService from 'services/utils/ArrayService';
import StringService from 'services/utils/StringService';
import FilterService from 'services/utils/FilterService';

import UserActions from 'actions/UserActions';
import UserStore from 'stores/UserStore';

import UserTypeActions from 'actions/UserTypeActions';
import UserTypeStore from 'stores/UserTypeStore';

// Sorting Methods
function sortNameColumn(u1: IUser, u2: IUser) {
    const s1 = u1.firstName + u1.lastName;
    const s2 = u2.firstName + u2.lastName;
    return StringService.compareCaseInsensitive(s1, s2);
}
function sortTypeColumn(u1: IUser, u2: IUser) {
    const s1 = u1 && Locale.trans(u1.type);
    const s2 = u2 && Locale.trans(u2.type);
    return StringService.compareCaseInsensitive(s1, s2);
}

type Prop = {};
type State = {
    loading: boolean,
    createUserVisible: boolean,
    editUserVisible: boolean,
    userToEdit: ?IUser,

    users: IUser[],
    filteredUsers: IUser[],
    userTypes: IUserType[],

    // Filters
    filterGlobal: string,
    filters: Object,
};

/**
 * The list of the users.
 */
export default class UserList extends React.Component<Prop, State> {
    userListener: *;
    table: *;
    constructor() {
        super();

        const users = UserStore.getAll();

        this.state = {
            loading: !users.length,
            users,
            userTypes: UserTypeStore.getAll(),

            filteredUsers: [],

            filterGlobal: '',
            filters: {
                user: [],
                type: [],
            },

            createUserVisible: false,
            editUserVisible: false,
            userToEdit: null,
        };
    }

    componentDidMount() {
        this.userListener = UserStore.addListener(this.receiveUsers);
        this.userTypeListener = UserTypeStore.addListener(this.receiveUserTypes);

        this.reloadData();

        this.updateFilters();
        // Here we set the default sorted column
        // Temporary solution waiting for AntD to propose a native way to do it.
        const column = this.table.findColumn('name');
        this.table.toggleSortOrder('ascend', column);
    }
    componentWillUnmount() {
        this.userListener.remove();
        this.userTypeListener.remove();
    }

    reloadData = () => {
        this.loadUsers();
        UserTypeActions.reload();
    };

    loadUsers = () => {
        this.setState({
            loading: true,
        });
        UserActions.reload().then(() => {
            this.setState({
                loading: false,
            });
        });
    };

    receiveUsers = () => {
        this.setState(
            {
                users: UserStore.getAll(),
            },
            this.updateFilters,
        );
    };

    receiveUserTypes = () => {
        this.setState(
            {
                userTypes: UserTypeStore.getAll(),
            },
            this.updateFilters,
        );
    };

    // Filters
    updateFilters = () => {
        const { users } = this.state;
        const filteredUsers = users.filter(this.userMatchFilters);
        this.setState({ filteredUsers });
    };

    userMatchFilters = (u: IUser) => {
        const { filters } = this.state;
        return (
            FilterService.matchFilter(filters.user, u.id) &&
            FilterService.matchFilter(filters.type, u.type && u.type.id) &&
            this.matchGlobalSearch(u)
        );
    };

    searchGlobal = (e) => {
        this.state.filterGlobal = e.target.value.toLowerCase();
        this.updateFilters();
    };

    matchGlobalSearch = (user: IUser) => {
        const { filterGlobal } = this.state;
        return (
            user.firstName.toLowerCase().indexOf(filterGlobal) > -1 ||
            user.lastName.toLowerCase().indexOf(filterGlobal) > -1
        );
    };

    handleFilterChange = (name: string, values: Array<string | number>) => {
        this.state.filters[name] = values;
        this.updateFilters();
    };

    getUsers = () => (this.state.users || []).sort(sortNameColumn);

    getUserTypes = () => this.state.userTypes || [];

    showCreateUserModal = () => {
        this.setState({
            createUserVisible: true,
        });
    };
    hideCreateUserModal = () => {
        this.setState({
            createUserVisible: false,
        });
    };

    editUser = (user: IUser) => {
        this.setState({
            editUserVisible: true,
            userToEdit: user,
        });
    };
    hideEditUserModal = () => {
        this.setState({
            editUserVisible: false,
            userToEdit: null,
        });
    };

    render() {
        const { userToEdit, loading } = this.state;
        return (
            <div className="user-list">
                {this.renderUserTable()}

                {!loading && (
                    <div className="actions-row" style={{ marginTop: !filteredUsers || filteredUsers.length === 0 ? '10px' : '-50px' }}>
                        <Button type="primary" icon="plus" onClick={this.showCreateUserModal}>
                            Ajouter un utilisateur
                        </Button>
                    </div>
                )}

                <CreateUserModal
                    onCancel={this.hideCreateUserModal}
                    visible={this.state.createUserVisible}
                />
                <EditUserModal
                    user={userToEdit}
                    onCancel={this.hideEditUserModal}
                    visible={this.state.editUserVisible}
                />
            </div>
        );
    }

    renderUserTable() {
        const { filters, filteredUsers } = this.state;

        const columns = [
            {
                title: Locale.trans('user.name'),
                key: 'name',
                sorter: sortNameColumn,
                filterIcon: <FilterIcon active={filters.user.length > 0} />,
                render: this.renderUserNameCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="user"
                        selectedValues={filters.user}
                        values={this.getUsers().map(u => ({
                            text: `${u.firstName} ${u.lastName}`,
                            value: u.id,
                        }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: Locale.trans('user.type'),
                key: 'type',
                sorter: sortTypeColumn,
                filterIcon: <FilterIcon active={filters.type.length > 0} />,
                render: this.renderUserTypeCell,
                filterDropdown: (
                    <TableColumnFilter
                        name="type"
                        selectedValues={filters.type}
                        values={this.getUserTypes().map(r => ({ text: r.title, value: r.id }))}
                        onChange={this.handleFilterChange}
                    />
                ),
            },
            {
                title: null,
                key: 'actions',
                width: '50px',
                render: this.rendActionsCell,
            },
        ];

        return (
            <Table
                dataSource={filteredUsers}
                rowKey="id"
                columns={columns}
                locale={Locale.Table}
                ref={(r) => {
                    this.table = r;
                }}
                loading={this.state.loading && { indicator: <LoadingIcon /> }}
            />
        );
    }

    renderUserNameCell = (user: IUser) => `${user.firstName} ${user.lastName}`;

    renderUserTypeCell = (user: IUser) => user.type && user.type.title;

    rendActionsCell = (user: IUser) => (
        <React.Fragment>
            <Button
                type="primary"
                shape="circle"
                icon="edit"
                onClick={(e) => {
                    this.editUser(user);
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                }}
            />
        </React.Fragment>
    );
}
