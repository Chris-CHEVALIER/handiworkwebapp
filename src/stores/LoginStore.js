// @flow

import Login from '../constants/LoginConstants';
import BaseStore from '../stores/BaseStore';

type State = {
    user: ?User,
    jwt: ?string,
    secutiryContext: ?Object,
    login: ?string,
};

function reduce(state: State, action: { type: string, payload: Object }): State {
    const { type, payload } = action;
    let newState = Object.assign({}, state);
    switch (type) {
    case Login.LOGIN_USER:
        newState.jwt = payload.jwt;
        newState.user = payload.user;
        newState.securityContext = payload.securityContext;
        newState.login = payload.login;
        break;
    case Login.LOGOUT_USER:
        newState.user = null;
        newState.jwt = null;
        newState.securityContext = null;
        break;
    case Login.RECEIVE_USER_LOGIN:
        newState.login = payload.login;
        break;
    default:
        newState = state;
    }
    return newState;
}

class LoginStore extends BaseStore {
    getInitialState(): State {
        return {
            user: null,
            jwt: null,
            secutiryContext: null,
            login: null,
        };
    }

    reduce = reduce;

    getUser(): ?User {
        return this.getState().user;
    }

    getJwt(): ?string {
        return this.getState().jwt;
    }

    getSecurityContext(): ?Object {
        return this.getState().securityContext;
    }

    getLogin(): ?string {
        return this.getState().login;
    }

    isLoggedIn(): boolean {
        return !!this.getState().user;
    }
}

export default new LoginStore();
