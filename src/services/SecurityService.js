// @flow

import LoginStore from 'stores/LoginStore';

/**
 * Service to use the security context of the user.
 */
class SecurityService {
    /**
     * Check if the user is allowed to access a module with an access level.
     * @param {string} moduleName The module to check. (see constants/moduleName.js)
     * @param {number} accessibility The access level. (see constants/Accessibility.js)
     * @returns {boolean} TRUE if the user is allowed, else FALSE.
     */
    static isGranted(moduleName: string, accessibility: number): boolean {
        const user: ?Object = LoginStore.getUser();
        const securityContext: ?Object = LoginStore.getSecurityContext();
        return (
            (!!user && user.isSuperAdmin) ||
            (!!securityContext && (securityContext[moduleName] & accessibility) !== 0)
        );
    }
}

export default SecurityService;
