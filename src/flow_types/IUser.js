declare interface IUser extends Entity {
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    nationality: string;
    type: IUserType;
}
