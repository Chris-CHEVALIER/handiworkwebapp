// @flow

import dispatcher from "../dispatchers/AppDispatcher";
import ActionsBase from "../actions/ActionsBase";

import ReportConstants from "../constants/ReportConstants";
import ReportService from "../services/ReportService";

import CacheManager from "../services/CacheManager";

const CACHE_DURATION = 30 * 1000;

class ReportActions extends ActionsBase {
    create = (report: Object): Promise<*> => {
        const $this = this;
        return new Promise(
            (
                resolve: (report: IReport) => void,
                reject: (response: Object) => void
            ): void => {
                function handleSuccess(newReport: IReport): void {
                    dispatcher.dispatch({
                        type: ReportConstants.RECEIVE_REPORT,
                        payload: {
                            report: newReport
                        }
                    });
                    resolve(newReport);
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                ReportService.post(report)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    edit = (reportId: number, report: Object): Promise<*> => {
        const $this = this;
        return new Promise(
            (
                resolve: (report: IReport) => void,
                reject: (response: Object) => void
            ): void => {
                function handleSuccess(newReport: IReport): void {
                    dispatcher.dispatch({
                        type: ReportConstants.RECEIVE_REPORT,
                        payload: {
                            report: newReport
                        }
                    });
                    resolve(newReport);
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                ReportService.patch(reportId, report)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    delete = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(): void {
                    dispatcher.dispatch({
                        type: ReportConstants.DELETE_REPORT,
                        payload: {
                            id
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                ReportService.remove(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reload = (): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                if (
                    CacheManager.isCached("Report:reload", "", CACHE_DURATION)
                ) {
                    resolve();
                    return;
                }

                function handleSuccess(reports: Array<IReport>): void {
                    dispatcher.dispatch({
                        type: ReportConstants.RECEIVE_REPORTS,
                        payload: {
                            reports
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                ReportService.getAll()
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    changeCurrentReportId = (reportId: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                dispatcher.dispatch({
                    type: ReportConstants.CHANGE_CURRENT_REPORT,
                    payload: {
                        reportId
                    }
                });
                resolve();
            }
        );
    };

    reloadByUser = (user: Entity): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                if (
                    CacheManager.isCached(
                        "Report:reloadByUser",
                        user.id.toString(),
                        CACHE_DURATION
                    )
                ) {
                    resolve();
                    return;
                }

                function handleSuccess(reports: Array<IReport>): void {
                    dispatcher.dispatch({
                        type: ReportConstants.RECEIVE_REPORTS_OF_USER,
                        payload: {
                            user,
                            reports
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                ReportService.getByUser(user.id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reloadById = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(report: IReport): void {
                    dispatcher.dispatch({
                        type: ReportConstants.RECEIVE_REPORT,
                        payload: {
                            report
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                ReportService.getById(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };
}

export default new ReportActions();