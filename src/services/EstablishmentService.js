// @flow

import ServiceBase from '../services/ServiceBase';
import BaseUrlConstants from '../constants/BaseUrlConstants';

class EstablishmentService extends ServiceBase {

    /**
     * Get an establishment by unique identifier.
     * @param {number} establishmentId The identifier of the establishment.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getById(establishmentId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}establishments/${establishmentId}`,
            method: 'GET',
        });
    }

    /**
     * Get host company of a mission.
     * @param {number} missionId The identifier of the mission.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getByMission(missionId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}missions/${missionId}/establishment`,
            method: 'GET',
        });
    }

    /**
     * Get all establishments.
     *
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getAll(): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}establishments`,
            method: 'GET',
        });
    }

    /**
     * Post a new establishment.
     * @param {Object} establishment The establishment to create.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    post(establishment: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}establishments`,
            method: 'POST',
            data: establishment,
        });
    }

    /**
     * Patch an existing resource. Only the properties that are set on the patch will be updated.
     * @param {number} establishmentId The identifier of the report.
     * @param {Object} patch The properties to update.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    patch(establishmentId: number, patch: Object): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}establishments/${establishmentId}`,
            method: 'PATCH',
            data: patch,
        });
    }

    /**
     * Delete an existing host establishment.
     * @param {number} establishmentId The identifier of the user.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    remove(establishmentId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}establishments/${establishmentId}`,
            method: 'DELETE',
        });
    }
}

export default new EstablishmentService();
