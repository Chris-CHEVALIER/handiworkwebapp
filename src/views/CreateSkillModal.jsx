// @flow

import React from 'react';
import { Modal } from 'antd';

import SkillForm from 'components/forms/SkillForm.jsx';

import ToastActions from 'actions/ToastActions';

import SkillActions from 'actions/SkillActions';

type Prop = {
    onCancel: () => void,
    visible: boolean,
};
type State = {
    loading: boolean,
    fields: Object,
};

/**
 * The modal to create a new skill.
 */
export default class CreateSkillModal extends React.Component<Prop, State> {
    constructor() {
        super();
        this.state = {
            loading: false,
            fields: {},
        };
    }

    handleFormChange = (changedFields: Object) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    handleSubmit = (skill: ISkill) => {
        this.setState({
            loading: true,
        });
        SkillActions.create(skill)
            .then((newSkill: ISkill) => {
                this.setState({
                    fields: {},
                    loading: false,
                });
                ToastActions.createToastSuccess(`Compétence "${newSkill.name}" créée`);
                this.props.onCancel();
            })
            .catch(this.handleError);
    };

    handleError = (err: Object) => {
        this.setState({
            loading: false,
        });
        try {
            const resp = JSON.parse(err.response);
            ToastActions.createToastError(resp.message);
        } catch (e) {
            ToastActions.createToastError('Une erreur est survenue');
        }
    };

    render() {
        const { visible, onCancel } = this.props;
        const { fields, loading } = this.state;
        return (
            <Modal
                title="Ajouter une Compétence"
                visible={visible}
                onCancel={onCancel}
                footer={null}
            >
                <SkillForm
                    onChange={this.handleFormChange}
                    fields={fields}
                    onSubmit={this.handleSubmit}
                    loading={loading}
                />
            </Modal>
        );
    }
}
