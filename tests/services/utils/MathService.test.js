/* global expect */
import MathService from 'services/utils/MathService';

test('round 10.125 to equal 10', () => {
    expect( MathService.round(10.125) ).toBe(10);
});

test('round 10.125 to equal 10.1', () => {
    expect( MathService.round(10.125, 1) ).toBe(10.1);
});

test('round 10.125 to equal 10.13', () => {
    expect( MathService.round(10.125, 2) ).toBe(10.13);
});
