export default {
    RECEIVE_MEDIAS_OF_REPORT: 'RECEIVE_MEDIAS_OF_REPORT',
    RECEIVE_MEDIAS_OF_INCIDENT: 'RECEIVE_MEDIAS_OF_INCIDENT',
    RECEIVE_MEDIAS_OF_REVIEW: 'RECEIVE_MEDIAS_OF_REVIEW',
    RECEIVE_MEDIA: 'RECEIVE_MEDIA',
    RECEIVE_MEDIAS: 'RECEIVE_MEDIAS',
    DELETE_MEDIA: 'DELETE_MEDIA',
};
