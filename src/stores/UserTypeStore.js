// @flow

import EntityStoreBase from "./EntityStoreBase";
import UserTypeConstants from "../constants/UserTypeConstants";

type State = {
    entities: { [user_id: number]: IUserType[] },
};

class UserTypeStore extends EntityStoreBase<IUserType> {
    getInitialState(): State {
        return {
            entities: []
        };
    }

    reduce(state: State, action: { type: string, payload: Object }): State {
        const { type, payload } = action;
        let newState = Object.assign({}, state);

        switch (type) {
            case UserTypeConstants.RECEIVE_USER_TYPE:
                newState = this._receiveEntity(state, payload.userType);
                break;
            case UserTypeConstants.RECEIVE_USER_TYPES:
                newState = this._receiveMany(state, payload.userTypes);
                break;
            case "clearAll":
                newState = this.getInitialState();
            default:
                newState = state;
        }
        return newState;
    }

    _receiveMany = (state: State, userTypes: IUserType[]) => {
        let newState = Object.assign({}, state);
        // We remove all the entities that have the same ID as the new user types
        newState.entities = state.entities.filter(
            e => userTypes.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...userTypes);
        return newState;
    };

    ///// Public Methods  /////

    /**
     * Get a user type by ID.
     * @param {number} id The identifier.
     * @return {?IUserType}    The user type, or NULL if no entity is found.
     */
    getById = (id: number): ?IUserType => {
        return this.getState().entities.find(e => e[0].id === id);
    };

    /**
     * Get all the user types.
     */
    getAll = (): Array<IUserType> => this.getState().entities;
}

export default new UserTypeStore();