// @flow

import React from 'react';
import { Modal } from 'antd';

import UserForm from 'components/forms/UserForm.jsx';

import ToastActions from 'actions/ToastActions';

import UserActions from 'actions/UserActions';

import RoleActions from 'actions/RoleActions';
import RoleStore from 'stores/RoleStore';

import UserTypeActions from 'actions/UserTypeActions';
import UserTypeStore from 'stores/UserTypeStore';

type Prop = {
    onCancel: () => void,
    visible: boolean,
    user: ?IUser,
};
type State = {
    loading: boolean,
    fields: Object,
    roles: IRole[],
    userTypes: IUserType[],
};

/**
 * The modal to edit a user.
 */
export default class EditUserModal extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super();
        this.state = {
            loading: false,
            fields: this.getFieldsFromEntity(props.user),
            roles: RoleStore.getAll(),
            userTypes: UserTypeStore.getAll()
        };
    }

    componentDidMount() {
        this.roleListener = RoleStore.addListener(this.receiveRoles);
        this.userTypeListener = UserTypeStore.addListener(this.receiveUserTypes);
        RoleActions.reload();
        UserTypeActions.reload();
    }

    componentWillUnmount() {
        this.roleListener.remove();
        this.userTypeListener.remove();
    }

    receiveRoles = () => {
        this.setState({
            roles: RoleStore.getAll(),
        });
    };

    receiveUserTypes = () => {
        this.setState({
            userTypes: UserTypeStore.getAll()
        });
    };

    componentWillReceiveProps(nextProps: Prop) {
        if (nextProps.visible && !this.props.visible) {
            this.setState({
                fields: this.getFieldsFromEntity(nextProps.user),
            });
        }
    }

    getFieldsFromEntity = (entity: Entity) => {
        if (!entity) {
            return {};
        }
        const fields = {};
        const keys = Object.keys(entity);
        for (let i = 0; i < keys.length; i++) {
            const k = keys[i];
            fields[k] = { value: entity[k] };
        }
        return fields;
    };

    handleFormChange = (changedFields: Object) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    handleSubmit = (user: IUser) => {
        if (!this.props.user) {
            return;
        }
        const userId = this.props.user.id;

        this.setState({
            loading: true,
        });
        UserActions.edit(userId, user)
            .then((newUser: IUser) => {
                this.setState({
                    fields: {},
                    loading: false,
                });
                ToastActions.createToastSuccess(`Utilisateur "${newUser.firstName} ${newUser.lastName}" modifié`);
                this.props.onCancel();
            })
            .catch(this.handleError);
    };

    handleError = (err) => {
        this.setState({
            loading: false,
        });
        try {
            const resp = JSON.parse(err.response);
            ToastActions.createToastError(resp.message);
        } catch (e) {
            ToastActions.createToastError('Une erreur est survenue');
        }
    };

    render() {
        const { user, visible, onCancel } = this.props;
        const { fields, roles, userTypes, loading } = this.state;
        return (
            <Modal
                title="Modifier un Utilisateur"
                visible={user && visible}
                onCancel={onCancel}
                footer={null}
            >
                <UserForm
                    user={user}
                    roles={roles}
                    userTypes={userTypes}
                    onChange={this.handleFormChange}
                    fields={fields}
                    onSubmit={this.handleSubmit}
                    loading={loading}
                />
            </Modal>
        );
    }
}
