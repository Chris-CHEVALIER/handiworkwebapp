// @flow

import dispatcher from "../dispatchers/AppDispatcher";
import ActionsBase from "../actions/ActionsBase";

import HostCompanyConstants from "../constants/HostCompanyConstants";
import HostCompanyService from "../services/HostCompanyService";

import CacheManager from "../services/CacheManager";

const CACHE_DURATION = 30 * 1000;

class HostCompanyActions extends ActionsBase {
    create = (hostCompany: Object): Promise<*> => {
        const $this = this;
        return new Promise((resolve: (hostCompany: IHostCompany) => void, reject: (response: Object) => void): void => {
            function handleSuccess(newCompany: IHostCompany): void {
                dispatcher.dispatch({
                    type: HostCompanyConstants.RECEIVE_HOST_COMPANY,
                    payload: {
                        hostCompany: newCompany,
                    },
                });
                resolve(newCompany);
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            HostCompanyService.post(hostCompany)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    edit = (companyId: number, hostCompany: Object): Promise<*> => {
        const $this = this;
        return new Promise((resolve: (hostCompany: IHostCompany) => void, reject: (response: Object) => void): void => {
            function handleSuccess(newCompany: IHostCompany): void {
                dispatcher.dispatch({
                    type: HostCompanyConstants.RECEIVE_HOST_COMPANY,
                    payload: {
                        hostCompany: newCompany,
                    },
                });
                resolve(newCompany);
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            HostCompanyService.patch(companyId, hostCompany)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    delete = (id: number): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(): void {
                dispatcher.dispatch({
                    type: HostCompanyConstants.DELETE_HOST_COMPANY,
                    payload: {
                        id,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            HostCompanyService.remove(id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    changeCurrentHostCompanyId = (hostCompanyId: number): Promise<*> => {
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                dispatcher.dispatch({
                    type: HostCompanyConstants.CHANGE_CURRENT_HOST_COMPANY,
                    payload: {
                        hostCompanyId,
                    }
                });
                resolve();
            }
        );
    };

    reload = (): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(hostCompanies: Array<HostCompany>): void {
                dispatcher.dispatch({
                    type: HostCompanyConstants.RECEIVE_HOST_COMPANIES,
                    payload: {
                        hostCompanies,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            HostCompanyService.getAll()
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reloadById = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(hostCompany: IHostCompany): void {
                    dispatcher.dispatch({
                        type: HostCompanyConstants.RECEIVE_HOST_COMPANY,
                        payload: {
                            hostCompany
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                HostCompanyService.getById(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reloadByMission = (mission: Entity): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                if (
                    CacheManager.isCached(
                        "HostCompany:reloadByMission",
                        mission.id.toString(),
                        CACHE_DURATION
                    )
                ) {
                    resolve();
                    return;
                }

                function handleSuccess(hostCompanies: Array<IHostCompany>): void {
                    dispatcher.dispatch({
                        type: HostCompanyConstants.RECEIVE_HOST_COMPANIES_OF_MISSION,
                        payload: {
                            hostCompanies,
                            mission
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                HostCompanyService.getByMission(mission.id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };
}

export default new HostCompanyActions();