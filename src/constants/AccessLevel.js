export default {
    READ: 1,
    EDIT: 2,
    CREATE: 4,
    DELETE: 8,
};
