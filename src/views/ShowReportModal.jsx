// @flow

import React from 'react';
import { Modal } from 'antd';

import StringService from '../services/utils/StringService';
import DateService from '../services/utils/DateService';
import UserStore from '../stores/UserStore';
import MissionStore from '../stores/MissionStore';

import MediaStore from '../stores/MediaStore';
import MediaActions from '../actions/MediaActions';

import BaseUrlConstants from '../constants/BaseUrlConstants';
import LoginStore from '../stores/LoginStore';

type Prop = {
    onCancel: () => void,
    visible: boolean,
    mission: ?IMission,
};
type State = {
    loading: boolean,
    fields: Object,
    users: IUser[],
    userTypes: IUserType[],
    skills: ISkill[],
    establishments: IEstablishment[],
    hostCompanies: IHostCompany[],
};

/**
 * The modal to show a report.
 */
export default class ShowReportModal extends React.Component<Prop, State> {
    componentDidMount() {
        this.userListener = UserStore.addListener(this.receiveUsers);
        this.missionListener = MissionStore.addListener(this.receiveMissions);
        this.mediaListener = MediaStore.addListener(this.receiveMedias);
    }

    componentWillUnmount() {
        this.userListener.remove();
        this.missionListener.remove();
    }

    componentDidUpdate(prevProps) {
        const { report } = this.props;
        if (report && report !== prevProps.report) {
            MediaActions.reloadByReport(report);
        }
    }

    receiveUsers = () => {
        this.forceUpdate();
    };

    receiveMissions = () => {
        this.forceUpdate();
    };

    receiveMedias = () => {
        this.forceUpdate();
    };

    render() {
        const { report, visible, onCancel } = this.props;

        if (!report) {
            return null;
        }

        return (
            <Modal
                title="Rapport d'activité"
                visible={visible}
                onCancel={onCancel}
                footer={null}
                bodyStyle={{ marginBottom: 200 }}
            >
                {this.renderContent()}
            </Modal>
        );
    }

    renderContent() {
        const { report } = this.props;

        const mission = MissionStore.getById(report.mission.id);
        const author = UserStore.getById(report.author.id);
        return (
            <div className="report-container">
                <p className="report-title">{report.title}</p>
                <p className="report-date">
                    Scéance du {DateService.formatApiToDisplay(report.date)}
                </p>
                <p className="report-content">{StringService.nl2br(report.content)}</p>
                <p className="report-footer">
                    {author && `Rédigé par : ${author.firstName} ${author.lastName.toUpperCase()}`}
                    <br />
                    {mission && `Dispositif : ${mission.name}`}
                </p>

                {this.renderMedias()}
            </div>
        );
    }

    renderMedias() {
        const { report } = this.props;

        const medias = MediaStore.getByReport(report);
        if (medias.length === 0) {
            return <p>Aucune pièce jointe</p>;
        }

        const token = LoginStore.getJwt();
        const formattedToken = token ? token.replace(/\+/g, '%2B') : '';
        const url = `${BaseUrlConstants.BASE_URL}medias/__ID__/file?X-Auth-Token=${formattedToken}`;

        return (
            <div className="report-media-container">
                <div className="report-media-title">Pièces jointes</div>
                <div className="report-media-list">
                    {medias.map(media => (
                        <a key={media.id} href={url.replace('__ID__', media.id)}>
                            {media.title}
                        </a>
                    ))}
                </div>
            </div>
        );
    }
}
