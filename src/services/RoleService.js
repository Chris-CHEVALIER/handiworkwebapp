// @flow

import ServiceBase from '../services/ServiceBase';
import BaseUrlConstants from '../constants/BaseUrlConstants';

const URL: string = `${BaseUrlConstants.BASE_URL}`;

class RoleService extends ServiceBase {
    constructor() {
        super(URL);
    }

    /**
     * Get a role by unique identifier.
     * @param {number} roleId The identifier of the role.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getById(roleId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}roles/${roleId}`,
            method: 'GET',
        });
    }

    /**
     * Get role of a user.
     * @param {number} userId The identifier of the user.
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getByMission(userId: number): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}users/${userId}/roles`,
            method: 'GET',
        });
    }

    /**
     * Get all roles.
     *
     * @returns {Promise} A promise to handle the request ascynchronously.
     */
    getAll(): Promise<*> {
        return this.execute({
            url: `${BaseUrlConstants.BASE_URL}roles`,
            method: 'GET',
        });
    }
}

export default new RoleService();