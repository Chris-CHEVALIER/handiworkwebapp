// @flow

import EntityStoreBase from "../stores/EntityStoreBase";
import SubSkillConstants from "../constants/SubSkillConstants";

type State = {
    entities: { [skill_id: number]: ISubSkill[] },
    currentSubSkillId: ?number,
};


class SubSkillStore extends EntityStoreBase<ISubSkill> {
    getInitialState(): State {
        return {
            currentSubSkillId: null,
            entities: []
        };
    }

    reduce(state: State, action: { type: string, payload: Object }): State {
        const { type, payload } = action;
        let newState = Object.assign({}, state);

        switch (type) {
            case SubSkillConstants.RECEIVE_SUB_SKILLS_OF_SKILL:
                newState = this._receiveBySkill(
                    state,
                    payload.skill,
                    payload.subSkills
                );
                break;
            case SubSkillConstants.RECEIVE_SUB_SKILLS_OF_USER_HAS_SKILL:
                newState = this._receiveByUserHasSkill(
                    state,
                    payload.userHasSkill,
                    payload.subSkills
                );
                break;
            case SubSkillConstants.RECEIVE_SUB_SKILL:
                newState = this._receiveEntity(state, payload.subSkill);
                break;
            case SubSkillConstants.RECEIVE_SUB_SKILLS:
                newState = this._receiveMany(state, payload.subSkills);
                break;
            case SubSkillConstants.CHANGE_CURRENT_SUB_SKILL:
                newState.currentSubSkillId = payload.subSkillId;
                break;
            case "clearAll":
                newState = this.getInitialState();
            default:
                newState = state;
        }
        return newState;
    }

    _receiveBySkill = (state: State, skill: ISkill, subSkills: ISubSkill[]) => {
        let newState = Object.assign({}, state);
        newState.entities = state.entities.filter(
            e => subSkills.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...subSkills);
        return newState;
    };

    _receiveByUserHasSkill = (state: State, userHasSkill: IUserHasSkill, subSkills: ISubSkill[]) => {
        let newState = Object.assign({}, state);
        newState.entities = state.entities.filter(
            e => subSkills.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...subSkills);
        return newState;
    };

    _receiveMany = (state: State, subSkills: ISubSkill[]) => {
        let newState = Object.assign({}, state);
        // We remove all the entities that have the same ID as the new skills
        newState.entities = state.entities.filter(
            e => subSkills.findIndex(a => e.id === a.id) === -1
        );
        newState.entities.push(...subSkills);
        return newState;
    };

    ///// Public Methods  /////

    getSubSkill(): ?ISubSkill {
        return this.getState().subSkill;
    }

    /**
     * Get a sub skill by ID.
     * @param {number} id The identifier.
     * @return {?Skill}    The sub skill, or NULL if no entity is found.
     */
    getById = (id: number): ?ISubSkill => {
        return this.getState().entities.find(e => e.id === id);
    };

    /**
     * Get all the sub skills of a skill.
     */
    getBySkill = (skill: ISkill): ISubSkill[] => {
        return skill ? this.getState().entities.filter(e => e.skill.id === skill.id) || [] : [];
    };

    /**
     * Get all the sub skills of a user has skill.
     */
    getByUserHasSkill = (userHasSkill: IUserHasSkill): ISubSkill[] => {
        return userHasSkill ? this.getState().entities.filter(e => e.user_has_skill.length > 0) || [] : [];
    };
        
    /**
     * Get all the sub skills.
     */
    getAll = (): Array<ISubSkill> => this.getState().entities;

    /**
     * Get the identifier of the current sub skill.
     * @return {[type]} [description]
     */
    getCurrentSubSkillId = (): ?number => {
        return (
            this.getState().currentSubSkillId ||
            (this.getState().entities.length
                ? this.getState().entities[0].id
                : null)
        );
    };

    /**
     * Get the identifier of the current sub skill.
     * @return {[type]} [description]
     */
    getCurrentSubSkill = (): ISubSkill =>
        this.getById(this.getCurrentSubSkillId());
}

export default new SubSkillStore();