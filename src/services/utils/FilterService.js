// @flow

export default class FilterService {
    static matchFilter(filter: string | number | Array, value: string | number | Array) {
        if (!filter || filter.length === 0) {
            return true;
        }

        let match;
        if (Array.isArray(value)) {
            match = filter.some(r => value.indexOf(r) >= 0);
        } else if (Array.isArray(filter)) {
            match = filter.indexOf(value) !== -1;
        } else {
            match = filter.toString() === value.toString();
        }

        return match;
    }
}
