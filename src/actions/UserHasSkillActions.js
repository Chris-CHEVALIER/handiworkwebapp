// @flow

import dispatcher from "../dispatchers/AppDispatcher";
import ActionsBase from "../actions/ActionsBase";

import UserHasSkillConstants from "../constants/UserHasSkillConstants";
import UserHasSkillService from "../services/UserHasSkillService";

import CacheManager from "../services/CacheManager";

const CACHE_DURATION = 30 * 1000;

class UserHasSkillActions extends ActionsBase {
    create = (userHasSkill: Object): Promise<*> => {
        const $this = this;
        return new Promise(
            (
                resolve: (userHasSkill: IUserHasSkill) => void,
                reject: (response: Object) => void
            ): void => {
                function handleSuccess(newUserHasSkill: IUserHasSkill): void {
                    dispatcher.dispatch({
                        type: UserHasSkillConstants.RECEIVE_USER_HAS_SKILL,
                        payload: {
                            userHasSkill: newUserHasSkill
                        }
                    });
                    resolve(newUserHasSkill);
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                UserHasSkillService.post(userHasSkill)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    edit = (
        userHasSkillId: number,
        userHasSkill: Object
    ): Promise<*> => {
        const $this = this;
        return new Promise(
            (
                resolve: (userHasSkill: IUserHasSkill) => void,
                reject: (response: Object) => void
            ): void => {
                function handleSuccess(newUserHasSkill: IUserHasSkill): void {

                    dispatcher.dispatch({
                        type: UserHasSkillConstants.RECEIVE_USER_HAS_SKILL,
                        payload: {
                            userHasSkill: newUserHasSkill
                        }
                    });
                    resolve(newUserHasSkill);
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                UserHasSkillService.patch(userHasSkillId, userHasSkill)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    delete = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(): void {
                    dispatcher.dispatch({
                        type: UserHasSkillConstants.DELETE_USER_HAS_SKILL,
                        payload: {
                            id
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                UserHasSkillService.remove(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reload = (): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                if (
                    CacheManager.isCached("UserHasSkill:reload", "", CACHE_DURATION)
                ) {
                    resolve();
                    return;
                }

                function handleSuccess(userHasSkills: Array<IUserHasSkill>): void {
                    dispatcher.dispatch({
                        type: UserHasSkillConstants.RECEIVE_USER_HAS_SKILLS,
                        payload: {
                            userHasSkills
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                UserHasSkillService.getAll()
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reloadById = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(userHasSkill: IUserHasSkill): void {
                    dispatcher.dispatch({
                        type: UserHasSkillConstants.RECEIVE_USER_HAS_SKILL,
                        payload: {
                            userHasSkill
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                UserHasSkillService.getById(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };
}

export default new UserHasSkillActions();