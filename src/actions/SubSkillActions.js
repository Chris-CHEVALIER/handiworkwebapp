// @flow

import dispatcher from "../dispatchers/AppDispatcher";
import ActionsBase from "../actions/ActionsBase";

import SubSkillConstants from "../constants/SubSkillConstants";
import SubSkillService from "../services/SubSkillService";

import CacheManager from "../services/CacheManager";

const CACHE_DURATION = 30 * 1000;

class SubSkillActions extends ActionsBase {
    create = (subSkill: Object): Promise<*> => {
        const $this = this;
        return new Promise((
            resolve: (subSkill: ISubSkill) => void,
            reject: (response: Object) => void,
        ): void => {
            function handleSuccess(newSubSkill: ISubSkill): void {
                dispatcher.dispatch({
                    type: SubSkillConstants.RECEIVE_SUB_SKILL,
                    payload: {
                        subSkill: newSubSkill,
                    },
                });
                resolve(newSubSkill);
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            SubSkillService.post(subSkill)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    edit = (id: number, subSkill: Object): Promise<*> => {
        const $this = this;
        return new Promise((
            resolve: (subSkill: ISubSkill) => void,
            reject: (response: Object) => void,
        ): void => {
            function handleSuccess(newSubSkill: ISubSkill): void {
                dispatcher.dispatch({
                    type: SubSkillConstants.RECEIVE_SUB_SKILL,
                    payload: {
                        subSkill: newSubSkill,
                    },
                });
                resolve(newSubSkill);
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            SubSkillService.patch(id, subSkill)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    delete = (id: number): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(): void {
                dispatcher.dispatch({
                    type: SubSkillConstants.DELETE_SUB_SKILL,
                    payload: {
                        id,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            SubSkillService.remove(id)
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    reload = (): Promise<*> => {
        const $this = this;
        return new Promise((resolve: () => void, reject: (response: Object) => void): void => {
            function handleSuccess(subSkills: Array<ISubSkill>): void {
                dispatcher.dispatch({
                    type: SubSkillConstants.RECEIVE_SUB_SKILLS,
                    payload: {
                        subSkills,
                    },
                });
                resolve();
            }

            function handleError(err: Object): void {
                $this.handleError(err, reject);
            }

            SubSkillService.getAll()
                .then(handleSuccess)
                .catch(handleError);
        });
    };

    changeCurrentSubSkillId = (subSkillId: number): Promise<*> => {
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                dispatcher.dispatch({
                    type: SubSkillConstants.CHANGE_CURRENT_SUB_SKILL,
                    payload: {
                        subSkillId
                    }
                });
                resolve();
            }
        );
    };

    reloadById = (id: number): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                function handleSuccess(subSkill: ISubSkill): void {
                    dispatcher.dispatch({
                        type: SubSkillConstants.RECEIVE_SUB_SKILL,
                        payload: {
                            subSkill
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                SubSkillService.getById(id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reloadBySkill = (skill: Entity): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                if (
                    CacheManager.isCached(
                        "SubSkill:reloadBySkill",
                        skill.id.toString(),
                        CACHE_DURATION
                    )
                ) {
                    resolve();
                    return;
                }

                function handleSuccess(subSkills: Array<ISubSkill>): void {
                    dispatcher.dispatch({
                        type: SubSkillConstants.RECEIVE_SUB_SKILLS_OF_SKILL,
                        payload: {
                            skill,
                            subSkills
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                SubSkillService.getBySkill(skill.id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };

    reloadByUserHasSkill = (userHasSkill: Entity): Promise<*> => {
        const $this = this;
        return new Promise(
            (resolve: () => void, reject: (response: Object) => void): void => {
                if (
                    CacheManager.isCached(
                        "SubSkill:reloadByUserHasSkill",
                        userHasSkill.id.toString(),
                        CACHE_DURATION
                    )
                ) {
                    resolve();
                    return;
                }

                function handleSuccess(subSkills: Array<ISubSkill>): void {
                    dispatcher.dispatch({
                        type: SubSkillConstants.RECEIVE_SUB_SKILLS_OF_USER_HAS_SKILL,
                        payload: {
                            userHasSkill,
                            subSkills
                        }
                    });
                    resolve();
                }

                function handleError(err: Object): void {
                    $this.handleError(err, reject);
                }

                SubSkillService.getByUserHasSkill(userHasSkill.id)
                    .then(handleSuccess)
                    .catch(handleError);
            }
        );
    };
}

export default new SubSkillActions();