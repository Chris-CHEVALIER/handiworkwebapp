// @flow

import React from 'react';
import { Tabs } from 'antd';

import Panel from 'components/Panel.jsx';

import UserList from 'views/UserList.jsx';
import HostCompanyList from 'views/HostCompanyList.jsx';
import EstablishmentList from 'views/EstablishmentList.jsx';
import SkillList from 'views/SkillList.jsx';
import SubSkillList from 'views/SubSkillList.jsx';

const { TabPane } = Tabs;

type Prop = {};
type State = {};

export default class Home extends React.Component<Prop, State> {
    constructor() {
        super();
        this.state = {};
    }

    render() {
        return (
            <Panel title="Configuration">
                <Tabs>
                    <TabPane tab="Utilisateurs" key="user">
                        <UserList />
                    </TabPane>
                    <TabPane tab="Entreprises d'Accueil" key="host-company">
                        <HostCompanyList />
                    </TabPane>
                    <TabPane tab="Etablissements" key="establishment">
                        <EstablishmentList />
                    </TabPane>
                    <TabPane tab="Compétences" key="skill">
                        <SkillList />
                    </TabPane>
                    <TabPane tab="Sous-Compétences" key="subskill">
                        <SubSkillList />
                    </TabPane>
                </Tabs>
            </Panel>
        );
    }
}
